# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [0.0.7] - 2016-12-31
### Changed
- Database configuration and logging
### Added
- Export all articles as CSV
## [0.0.6] - 2016-12-31
### Added
- User can sort and categorize articles
- REST API for retrieving articles as JSON
### Changed
- Switched from spray-can to Akka HTTP for our server
### Fixed
- Bug fix for feature that highlights changed data in forms

## [0.0.5] - 2016-11-18
### Added
- Feature that highlights changed data in form

## [0.0.4] - 2016-09-15
### Changed
- Application is now dockerized
- Modified build.sbt and README accordingly

## [0.0.3] - 2016-09-15
### Added
- A minimal form for the client and a button that sends the data in the form to the server
- The server sends a response to the client when it received the data from the form

## [0.0.2] - 2016-09-11
### Added
- Autowire example for having a type-safe API between client and server
- Title tag for web page

### Changed
- We use the launcher script created by SBT in the client to run the main JavaScript method. This way, we do not have to call the main method of the client directly which is better if we want to refactor the client

## [0.0.1] - 2016-09-04
### Added
- Code for starting a [Spray](http://spray.io/) server that serves a website where the user can list files from the server.
- README.md that explains how to start the server
- LICENSE containing the [BSD 2-Clause License](https://opensource.org/licenses/BSD-2-Clause)

