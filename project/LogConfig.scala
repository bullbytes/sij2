/**
  * Configures the logging behavior for this project.
  */
object LogConfig {

  val logDir= "/logs"
}
