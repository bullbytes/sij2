/**
  * Configuration for this project.
  */
object BuildConfig {
  val appVersion = "0.0.7"

  /**
    * SkeletonPage.scala uses this name. If you change it here, do `sbt clean` and change it in
    * SkeletonPage.scala as well
    */
  val appName = "sij"
}