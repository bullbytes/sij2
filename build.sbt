scalaVersion in ThisBuild := ScalaConfig.version
//scalaOrganization in ThisBuild := ScalaConfig.organization

scalacOptions in ThisBuild := ScalaConfig.CompilerOptions.value

// Set the version for all projects in this build
version in ThisBuild := BuildConfig.appVersion

lazy val root = project.in(file(".")).
  aggregate(client, server).
  settings(
    name := "Sij root"
  )

val appName = BuildConfig.appName
def inDevMode = sys.props.get("dev.mode").exists(value => value.equalsIgnoreCase("true"))

// We can run this project by calling appJS/run and appJVM/run. Enter `projects` in SBT to see the name of all projects
lazy val app = crossProject.in(file("./app")).
  settings(
    name := appName
  ).
  jvmSettings(
    libraryDependencies ++= Dependencies.server.value,
    javaOptions in Universal ++= RuntimeConfig.debugOptions(inDevMode),
    javaOptions in Universal ++= RuntimeConfig.javaRuntimeOptions,

    // Don't generate ScalaDoc when we are in development mode
    publishArtifact in(Compile, packageDoc) := !inDevMode,
    publishArtifact in packageDoc := !inDevMode,

    imageNames in docker := {

      val gitLabRepository = "registry.gitlab.com/bullbytes/sij2"
      // Tag the image with the value of the environment variable provided by GitLab continuous integration
      val defaultTag = "latest"
      val imageTag = sys.env.getOrElse("CI_BUILD_REF", defaultTag)
      Seq(
        ImageName(
          //namespace = Some(organization.value),
          repository = gitLabRepository,
          tag = Some(imageTag)
        )
      )
    },
    dockerfile in docker := {
      val appDir = stage.value
      val targetDir = "/app"
      new Dockerfile {
        from("java:8-jre")
        entryPoint(s"$targetDir/bin/${executableScriptName.value}")

        copy(appDir, targetDir)
        expose(8080)

        // Use a volume to keep the logs between container invocations
        run("mkdir", "-p", LogConfig.logDir)
        volume(LogConfig.logDir)
      }
    }

    /**
      * We enable JavaAppPackaging to create a jar. Also, this gives us access to the variables stage and executableScriptName.
      * Issuing "appJVM/docker" in SBT creates a Docker image from that jar.
      */
  ).enablePlugins(JavaAppPackaging, sbtdocker.DockerPlugin).
  jsSettings(

    libraryDependencies ++= Dependencies.client.scalaJsDependencies.value,
    jsDependencies ++= Dependencies.client.jsDependencies.value,

    // Include the JavaScript dependencies
    skip in packageJSDependencies := false,

    // Use the faster Node.js instead of Rhino. Get Node.js from here: https://nodejs.org
    //scalaJSUseRhino in Global := false

    // Regardless of whether we optimize the created JavaScript fast or fully, produce a .js file with the same name.
    // This way, we don't have to adapt the name of the script to load at the client when we switch optimize modes
    artifactPath in Compile in fastOptJS := (crossTarget in fastOptJS).value / ((moduleName in fastOptJS).value + ".js"),
    artifactPath in Compile in fullOptJS := (crossTarget in fullOptJS).value / ((moduleName in fullOptJS).value + ".js"),

    persistLauncher in Compile := true,
    persistLauncher in Test := false
  )

// We need to define the subprojects. Note that the names of these vals do not affect how you run the subprojects:
// It will be `<nameOfCrossProject>JS/run` and `<nameOfCrossProject>JVM/run`, irrespective of how these vals are named
lazy val client = app.js.settings(
  // Attempt to make Settings.scala known to client code
  //unmanagedSourceDirectories in Compile += baseDirectory.value / "project"

)

/**
  * Adds the compiled JavaScript to the server's resources since the server sends the JavaScript to the client
  *
  * @return a sequence of files that consists of our generated JavaScript file. Wrapped in a setting task for SBT
  */
def addJavaScriptToServerResources() = {
  if (inDevMode) {
    (resources in Compile) += (fastOptJS in(client, Compile)).value.data
  } else {
    (resources in Compile) += (fullOptJS in(client, Compile)).value.data
  }
}
def addJSDependenciesToServerResources() = {
  (resources in Compile) += (packageMinifiedJSDependencies in(client, Compile)).value
}
lazy val server = app.jvm.settings(
  addJavaScriptToServerResources(),
  addJSDependenciesToServerResources(),

  /*
    Setting 'persistLauncher' to true generates a small JavaScript launcher that calls the main method in the client.
    The server sends the launcher script to the client.
    See also: https://www.scala-js.org/tutorial/basic/#automatically-creating-a-launcher
  */
  (resources in Compile) += (packageScalaJSLauncher in(client, Compile)).value.data
)


