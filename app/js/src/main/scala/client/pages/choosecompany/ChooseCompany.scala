package client.pages.choosecompany

import client.sharedgui.{SharedElements, SharedHtmlClasses}
import client.utils.Navigation
import org.scalajs.dom._
import org.scalajs.dom.html.Div
import shared.articleformdata.MerchantOrSupplier
import shared.merchantsandsuppliers.MerchantsAndSuppliers
import shared.pages.PageInfo

import scalacss.ScalatagsCss._
import scalacss.internal.StyleA
import scalatags.JsDom.TypedTag
import scalatags.JsDom.all._

// Converts GuiStrings to ScalaTags' StringFrags
import client.sharedgui.GuiStringImplicits.guiString2StringFrag

/**
  * On this page, the user can choose the company they represent.
  * <p>
  * Created by Matthias Braun on 11/2/2016.
  */
object ChooseCompany {
  def create(): Node = {

    val body = document.body

    ChooseCompanyCss.addTo(document)

    body.appendChild(
      div(
        header,
        merchantAndSupplierButtons
      ).render
    )
  }

  private def header =
    div(SharedHtmlClasses.header)(
      h1(ChooseCompanyGuiStrings.chooseCompany),
      SharedElements.subtleSeparator
    )

  private def toGridCell(merchantOrSupplier: MerchantOrSupplier, buttonClass: StyleA) = {
    // When the user selects their company, we show the articles they've created
    val showArticleOverview = (_: Event) => Navigation.changeTo(merchantOrSupplier.id, PageInfo.ArticleOverview)

    div(ChooseCompanyClasses.gridCell)(button(SharedHtmlClasses.pureButton, buttonClass,
      onclick := showArticleOverview)(merchantOrSupplier.name))
  }

  private def merchants = {
    import ChooseCompanyClasses._
    def toMerchantGridCell: (MerchantOrSupplier) => TypedTag[Div] = toGridCell(_, merchantButton)

    div(grid)(MerchantsAndSuppliers.merchants.map(toMerchantGridCell))
  }

  private def suppliers = {
    import ChooseCompanyClasses._
    def toSupplierGridCell: (MerchantOrSupplier) => TypedTag[Div] = toGridCell(_, supplierButton)

    div(grid)(MerchantsAndSuppliers.suppliers.map(toSupplierGridCell))

  }

  private def merchantAndSupplierButtons =
    div(ChooseCompanyClasses.merchantsAndSuppliers)(
      h2(ChooseCompanyGuiStrings.merchants),
      merchants,
      SharedElements.strongSeparator,
      h2(ChooseCompanyGuiStrings.suppliers),
      suppliers
    )
}
