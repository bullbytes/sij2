package client.pages.allarticles

import shared.articleformdata._
import shared.data.sortarticlespage.ArticleForSortArticlesPage

/**
  * Creates copies of an [[ArticleForSortArticlesPage]] with updated fields.
  */
object ArticleUpdater {

  def articleWithNewArticleName(oldArticle: ArticleForSortArticlesPage, newArticleName: String): ArticleForSortArticlesPage = {
    oldArticle.copy(articleName = newArticleName)
    //    val oldArticleData = oldArticle.articleFormData.articleData
    //    val newArticleData = oldArticleData.copy(name = newArticleName)
    //    withNewArticleData(oldArticle, newArticleData)
  }

  def articleWithNewDescription(oldArticle: ArticleForSortArticlesPage, newDescription: String): ArticleForSortArticlesPage = {
    oldArticle.copy(articleDescription = newDescription)

    //    val oldArticleData = oldArticle.articleFormData.articleData
    //    val newArticleData = oldArticleData.copy(description = newDescription)
    //    withNewArticleData(oldArticle, newArticleData)
  }

  def articleWithNewArticleNrInCatalog(oldArticle: ArticleForSortArticlesPage, newArticleNr: String): ArticleForSortArticlesPage = {

    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(articleNrInCatalog = newArticleNr)
    oldArticle.copy(catalogData = newCatalogData)


    //    val oldCatalogData = oldArticle.articleFormData.catalogData
    //    val newCatalogData = oldCatalogData.copy(articleNrInCatalog = newArticleNr)
    //
    //    withNewCatalogData(oldArticle, newCatalogData)
  }

  def articleWithNewArticleNrUsedByMerchantOrSupplier(oldArticle: ArticleForSortArticlesPage, newArticleNr: String): ArticleForSortArticlesPage = {

    oldArticle.copy(articleNrAssignedByPartner = newArticleNr)

    //    val oldArticleData = oldArticle.articleFormData.articleData
    //    val newArticleData = oldArticleData.copy(articleNumberAssignedByMerchantOrSupplier = newArticleNr)
    //    withNewArticleData(oldArticle, newArticleData)
  }


  def withNewPositionInCatalog(oldArticle: ArticleForSortArticlesPage, newPosition: Int): ArticleForSortArticlesPage = {
    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(positionInCatalog = newPosition)
    oldArticle.copy(catalogData = newCatalogData)

//    val oldCatalogData = oldArticle.articleFormData.catalogData
//    val newCatalogData = oldCatalogData.copy(positionInCatalog = newPosition)
//
//    withNewCatalogData(oldArticle, newCatalogData)
  }

  def articleWithNewRemarksOnCatalogCreation(oldArticle: ArticleForSortArticlesPage, newRemarks: String): ArticleForSortArticlesPage = {
    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(remarksOnCatalogCreation = newRemarks)
    oldArticle.copy(catalogData = newCatalogData)

//    val oldCatalogData = oldArticle.articleFormData.catalogData
//    val newCatalogData = oldCatalogData.copy(remarksOnCatalogCreation = newRemarks)
//
//    withNewCatalogData(oldArticle, newCatalogData)
  }

  def withNewPageInCatalog(oldArticle: ArticleForSortArticlesPage, newPage: Int): ArticleForSortArticlesPage = {
    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(pageInCatalog = newPage)
    oldArticle.copy(catalogData = newCatalogData)

//    val oldCatalogData = oldArticle.articleFormData.catalogData
//    val newCatalogData = oldCatalogData.copy(pageInCatalog = newPage)
//
//    withNewCatalogData(oldArticle, newCatalogData)
  }

//  private def withNewCatalogData(oldArticle: ArticleForSortArticlesPage, newCatalogData: CatalogData): ArticleForSortArticlesPage = {
//
//    val newFormData = oldArticle.articleFormData.copy(catalogData = newCatalogData)
//
//    oldArticle.copy(articleFormData = newFormData)
//  }

//  private def withNewArticleData(oldArticle: ArticleForSortArticlesPage, newArticleData: PromotionalArticle): ArticleForSortArticlesPage = {
//    val newFormData = oldArticle.articleFormData.copy(articleData = newArticleData)
//    oldArticle.copy(articleFormData = newFormData)
//  }

  def articleWithNewSubcategory(oldArticle: ArticleForSortArticlesPage, newSubcategory: CatalogSubCategory): ArticleForSortArticlesPage = {
    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(subCategory = newSubcategory)
    oldArticle.copy(catalogData = newCatalogData)

//    val oldCatalogData = oldArticle.articleFormData.catalogData
//    val newCatalogData = oldCatalogData.copy(subCategory = newSubcategory)
//
//    withNewCatalogData(oldArticle, newCatalogData)
  }

  /**
    * Creates a new [[ArticleForSortArticlesPage]] with a `newCategory` from the `oldArticle`.
    */
  def articleWithNewCategory(oldArticle: ArticleForSortArticlesPage, newCategory: CatalogCategory): ArticleForSortArticlesPage = {
    val oldCatalogData = oldArticle.catalogData
    val newCatalogData = oldCatalogData.copy(category = newCategory)
    oldArticle.copy(catalogData = newCatalogData)

//    val oldCatalogData = oldArticle.articleFormData.catalogData
//    val newCatalogData = oldCatalogData.copy(category = newCategory)
//
//    withNewCatalogData(oldArticle, newCatalogData)
  }
}