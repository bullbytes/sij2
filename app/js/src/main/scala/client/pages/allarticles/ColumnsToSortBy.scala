package client.pages.allarticles

/**
  * The user can sort the articles inside the table of [[SortArticles]] by these column values.
  * <p>
  * Created by Matthias Braun on 12/16/2016.
  */
object ColumnsToSortBy {


  // First sort by the article's category and then by its subcategory
  val categoryAndSubcategory = "category-and-subcategory"

  // First sort by the article's category and then by its position in the catalog
  val categoryAndPosition = "category-and-position-in-catalog"

  val merchantOrSupplier = "merchant-or-supplier"
}
