package client.pages.allarticles

import client.sharedgui.SharedElements
import client.utils.Ajaxer
import client.utils.gui.Elements
import org.scalajs.dom
import org.scalajs.dom._
import org.scalajs.dom.html.Button
import shared.files.SijFile
import shared.pages.{GuiString, HtmlId}
import slogging.LazyLogging

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Promise}
import scalatags.JsDom.all._

/**
  * Lets us upload articles as JSON to the server.
  * <p>
  * Created by Matthias Braun on 4/5/2017.
  */
object ImportJsonArticles extends LazyLogging {
  private val inputId = HtmlId("upload-json-input-id")

  private val chooseJsonFileInput = {
    val labelText = GuiString("Choose JSON file with articles: ")
    div(
      label(`for` := inputId.value)(labelText.value),
      input(id := inputId.value, `type` := "file")
    )
  }

  private def toSijFile(fileToUpload: dom.File): Future[Either[DOMError, SijFile]] = readTextFile(fileToUpload)
    .map(errorOrContent => errorOrContent
      .map(fileContent => SijFile(fileToUpload.name, fileContent)))

  private def readTextFile(fileToRead: dom.File): Future[Either[DOMError, String]] = {
    // Used to create the return value of this method
    val promisedErrorOrContent = Promise[Either[DOMError, String]]

    val reader = new FileReader()
    reader.readAsText(fileToRead, "UTF-8")

    reader.onload = (_: UIEvent) => {
      val resultAsString = s"${reader.result}"
      promisedErrorOrContent.success(Right(resultAsString))
    }
    reader.onerror = (_: Event) => promisedErrorOrContent.success(Left(reader.error))

    promisedErrorOrContent.future
  }

  private val uploadJsonFileButton: Button = {
    val labelText = GuiString("Upload file with articles")

    def onClick: Event => Unit = (_: Event) => {
      logger.info("Upload button clicked")
      val files = Elements.getFilesOfInput(inputId)
      val fileToUploadMaybe: Option[File] = if (files.isEmpty) {
        logger.info("The file input doesn't seem to contain any files")
        None
      }
      else if (files.size > 1) {
        logger.warn("The file input contains multiple files; getting first")
        files.headOption
      } else {
        files.headOption
      }
      fileToUploadMaybe
        .foreach(fileToUpload => toSijFile(fileToUpload)
          .foreach {
            case Right(sijFile) => Ajaxer.saveArticlesFromJsonFile(sijFile)
            case Left(error) => logger.warn(s"Could not read file ${fileToUpload.name}. Error is: $error")
          })
    }

    button(`type` := "button", onclick := onClick)(labelText.value).render
  }

  def create() = {
    Elements.append(div(h1("Import articles in JSON format"), SharedElements.subtleSeparator))
    Elements.append(chooseJsonFileInput)
    Elements.append(uploadJsonFileButton)
  }
}
