package client.pages.allarticles.table

import shared.articles.categories.ArticleCategories.ArticleCategory
import shared.articles.categories.CategoryIds.CategoryId
import shared.articles.categories.subcategories.{ArticleSubcategories, SubcategoryGuiStrings}
import shared.articles.categories.subcategories.ArticleSubcategories.{ArticleSubcategory, SubcategoryId}
import client.pages.allarticles.{AllArticlesClasses, ArticleTableModel, ArticleUpdater}
import client.utils.gui.Elements
import client.utils.gui.inputs.{TextAreas, TextInputs}
import client.utils.gui.selects.{Selects, SelectsValue}
import org.scalajs.dom.html.{Select, TableRow}
import shared.articleformdata._
import shared.articles.categories.{ArticleCategories, CategoryGuiStrings}
import shared.data.sortarticlespage.ArticleForSortArticlesPage
import shared.pages.GuiString
import slogging.LazyLogging

import scalacss.ScalatagsCss._
import scalatags.JsDom.all._

/**
  * Creates rows within [[ArticleTables]].
  * <p>
  * Created by Matthias Braun on 12/16/2016.
  */
object ArticleTableRows extends LazyLogging {

  private def toSelectsValue(subcategory: ArticleSubcategory): SelectsValue = SelectsValue(subcategory.id.value)

  private def selectsValueToSubcategory(selectsValue: SelectsValue): ArticleSubcategory =
  ArticleSubcategory(SubcategoryId(selectsValue.value))

  private def toSelectsValue(category: ArticleCategory): SelectsValue = SelectsValue(category.id.value)

  private def subcategoriesFor(category: ArticleCategory): Seq[(SelectsValue, GuiString)] =
    ArticleSubcategories.getFor(category.id)
      .map(subcategory => (toSelectsValue(subcategory), SubcategoryGuiStrings.subcategory(subcategory.id)))

  def categoryNameToCategory(categoryName: GuiString): Option[ArticleCategory] =
    if (categoryName.value.isEmpty)
      Some(ArticleCategories.none)
    else
      ArticleCategories.fromName(categoryName)

  private def subcategoryNameToSubcategory(subcategoryName: GuiString): ArticleSubcategory =
    ArticleSubcategories.fromName(subcategoryName).getOrElse(ArticleSubcategories.none)


  private def subcategoriesDropDown(catalogData: CatalogData, id: ArticleId): Select = {
    val categoryOfArticle = categoryNameToCategory(GuiString(catalogData.category.name))
      .getOrElse(ArticleCategories.none)

    val subcategoryOfArticle = subcategoryNameToSubcategory(GuiString(catalogData.subCategory.name))
    val subcategories = subcategoriesFor(categoryOfArticle)

    // If the article doesn't have a subcategory assigned to it, we assign it the first subcategory of the category
    val subcategoryToSelect = if (subcategoryOfArticle == ArticleSubcategories.none) {
      val maybeSubcategory = categoryOfArticle.subcategories.headOption
      //logger.info(s"First subcategory of $categoryOfArticle is $maybeSubcategory")
      maybeSubcategory.getOrElse(ArticleSubcategories.none)
    }
    else subcategoryOfArticle

    ArticleTableModel.updateSubcategory(subcategoryToSelect, id)

    Selects.dropDown(subcategories, optionToSelect = toSelectsValue(subcategoryToSelect),
      (value: SelectsValue) => {
        logger.info(s"Subcategory $value was selected")
        ArticleTableModel.updateSubcategory(selectsValueToSubcategory(value), id)
      })
  }

  private def dropDownForCategories(catalogData: CatalogData, id: ArticleId,
                                    changeSubcategory: (SelectsValue) => Unit): Select = {

    // The category which is currently assigned to the article
    val categoryName = GuiString(catalogData.category.name)
    val currentCategory = categoryNameToCategory(categoryName).getOrElse(ArticleCategories.none)

    val updateCategoryAndChangeSubcategories = (selectedCategoryId: SelectsValue) => {
      ArticleTableModel.updateCategory(CategoryId(selectedCategoryId.value), id)
      changeSubcategory(selectedCategoryId)
    }

    // The categories as pairs of selects values and GUI strings for the drop-down list
    val articleCategories: Seq[(SelectsValue, GuiString)] = ArticleCategories.all
      .map(category => (SelectsValue(category.id.value), CategoryGuiStrings.nameFromId(category.id)))

    Selects.dropDown(articleCategories, optionToSelect = toSelectsValue(currentCategory),
      onChange = updateCategoryAndChangeSubcategories)
  }

  /**
    * Creates a table row from the given `article`.
    *
    * @param article  the [[ArticleWithMetaInfo]] whose data we'll put in the row.
    * @param rowIndex the index of this row within the table. Used to make odd and even rows visually distinct
    * @return the created [[TableRow]] for the [[ArticleTables]]
    */
  def create(article: ArticleForSortArticlesPage, rowIndex: Int): TableRow = {

    val articleId = article.articleId
    val catalogData = article.catalogData

    // Contains the subcategories of the currently selected category. Initially, show the subcategories of the first category
    val divWithSubcategories = div(subcategoriesDropDown(catalogData, articleId)).render

    // When the user changes the category we change the shown subcategories to match the new category
    def changeSubcategories(selectedCategoryId: SelectsValue) = {
      val newCategoryId = CategoryId(selectedCategoryId.value)
      ArticleCategories.get(newCategoryId)
        .fold {
          logger.warn(s"Unknown category ID '$newCategoryId'")
        } {
          newCategory =>
            Elements.removeAllChildren(divWithSubcategories)
            val changedArticle = ArticleUpdater.articleWithNewCategory(article, ArticleTableModel.toCatalogCategory(newCategory))
            val newDropDown = subcategoriesDropDown(changedArticle.catalogData, articleId)
            divWithSubcategories.appendChild(newDropDown)
        }
    }

    // We have visually distinct rows so the user doesn't accidentally mix them up
    val rowClass = if (rowIndex % 2 == 0) AllArticlesClasses.evenRow else AllArticlesClasses.oddRow

    def updatePositionInCatalog(newPosition: String) = ArticleTableModel.updatePositionInCatalog(newPosition, articleId)

    def updatePageInCatalog(newPage: String) = ArticleTableModel.updatePageInCatalog(newPage, articleId)

    def updateArticleNrInCatalog(newArticleNr: String) = ArticleTableModel.updateArticleNrInCatalog(newArticleNr, articleId)

    def updateArticleName(newArticleName: String) = ArticleTableModel.updateArticleName(newArticleName, articleId)

    def updateArticleNrByMerchantOrSupplier(newArticleNr: String) =
      ArticleTableModel.updateArticleNrByMerchantOrSupplier(newArticleNr, articleId)

    def updateArticleDescription(newDescription: String) =
      ArticleTableModel.updateArticleDescription(newDescription, articleId)

    def updateRemarksOnCatalogCreation(newRemarks: String) =
      ArticleTableModel.updateRemarksOnCatalogCreation(newRemarks, articleId)

    import client.pages.allarticles.table.{ArticleTableGuiStrings => Strings, ArticleTableIds => Ids}
    tr(rowClass)(
      td(TextInputs.shortWithPlaceholder(Ids.pageInCatalogOfArticle, Strings.pageInCatalogOfArticle,
        GuiString(catalogData.pageInCatalog.toString), updatePageInCatalog)),
      td(TextInputs.withPlaceholder(Ids.articleNrInCatalog, Strings.articleNrInCatalog,
        GuiString(catalogData.articleNrInCatalog), updateArticleNrInCatalog)),
      td(dropDownForCategories(catalogData, articleId, changeSubcategories)),
      td(divWithSubcategories),
      td(TextInputs.shortWithPlaceholder(Ids.positionInCatalog, Strings.positionInCatalog,
        GuiString(catalogData.positionInCatalog.toString), updatePositionInCatalog)),
      td(article.merchantOrSupplier.name),
      td(TextInputs.longWithPlaceholderInRow(Ids.articleName, Strings.articleName, GuiString(article.articleName), updateArticleName)),
      td(TextAreas.bigWithPlaceholderInRow(Ids.articleDescription, Strings.articleDescription,
        GuiString(article.articleDescription), updateArticleDescription)),
      td(TextAreas.bigWithPlaceholderInRow(Ids.remarksForCatalogCreation, Strings.remarksForCatalogCreation,
        GuiString(catalogData.remarksOnCatalogCreation), updateRemarksOnCatalogCreation)),
      td(TextInputs.withPlaceholder(Ids.articleNrByMerchantOrSupplier, Strings.articleNrByMerchantOrSupplier,
        GuiString(article.articleNrAssignedByPartner), updateArticleNrByMerchantOrSupplier))
    ).render
  }
}
