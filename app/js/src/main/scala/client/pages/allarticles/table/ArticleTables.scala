package client.pages.allarticles.table

import client.pages.allarticles.{AllArticlesClasses, AllArticlesGuiStrings}
import shared.utils.conversions.RichSeq.SeqUsingIndex
import org.scalajs.dom.html.Table
import shared.articleformdata.ArticleWithMetaInfo
import shared.data.sortarticlespage.ArticleForSortArticlesPage
import slogging.LazyLogging

import scalacss.ScalatagsCss._
import scalatags.JsDom.all._

/**
  * Produces tables that show the data of promotional articles.
  * <p>
  * Created by Matthias Braun on 12/10/2016.
  */
object ArticleTables extends LazyLogging {
  def emptyTable(): Table = create(Seq())

  def create(articles: Seq[ArticleForSortArticlesPage]): Table =
    table(AllArticlesClasses.articleTable)(tableHead, tableBody(articles)).render

  private def tableHead =
  // Turn every table head GUI string into a table head
    thead(tr(AllArticlesGuiStrings.tableHeaders.map(header => th(header.value))))

  private def tableBody(articles: Seq[ArticleForSortArticlesPage]) =
  // Turn every article into a table row. Use the article's index to make the odd and even rows visually distinct
    tbody(articles.mapWithIndex(ArticleTableRows.create))
}
