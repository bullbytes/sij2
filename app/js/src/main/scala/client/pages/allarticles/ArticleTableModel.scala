package client.pages.allarticles

import client.utils.Ajaxer
import shared.articleformdata._
import shared.articles.categories.ArticleCategories.ArticleCategory
import shared.articles.categories.CategoryIds.CategoryId
import shared.articles.categories.subcategories.ArticleSubcategories.ArticleSubcategory
import shared.articles.categories.subcategories.SubcategoryGuiStrings
import shared.articles.categories.{ArticleCategories, CategoryGuiStrings}
import shared.data.sortarticlespage.ArticleForSortArticlesPage
import shared.pages.GuiString
import shared.serverresponses.sortarticlespage.{OfferArticlesToSortArticlesPage, ServerError}
import shared.utils.ParseUtil
import slogging.LazyLogging

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

/**
  * Provides access to the current articles in the articles table of [[SortArticles]] page.
  * <p>
  * Created by Matthias Braun on 12/19/2016.
  */
object ArticleTableModel extends LazyLogging {

  private var currentArticles: Map[ArticleId, ArticleForSortArticlesPage] = Map()

  // The articles in the table can be sorted in different ways. Per default, this is the alphabetical order of the article's name
  private var currentOrdering: Ordering[ArticleForSortArticlesPage] = Ordering.by(_.articleName)

  def sortArticles(ordering: Ordering[ArticleForSortArticlesPage]): Seq[ArticleForSortArticlesPage] = {

    // Set the new ordering
    currentOrdering = ordering

    // getCurrentArticles uses the current ordering to sort the articles
    getCurrentArticles
  }

  /**
    * @return the current articles in the table. The articles are sorted by the current ordering which can be set by
    *         calling [[sortArticles]]
    */
  def getCurrentArticles: Seq[ArticleForSortArticlesPage] = currentArticles.values.toSeq.sorted(currentOrdering)

  /**
    * Creates a map from the given `articles` by using their ID as the key
    */
  private def indexedById(articles: Seq[ArticleForSortArticlesPage]) =
    articles.map(article => article.articleId -> article).toMap

  def updatePageInCatalog(newPage: String, articleId: ArticleId): Unit =
    currentArticles.get(articleId).fold {
      logger.warn(s"Can't assign new page in catalog to article with ID: $articleId since it's not in the model")
    } {
      articleToChange =>
        ParseUtil.parseInt(newPage).fold {
          logger.info(s"The new article's page in the catalog doesn't seem to be an integer: '$newPage'")
        } {
          newPageAsInt =>
            val updatedArticle = ArticleUpdater.withNewPageInCatalog(articleToChange, newPageAsInt)
            // Replace the old article with the updated one
            currentArticles = currentArticles + (articleId -> updatedArticle)
        }
    }

  def updatePositionInCatalog(newPosition: String, articleId: ArticleId): Unit = currentArticles.get(articleId).fold {
    logger.warn(s"Can't assign new position in catalog to article with ID: $articleId since it's not in the model")

  } {
    articleToChange =>
      ParseUtil.parseInt(newPosition).fold {
        logger.info(s"The new article's position in the catalog doesn't seem to be an integer: '$newPosition'")
      } {
        newPositionAsInt =>
          val updatedArticle = ArticleUpdater.withNewPositionInCatalog(articleToChange, newPositionAsInt)
          // Replace the old article with the updated one
          currentArticles = currentArticles + (articleId -> updatedArticle)
      }
  }

  /**
    * In the database we use the class [[CatalogCategory]] which is simpler than the [[ArticleCategory]] we use for
    * the most part. Since the database has gone live we can't replace [[CatalogCategory]] with [[ArticleCategory]].
    * But we can convert an [[ArticleCategory]] to a [[CatalogCategory]] using this method.
    */
  def toCatalogCategory(articleCategory: ArticleCategory): CatalogCategory = {
    val newCategoryName = CategoryGuiStrings.nameFromId(articleCategory.id)
    CatalogCategory(newCategoryName.value)
  }

  def updateArticleNrInCatalog(newArticleNr: String, articleId: ArticleId): Unit =
    currentArticles.get(articleId).fold {
      logger.warn(s"Can't assign new number (for catalog) to article with ID: $articleId since it's not in the model")
    } {
      articleToChange =>
        val updatedArticle = ArticleUpdater.articleWithNewArticleNrInCatalog(articleToChange, newArticleNr)
        // Replace the old article with the updated one
        currentArticles = currentArticles + (articleId -> updatedArticle)
    }

  def updateArticleDescription(newDescription: String, articleId: ArticleId): Unit =
    currentArticles.get(articleId).fold {
      logger.warn(s"Can't assign new description to article with ID: $articleId since it's not in the model")
    } {
      articleToChange =>
        val updatedArticle = ArticleUpdater.articleWithNewDescription(articleToChange, newDescription)
        // Replace the old article with the updated one
        currentArticles = currentArticles + (articleId -> updatedArticle)
    }

  def updateRemarksOnCatalogCreation(newRemarks: String, articleId: ArticleId): Unit =
    currentArticles.get(articleId).fold {
      logger.warn(s"Can't assign new remarks on catalog creation to article with ID: $articleId since it's not in the model")
    } {
      articleToChange =>
        val updatedArticle = ArticleUpdater.articleWithNewRemarksOnCatalogCreation(articleToChange, newRemarks)
        // Replace the old article with the updated one
        currentArticles = currentArticles + (articleId -> updatedArticle)
    }

  def updateArticleNrByMerchantOrSupplier(newArticleNr: String, articleId: ArticleId): Unit =
    currentArticles.get(articleId).fold {
      logger.warn(s"Can't assign new number to article (as used by merchant/supplier) with ID: $articleId since it's not in the model")
    } {
      articleToChange =>
        val updatedArticle = ArticleUpdater.articleWithNewArticleNrUsedByMerchantOrSupplier(articleToChange, newArticleNr)
        // Replace the old article with the updated one
        currentArticles = currentArticles + (articleId -> updatedArticle)
    }

  def updateArticleName(newArticleName: String, articleId: ArticleId): Unit =
    currentArticles.get(articleId).fold {
      logger.warn(s"Can't assign new name to article with ID: $articleId since it's not in the model")
    } {
      articleToChange =>
        val updatedArticle = ArticleUpdater.articleWithNewArticleName(articleToChange, newArticleName)
        // Replace the old article with the updated one
        currentArticles = currentArticles + (articleId -> updatedArticle)
    }

  def updateCategory(newCategory: ArticleCategory, articleId: ArticleId): Unit =
    currentArticles.get(articleId).fold {
      logger.warn(s"Can't assign new category to article with ID: $articleId since it's not in the model")
    } {
      articleToChange =>
        val updatedArticle = ArticleUpdater.articleWithNewCategory(articleToChange, toCatalogCategory(newCategory))
        // Replace the old article with the updated one
        currentArticles = currentArticles + (articleId -> updatedArticle)
    }

  def updateCategory(newCategoryId: CategoryId, articleId: ArticleId): Unit =
    ArticleCategories.get(newCategoryId).fold {
      logger.warn(s"Can't assign new category for article with ID '$articleId' since category ID '$newCategoryId' is unknown")
    } {
      newCategory =>
        updateCategory(newCategory, articleId)
    }

  def updateSubcategory(newSubcategory: ArticleSubcategory, articleId: ArticleId): Unit =
    currentArticles.get(articleId).fold {
      logger.warn(s"Can't assign new subcategory to article with ID: $articleId since it's not in the model")
    } {
      articleToChange =>
        val newSubcategoryName = SubcategoryGuiStrings.subcategory(newSubcategory.id)

        val updatedArticle = ArticleUpdater.articleWithNewSubcategory(articleToChange, CatalogSubCategory(newSubcategoryName.value))
        // Replace the old article with the updated one
        currentArticles = currentArticles + (articleId -> updatedArticle)
      //logger.info(s"New subcategory of '$articleId': $newSubcategory")
    }

  type FutureErrorOrArticles = Future[Either[GuiString, Seq[ArticleForSortArticlesPage]]]

  def loadArticles: FutureErrorOrArticles =
    Ajaxer.getLatestArticlesForSortArticlesPage.map {
      case OfferArticlesToSortArticlesPage(latestArticles) =>
        currentArticles = indexedById(latestArticles)
        Right(latestArticles)

      case ServerError(msg) =>
        Left(GuiString(msg.value))

      case other =>
        Left(AllArticlesGuiStrings.unexpectedServerResponse(other))
    }
}
