package client.pages.allarticles

import client.pages.articleform.ArticleFormIcons
import org.scalajs.dom.html.Element

import scalatags.JsDom.TypedTag

/**
  * Icons shown on the "all articles" page.
  * <p>
  * Created by Matthias Braun on 12/16/2016.
  */
object AllArticlesIcons {
  val save: TypedTag[Element] = ArticleFormIcons.send

}
