package client.pages.allarticles

import client.pages.allarticles.table.ArticleTables
import client.sharedgui.GuiStringImplicits.guiString2StringFrag
import client.sharedgui._
import client.utils.gui.Elements
import client.utils.gui.inputs.Buttons
import client.utils.gui.selects.{Selects, SelectsValue}
import client.utils.{Ajaxer, Navigation}
import org.scalajs.dom._
import org.scalajs.dom.html.Div
import shared.SijRestApi.Paths
import shared.articleformdata.{ArticleWithMetaInfo, CatalogCategory}
import shared.articles.ArticleOrderings
import shared.articles.categories.ArticleCategories
import shared.articles.categories.subcategories.ArticleSubcategories
import shared.data.sortarticlespage.ArticleForSortArticlesPage
import shared.pages.GuiString
import shared.serverresponses._
import slogging.LazyLogging

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.scalajs.js.Date
import scalacss.ScalatagsCss._
import scalatags.JsDom.all._

/**
  * Lets the user define in which order the articles should appear in the catalog.
  * Also, the user can assign categories ("office") and subcategories ("pens") to the articles on this page.
  * <p>
  * Created by Matthias Braun on 12/10/2016.
  */
object SortArticles extends LazyLogging {

  private def addHeader() = Elements.append(
    div(SharedHtmlClasses.header)(h1(AllArticlesGuiStrings.pageTitle), SharedElements.subtleSeparator)
  )

  /**
    * The user can sort the articles by these columns of the table.
    */
  private val columnsToSortBy: Seq[(SelectsValue, GuiString)] = Seq(
    (SelectsValue(ColumnsToSortBy.categoryAndSubcategory), AllArticlesGuiStrings.articleCategoryAndSubcategory),
    (SelectsValue(ColumnsToSortBy.categoryAndPosition), AllArticlesGuiStrings.articleCategoryAndPosition),
    (SelectsValue(ColumnsToSortBy.merchantOrSupplier), AllArticlesGuiStrings.supplierOrMerchant)
  )


  /**
    * When we sort an article by its category and its subcategory we use the orderings defined by VÖW.
    */
  val byCategoryAndSubcategory: Ordering[ArticleForSortArticlesPage] = Ordering.by(article =>
    (categorySortNr(article), subcategorySortNr(article)))

  val byCategoryAndPosition: Ordering[ArticleForSortArticlesPage] = Ordering.by(article =>
    (categorySortNr(article), article.catalogData.positionInCatalog))

  val byMerchantOrSupplierName: Ordering[ArticleForSortArticlesPage] = Ordering.by(_.merchantOrSupplier.name)

  private def categorySortNr(article:ArticleForSortArticlesPage) = ArticleCategories.sortNr(article.catalogData.category)
  private def subcategorySortNr(article: ArticleForSortArticlesPage) = ArticleSubcategories.sortNr(article.catalogData.subCategory)

  private def getOrdering(newColumn: SelectsValue): Ordering[ArticleForSortArticlesPage] =
    newColumn.value match {
      case ColumnsToSortBy.categoryAndSubcategory => byCategoryAndSubcategory

      case ColumnsToSortBy.categoryAndPosition => byCategoryAndPosition

      case ColumnsToSortBy.merchantOrSupplier => byMerchantOrSupplierName

      case other =>
        logger.warn(s"Don't know how to sort articles by '$other'. Sorting by merchant/supplier name as fallback")
        byMerchantOrSupplierName
    }


  private def sortArticlesGui(divWithTable: Div) =
    div(
      span(AllArticlesGuiStrings.sortBy),

      // When the user selects a new column by which to sort the articles, create a new table containing the sorted
      // articles and place the table in the surrounding div
      Selects.dropDown(columnsToSortBy, onChange = (newColumnToSortBy: SelectsValue) => {
        val sortedArticles = ArticleTableModel.sortArticles(getOrdering(newColumnToSortBy))
        updateTable(divWithTable, sortedArticles)
      }
      )
    )

  private def buttonForAssigningArticleNumbers(divWithArticleTable: Div) =
    Buttons.withText(AllArticlesGuiStrings.assignArticleNumbers,
      (_: Event) => assignArticleNumbers(divWithArticleTable), AllArticlesClasses.pureButton)

  private def getArticleNrForCatalog(category: CatalogCategory, index: Int): Option[String] = {
    // An article number should look like this: "TEX20170101". The first article is 101 and its number is padded to four digits
    ArticleCategories.abbreviationFor(category).map(abbreviation => {
      val articleNr = 101 + index
      // Pad to four digits
      val paddedArticleNr = f"$articleNr%04d"
      val yearOfCatalog = 2017

      s"$abbreviation$yearOfCatalog$paddedArticleNr"
    })
  }

  /**
    * Each article gets an article number for the catalog.
    * For example, if the article is the first one in the category "textile", it gets the number "TEX20170101".
    * That's composed of "[abbreviationOfCategory][YearOfCatalog][articleIndexWithinCategory+101].
    * The article index is defined by the article's current position in the table. The use can change this index by
    * sorting the articles in the table.
    * The article index starts anew for a new category: The first article in the category "technology" will have this
    * number "TEC20170101"
    */
  private def assignArticleNumbers(divWithArticleTable: Div) = {
    // Group the articles by their category
    ArticleTableModel.getCurrentArticles.groupBy(_.catalogData.category)
      .foreach { case (category, articlesInCategory) =>
        articlesInCategory.zipWithIndex.foreach { case (articleInCategory, articleIndexInCategory) =>
          // Only categorized articles can have an article number
          getArticleNrForCatalog(category, articleIndexInCategory).foreach(articleNr => {
            // Update the model and the GUI
            ArticleTableModel.updateArticleNrInCatalog(articleNr, articleInCategory.articleId)
            updateTable(divWithArticleTable, ArticleTableModel.getCurrentArticles)
          })
        }
      }
  }

  private def saveArticles(event: Event): Future[Either[ServerResponse, ServerStringForClient]] = {
    logger.info("Save articles in database")
    val eventualResponse = Ajaxer.saveNewVersionOfArticles(ArticleTableModel.getCurrentArticles)

    eventualResponse.map {
      case GotArticlesAndSavedThem(msg) =>
        logger.info(s"Server says it received the articles: $msg")
        Right(msg)
      case GotArticlesButCouldNotSave(msg) =>
        logger.info(s"Server says that it couldn't save the articles: $msg")
        Right(msg)
      case ServerError(msg) =>
        logger.warn(s"Server says something went wrong: $msg")
        Right(msg)
      case other =>
        logger.warn(s"Unexpected response from server: $other")
        Left(other)
    }
  }

  private def fillInfoBox(infoBox: Div, serverResponse: Either[ServerResponse, ServerStringForClient]): Unit = {
    val infoText = serverResponse.fold(unexpectedServerResponse => unexpectedServerResponse.toString,
      serverStringForClient => serverStringForClient.value)

    val currentTime = new Date().toLocaleTimeString()
    Elements.setChild(infoBox, span(s"$currentTime: $infoText").render)
    Elements.show(infoBox)
  }

  private def saveButton(infoBox: Div) = {
    def fillSpecificInfoBox(serverResponse: Either[ServerResponse, ServerStringForClient]) = fillInfoBox(infoBox, serverResponse)

    val saveArticlesAndFillInfoBox = (e: Event) => saveArticles(e).foreach(fillSpecificInfoBox)

    p(
      Buttons.withTextAndIcon(AllArticlesGuiStrings.saveChanges, AllArticlesIcons.save,
        onClick = saveArticlesAndFillInfoBox, AllArticlesClasses.saveButton)
    )
  }

  private def exportArticlesAsCsv(e: Event) = Navigation.changeTo(Paths.GetArticlesAsCsv)

  private def buttonForCsvExport =
    Buttons.withText(AllArticlesGuiStrings.exportAsCsv, exportArticlesAsCsv, SharedHtmlClasses.pureButton)

  private def addTableAndControls(): Node = {
    // Show the table head without its body and a loading animation until the articles are loaded from the server
    val divWithArticleTable: Div = div(ArticleTables.emptyTable()).render
    val loadingIcon = SharedIcons.loadingIcon

    // Load the articles from the server and show them in the table
    ArticleTableModel.loadArticles.foreach(errorOrArticles =>
      showArticlesOrError(errorOrArticles, divWithArticleTable, loadingIcon))

    // Contains information for the user from the server. Initially empty and invisible
    val infoBox = div(AllArticlesClasses.infoBox)().render
    Elements.hide(infoBox)

    Elements.append(
      div(AllArticlesClasses.tableAndControls)(
        sortArticlesGui(divWithArticleTable),

        p(
          buttonForAssigningArticleNumbers(divWithArticleTable),
          SharedElements.horizontalSpace,
          buttonForCsvExport
        ),

        div(divWithArticleTable, loadingIcon),

        infoBox,
        saveButton(infoBox)
      ))
  }


  def create(): Node = {

    AllArticlesCss.addTo(document)

    addHeader()

    addTableAndControls()
  }


  private def showArticlesOrError(articlesOrError: Either[GuiString, Seq[ArticleForSortArticlesPage]], divWithTable: Div,
                                  loadingIcon: Div) =
    articlesOrError.fold(error => {
      Elements.append(div(error))
      Elements.hide(loadingIcon)
    }, articles => {
      // Get the ordering for the first value of the drop down menu that lets the user sort the articles.
      // This way, when we initially show the articles, their ordering matches the label of the drop down menu
      val ordering = columnsToSortBy.headOption.map(_._1).fold {
        logger.warn("There is no first entry in the columns to sort by")
        ArticleOrderings.byMerchantOrSupplierName
      }(getOrdering)

      updateTable(divWithTable, articles.sorted(ordering))
      Elements.hide(loadingIcon)
    })

  /**
    * Shows the `articles` in the articles table which is inside the `divWithTable`.
    * This is a visual change only; to update the model with new data use [[ArticleTableModel]]
    */
  private def updateTable(divWithTable: Div, articles: Seq[ArticleForSortArticlesPage]) =
    Elements.setChild(divWithTable, ArticleTables.create(articles))
}
