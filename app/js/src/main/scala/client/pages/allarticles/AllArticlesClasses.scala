package client.pages.allarticles

import client.sharedgui.SharedHtmlClasses

import scalacss.internal.StyleA

/**
  * Contains the CSS classes we use for the [[client.pages.allarticles.SortArticles]].
  * <p>
  * Created by Matthias Braun on 12/10/2016.
  */
object AllArticlesClasses extends SharedHtmlClasses {


  val saveButton = Seq(largeButton, primaryButton, pureButton)

  val articleTable: StyleA = fromClassName("pure-table")
  val oddRow: StyleA = fromClassName("pure-table-odd")
  val evenRow: StyleA = fromClassName("even-row")

  val tableAndControls: StyleA = fromClassName("table-and-controls")

  val infoBox: StyleA = fromClassName("info-box")

}
