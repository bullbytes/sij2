package client.pages.allarticles

import shared.pages.GuiString
import shared.serverresponses.sortarticlespage.ServerResponseForSortArticlesPage

/**
  * These strings show up for the user to read on the [[SortArticles]] page.
  * <p>
  * Created by Matthias Braun on 12/10/2016.
  */
object AllArticlesGuiStrings {

  val exportAsCsv = GuiString("Als CSV-Datei exportieren")

  val pageTitle = GuiString("Artikel sortieren & kategorisieren")

  val sortBy = GuiString("Sortieren nach ")

  val articleCategoryAndSubcategory = GuiString("Kategorie und Subkategorie")
  val articleCategoryAndPosition = GuiString("Kategorie und Anordnung")
  val catalogPage = GuiString("Katalogseite")
  val articleNrInCatalog = GuiString("VÖW-Artikelnummer")
  val articleCategory = GuiString("Kategorie")
  val articleSubcategory = GuiString("Unterkategorie")
  val positionInCatalog = GuiString("Anordnung")
  val supplierOrMerchant = GuiString("Lieferant/Händler")
  val articleName = GuiString("Artikelbezeichnung")
  val articleDescription = GuiString("Artikelbeschreibung")
  val remarksOnCatalogCreation = GuiString("Kataloganmerkung")
  val articleNrAssignedBySupplierOrMerchant = GuiString("Artikelnummer von Lieferant/Händler")

  val saveChanges = GuiString("Änderungen speichern")
  val assignArticleNumbers = GuiString("VÖW-Artikelnummern vergeben")

  def unexpectedServerResponse(unexpectedResponse: ServerResponseForSortArticlesPage) =
    GuiString(s"Unerwartete Antwort des Servers: $unexpectedResponse")

  val tableHeaders: Seq[GuiString] = Seq(catalogPage, articleNrInCatalog, articleCategory, articleSubcategory,
    positionInCatalog, supplierOrMerchant, articleName, articleDescription, remarksOnCatalogCreation,
    articleNrAssignedBySupplierOrMerchant
  )
}
