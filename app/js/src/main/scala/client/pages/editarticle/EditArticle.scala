package client.pages.editarticle

import client.pages.articleform.{ArticleForm, ArticleFormCss, ArticleFormFiller}
import client.pages.shared.HiddenFieldsReader
import client.utils.gui.Elements
import client.utils.{Ajaxer, Navigation}
import org.scalajs.dom.{Event, _}
import shared.articleformdata.{ArticleFormIds, ArticleId, ArticleMetaInfo, ArticleWithMetaInfo}
import shared.pages.HiddenFields.HiddenFieldKeys
import shared.pages.PageInfo
import shared.serverresponses._
import slogging.LazyLogging

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scalatags.JsDom.all._

/**
  * Lets the user edit an article they have created in the past.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
object EditArticle extends LazyLogging {

  def create(): Option[Future[Any]] =
    HiddenFieldsReader.parseHiddenFields.get(HiddenFieldKeys.ArticleId).map(articleId =>

      Ajaxer.getLatestAndPreviousArticle(ArticleId(articleId)).map {
        case OfferLatestArticleWithPreviousVersion(latestArticle, prevArticleMaybe) => create(latestArticle, prevArticleMaybe)

        case ServerError(msg) => logger.warn(s"Server says something went wrong: $msg")
        case other => logger.warn(s"Unexpected answer from server: $other")
      }
    )

  private def sendArticleToServer(articleMetaInfo: ArticleMetaInfo) = {
    logger.info("Send the article data with the document's ID (from CouchDB) to the server")
    ArticleForm.data.fold {
      logger.warn("Couldn't get form data")
    } {
      newFormData =>
        logger.info(s"Sending form to server")
        val articleWithMetaInfo = ArticleWithMetaInfo(newFormData, articleMetaInfo)
        val merchantOrSupplierId = newFormData.merchantOrSupplier.id
        val eventualResponse = Ajaxer.saveNewVersionOfArticle(articleWithMetaInfo)
        eventualResponse.map {
          case GotArticleAndSavedIt(msg) =>
            Navigation.changeTo(merchantOrSupplierId, PageInfo.ArticleOverview)
          case GotArticleButCouldNotSave(msg) =>
            logger.info(s"Server says that it couldn't save the article: $msg")
            window.alert(msg.value)
          case DeadlineHasPassed(msg) =>
            logger.info(s"Server says that deadline has passed: $msg")
            window.alert(msg.value)
            Navigation.changeTo(merchantOrSupplierId, PageInfo.ArticleOverview)
          case ServerError(msg) =>
            logger.warn(s"Server says something went wrong: $msg")
          case other =>
            logger.warn(s"Unexpected response from server: $other")
        }
    }
  }

  private def create(latestArticle: ArticleWithMetaInfo, prevArticleMaybe: Option[ArticleWithMetaInfo]): Node = {
    logger.info("Creating page for editing article")

    ArticleFormCss.addTo(document)

    val onSubmit = (_: Event) => sendArticleToServer(latestArticle.metaInfo)
    val merchantOrSupplier = latestArticle.articleFormData.merchantOrSupplier
    val form = ArticleForm.create(onSubmit, merchantOrSupplier)

    // It's important that we first add the form to the document and then fill it out because we access elements
    // of the form via the document while filling out the form
    document.body.appendChild(form.render)

    prevArticleMaybe.fold(
      logger.info("There's no previous article version, this is the first version")
    )(prevArticle =>
      // There is a previous version of the article -> explain how we highlight changed data
      Elements.show(ArticleFormIds.fieldsWithNewDataAreHighlighted)
    )
    ArticleFormFiller.fillOut(form, latestArticle, prevArticleMaybe)
  }
}
