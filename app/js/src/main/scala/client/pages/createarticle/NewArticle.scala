package client.pages.createarticle

import client.pages.articleform.{ArticleForm, ArticleFormCss}
import client.pages.shared.HiddenFieldsReader
import client.utils.{Ajaxer, Navigation}
import org.scalajs.dom.{Event, _}
import shared.articleformdata.{MerchantOrSupplier, MerchantOrSupplierId}
import shared.merchantsandsuppliers.MerchantsAndSuppliers
import shared.pages.HiddenFields.HiddenFieldKeys
import shared.pages.PageInfo
import shared.serverresponses.{DeadlineHasPassed, GotArticleAndSavedIt, GotArticleButCouldNotSave, ServerError}
import slogging.LazyLogging

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scalatags.JsDom.all._

/**
  * On this page, the user can create a new promotional article.
  * <p>
  * Created by Matthias Braun on 11/6/2016.
  */
object NewArticle extends LazyLogging {

  private def sendFormDataToServer(merchantOrSupplierId: MerchantOrSupplierId) =
    (_: Event) =>

      ArticleForm.data.fold {
        logger.warn("Couldn't get form data")
      } {
        formData =>
          logger.info(s"Sending form to server")
          val eventualResponse = Ajaxer.saveNewArticle(formData)
          eventualResponse.map {
            case GotArticleAndSavedIt(msg) =>
              logger.info(s"Server says it received the form data: $msg")
              Navigation.changeTo(merchantOrSupplierId, PageInfo.ArticleOverview)
            case GotArticleButCouldNotSave(msg) =>
              logger.info(s"Server says that it couldn't save the article: $msg")
              window.alert(msg.value)
            case DeadlineHasPassed(msg) =>
              logger.info(s"Server says that deadline has passed: $msg")
              window.alert(msg.value)
              Navigation.changeTo(merchantOrSupplierId, PageInfo.ArticleOverview)
            case ServerError(msg) =>
              logger.warn(s"Server says something went wrong: $msg")
            case other =>
              logger.warn(s"Unexpected response from server: $other")
          }
      }

  private def merchantOrSupplier: MerchantOrSupplier =
    HiddenFieldsReader.parseHiddenFields.get(HiddenFieldKeys.MerchantOrSupplierId)
      .flatMap(idStr => {
        MerchantsAndSuppliers.idToMerchantOrSupplier(MerchantOrSupplierId(idStr))
      })
      .getOrElse(MerchantsAndSuppliers.unknownMerchant)

  def create(): Node = {

    val articleForm = ArticleForm.create(onSubmit = sendFormDataToServer(merchantOrSupplier.id), merchantOrSupplier)

    ArticleFormCss.addTo(document)

    document.body.appendChild(articleForm.render)
  }
}
