package client.pages.articleform

import client.pages.articleform.ArticleFormGuiStrings.ImageStrings.UrlPlaceholders
import client.pages.articleform.ArticleFormGuiStrings._
import client.sharedgui.SharedElements.{strongSeparator, subtleSeparator}
import client.sharedgui.{SharedGuiStrings, SharedHtmlClasses}
import client.utils.Navigation
import client.utils.gui.Elements
import client.utils.gui.inputs.{Buttons, TextAreas, TextInputs}
import org.scalajs.dom.html.{Div, Form}
import org.scalajs.dom.raw.HTMLFormElement
import org.scalajs.dom.{Element, Event}
import shared.articleformdata.ArticleFormConstants._
import shared.articleformdata.ArticleFormIds._
import shared.articleformdata._
import shared.pages.{GuiString, HtmlId, PageInfo}
import shared.merchantsandsuppliers.{MerchantOrSupplierRight, MerchantOrSupplierRights}
import slogging.LazyLogging

import scalacss.ScalatagsCss._
import scalatags.JsDom.TypedTag
import scalatags.JsDom.all._

// Converts GuiStrings to ScalaTags' StringFrags
import client.sharedgui.GuiStringImplicits.guiString2StringFrag

/**
  * A merchant or a supplier can enter the data of one of their promotion articles on this web page.
  * <p>
  * Created by Matthias Braun on 9/11/2016.
  */
object ArticleForm extends LazyLogging {
  /**
    * Gets the data in the [[ArticleForm]].
    *
    * @return [[Option optionally]], the data of the form or [[None]] if something went wrong
    */
  def data: Option[ArticleFormData] = {
    val formId = ArticleFormIds.articleForm
    Elements.get[HTMLFormElement](formId).fold {
      logger.warn(s"Could not find get form via ID $formId")
      None: Option[ArticleFormData]
    } {
      formElem => {
        Some(ArticleFormDataParser.fromForm(formElem))
      }
    }
  }

  // This character after a label means that the user must provide the data in the form
  val RequiredDataChar = "*"

  /**
    * Lets the user know that they have to enter the data in the [[ArticleForm]].
    *
    * @param labelText the [[GuiString]] of the label describing the data
    * @return the transformed `labelText` that signals to the user that they have to the data associated with the label
    */
  private def markRequired(labelText: GuiString) = GuiString(labelText.value + RequiredDataChar)

  private def suggestedPrice = div(
    p(infoText(ArticleStrings.suggestedPriceInfo)),
    TextInputs.withLabel(ArticleStrings.suggestedPrice, UnbrandedArticleIds.suggestedPrice)
  )

  private def unbrandedArticlePriceFields = fieldset(
    ArticleFormTables.scalesAndPricesForUnbrandedArticle,
    suggestedPrice
  )

  private def initialPrintingAndFilmCosts(typeOfBranding: HtmlId) = div(
    p(infoText(BrandingStrings.initialPrintingCostsInfo)),

    TextInputs.withLabel(BrandingStrings.initialPrintingCosts, BrandingIds.initialPrintingCosts(typeOfBranding)),
    TextInputs.withLabel(BrandingStrings.filmCosts, BrandingIds.filmCosts(typeOfBranding))
  )

  private def articleBrandingFields = div(
    TextInputs.longWithLabel(labelText = BrandingStrings.positionAndDimension,
      ArticleFormIds.BrandingIds.positionAndDimension,
      placeholderText = BrandingStrings.positionAndDimensionPlaceholder),

    p(infoText(BrandingStrings.infoAboutBrandingPrice)),

    // Create a header, a table, and fields for each standard branding type and separate the brandings visually
    BrandingIds.standard.map(branding =>
      div(headerAndFieldsForStandardBranding(branding), subtleSeparator)),

    (1 to NrOfUserDefinedBrandings).map(nrOfUserDefinedBranding =>
      div(headerAndFieldsForUserDefinedBranding(nrOfUserDefinedBranding), subtleSeparator))
  )

  /**
    * Associates the [[HtmlId]] of a particular kind of branding (e.g., print, etching, engraving) with a
    * [[GuiString]] that names that branding.
    */
  private val brandingIdToGuiString = Map[HtmlId, GuiString](
    BrandingIds.print1c -> BrandingStrings.print1c,
    BrandingIds.printAdditionalColor -> BrandingStrings.printAdditionalColor,
    BrandingIds.print2c -> BrandingStrings.print2c,
    BrandingIds.print3c -> BrandingStrings.print3c,
    BrandingIds.print4c -> BrandingStrings.print4c,
    BrandingIds.digitalPrint4c -> BrandingStrings.digitalPrint4c,
    BrandingIds.engraving -> BrandingStrings.engraving,
    BrandingIds.etching -> BrandingStrings.etching,
    BrandingIds.stitching -> BrandingStrings.stitching
  )

  private def headerAndFieldsForStandardBranding(typeOfBranding: HtmlId) = {
    val brandingName: GuiString = brandingIdToGuiString.getOrElse(typeOfBranding, BrandingStrings.unknownBranding)
    div(
      h4(brandingName),
      fieldset(
        ArticleFormTables.scalesAndPricesForArticleBranding(typeOfBranding),
        initialPrintingAndFilmCosts(typeOfBranding)
      ))
  }

  private def headerAndFieldsForUserDefinedBranding(nrOfUserDefinedBranding: Int) = {

    val userDefinedBrandingId = BrandingIds.userDefinedBranding(nrOfUserDefinedBranding)
    val brandingName = BrandingStrings.userDefinedBranding(nrOfUserDefinedBranding)

    div(
      h4(brandingName),
      fieldset(
        TextInputs.longWithLabel(BrandingStrings.nameOfUserDefinedBranding,
          BrandingIds.nameOfUserDefinedBranding(nrOfUserDefinedBranding)),

        ArticleFormTables.scalesAndPricesForArticleBranding(userDefinedBrandingId),

        initialPrintingAndFilmCosts(userDefinedBrandingId)
      )
    )
  }


  private def headerAndFieldsForMinimumOrderQuantity(nrOfMinimumOrderQuantity: Int) = {
    import ArticleIds.MinimumOrderQuantityIds._
    import ArticleStrings.MinimumOrderQuantity._

    div(
      h4(header(nrOfMinimumOrderQuantity)),

      TextInputs.longWithLabel(condition, conditionId(nrOfMinimumOrderQuantity), conditionPlaceholder),

      TextInputs.longWithLabel(quantity, quantityId(nrOfMinimumOrderQuantity))
    )
  }

  private def minimumOrderQuantities = div(
    (1 to NrOfMinimumOrderQuantities).map(nrOfMinimumOrderQuantity =>
      headerAndFieldsForMinimumOrderQuantity(nrOfMinimumOrderQuantity))
  )

  private def additionalImageUrls = (1 to ImageUrlConstants.NrOfAdditionalArticleImageUrls)
    .map(imageNr => TextInputs.veryLongTextInputWithLabel(ImageStrings.additionalArticleImage(imageNr),
      ImageUrlIds.additionalArticleImage(imageNr),
      ImageStrings.UrlPlaceholders.additionalArticleImageUrl(imageNr)))

  private def colorSampleUrls = (1 to ImageUrlConstants.NrOfColorSampleUrls)
    .map(imageNr => TextInputs.veryLongTextInputWithLabel(ImageStrings.colorSample(imageNr),
      ImageUrlIds.colorSample(imageNr),
      ImageStrings.UrlPlaceholders.colorSampleUrl(imageNr)))

  private def additionalLogoUrls = (1 to ImageUrlConstants.NrOfAdditionalLogoUrls)
    .map(imageNr => TextInputs.veryLongTextInputWithLabel(ImageStrings.additionalLogo(imageNr),
      ImageUrlIds.additionalLogo(imageNr),
      ImageStrings.UrlPlaceholders.additionalLogoUrl(imageNr)))

  private def certificateUrls = (1 to ImageUrlConstants.NrOfCertificateUrls)
    .map(imageNr => TextInputs.veryLongTextInputWithLabel(ImageStrings.certificate(imageNr),
      ImageUrlIds.certificate(imageNr),
      ImageStrings.UrlPlaceholders.certificateUrl(imageNr)))

  private def imageUrls = div(
    p(TextInputs.veryLongTextInputWithLabel(ImageStrings.mainImage, ImageUrlIds.mainArticleImage, ImageStrings.UrlPlaceholders.mainImage)),
    p(additionalImageUrls),
    p(colorSampleUrls),
    p(TextInputs.veryLongTextInputWithLabel(ImageStrings.brandImage, ImageUrlIds.brandImage, ImageStrings.UrlPlaceholders.brandImage)),
    p(additionalLogoUrls),
    p(certificateUrls)
  )


  private def remarksForCatalogCreation = TextAreas.veryLong(ArticleFormIds.remarksOnCatalogCreation)

  private def merchantSpecificArea(merchantOrSupplier: MerchantOrSupplier) =

  // If the form is for a merchant, add their specific fields, otherwise leave the area empty
    merchantOrSupplier match {
      case _: Merchant =>
        div(id := MerchantIds.merchantSpecificFiles.value)(
          strongSeparator,
          h3(ArticleFormGuiStrings.MerchantStrings.printData),

          // The merchant specific area has a red background which would clash with the color of the info text. Thus
          // we set the background to the default bright color
          div(ArticleFormClasses.defaultBackgroundAndCentered)(
            infoText(MerchantStrings.fileInfoText)),

          p(TextInputs.veryLongTextInputWithLabel(MerchantStrings.cover, MerchantIds.coverUrlInput, UrlPlaceholders.merchantCover)),
          p(TextInputs.veryLongTextInputWithLabel(MerchantStrings.customPages, MerchantIds.customPagesUrlInput,
            UrlPlaceholders.merchantCustomPages)),
          strongSeparator
        )
      case _: Supplier => strongSeparator
    }


  //  def submitButton =
  //  button(ArticleFormClasses.submitButton, `type` := "submit")(s"${ArticleStrings.submit} ", ArticleFormIcons.send)
  private def submitButton(onSubmit: (Event => Unit), rights: Seq[MerchantOrSupplierRight]) = {

    val canSubmit = rights.contains(MerchantOrSupplierRights.submitArticle)

    //val submitButton = button(ArticleFormClasses.submitButton, `type` := "button", onclick := onSubmit)(s"${ArticleStrings.submit} ", ArticleFormIcons.send).render
    val submitButton = Buttons.withTextAndIcon(ArticleStrings.submit, ArticleFormIcons.send, onSubmit, ArticleFormClasses.submitButton)
    submitButton.disabled = !canSubmit
    submitButton
  }

  private def cancelButton(merchantOrSupplier: MerchantOrSupplier) = {
    val changeToArticleOverview = (_: Event) => Navigation.changeTo(merchantOrSupplier.id, PageInfo.ArticleOverview)
    button(ArticleFormClasses.cancelButton, `type` := "button", onclick := changeToArticleOverview)(ArticleStrings.cancel)
  }

  private def textAboutFieldHighlighting = infoText(ArticleFormGuiStrings.fieldsWithNewDataAreHighlighted)


  def create(onSubmit: (Event => Unit), merchantOrSupplier: MerchantOrSupplier): Form = {
    val rights = MerchantOrSupplierRights.rightsFor(merchantOrSupplier)

    form(ArticleFormClasses.alignedForm, id := ArticleFormIds.articleForm.value, onsubmit := onSubmit)(

      div(SharedHtmlClasses.header)(
        h1(ArticleFormGuiStrings.formTitle)
      ),
      div(id := MerchantAndSupplierIds.data.value)(
        h2(MerchantOrSupplierStrings.yourCompanyData),
        merchantOrSupplierFields(merchantOrSupplier)
      ),
      div(id := ArticleIds.data.value)(
        h2(ArticleStrings.yourArticle),

        p(infoText(ArticleFormGuiStrings.fieldsMarkedWithRequiredDataCharMustBeFilledOut)),

        // This text is hidden by default since it's out of place when the user creates a new article. When the user
        // edits an article, we show it
        p(SharedHtmlClasses.hidden)(id := ArticleFormIds.fieldsWithNewDataAreHighlighted.value)(textAboutFieldHighlighting),

        articleDescriptionFields,
        h3(ArticleStrings.scalesAndPricesForUnbrandedArticles),
        infoText(ArticleFormGuiStrings.ArticleStrings.infoAboutPrice),
        unbrandedArticlePriceFields,

        strongSeparator,
        h3(ArticleStrings.brandings),
        articleBrandingFields,

        strongSeparator,
        h3(ArticleStrings.MinimumOrderQuantity.quantities),
        minimumOrderQuantities,

        strongSeparator,
        h3(ImageStrings.images),
        infoTextAboutImageLinks,
        imageUrls,

        strongSeparator,
        h3(ArticleFormGuiStrings.remarksForCatalogCreation),
        remarksForCatalogCreation,

        merchantSpecificArea(merchantOrSupplier),

        submitButton(onSubmit, rights),
        infoAboutRights(rights),
        p(cancelButton(merchantOrSupplier))
      )
    ).render
  }

  private def infoAboutRights(rights: Seq[MerchantOrSupplierRight]): TypedTag[Div] =
    if (!rights.contains(MerchantOrSupplierRights.submitArticle))
      div(SharedHtmlClasses.deadlineOver)(p(SharedGuiStrings.deadlineOver))
    else
      div()

  private def infoTextAboutImageLinks =
    div(ArticleFormClasses.defaultBackgroundAndCentered)(infoText(ImageStrings.infoAboutImageLinks))


  private def merchantOrSupplierFields(merchantOrSupplier: MerchantOrSupplier) = fieldset(
    TextInputs.withLabel(labelText = MerchantOrSupplierStrings.name, labelId = MerchantAndSupplierIds.name,
      inputText = GuiString(merchantOrSupplier.name), readonly)
  )

  private def articleDescriptionFields = fieldset(
    TextInputs.longWithLabel(markRequired(ArticleStrings.number), ArticleIds.number, required),
    TextInputs.longWithLabel(markRequired(ArticleStrings.name), ArticleIds.name, required),
    articleDescriptionArea,
    TextInputs.longWithLabel(ArticleStrings.dimensionAndSize, ArticleIds.size,
      ArticleStrings.dimensionAndSizePlaceholder),
    TextInputs.longWithLabel(ArticleStrings.colorAndShape, ArticleIds.colorAndShape)
  )

  private val articleDescriptionArea = TextAreas.bigWithLabel(markRequired(ArticleStrings.description),
    ArticleIds.description, ArticleStrings.descriptionPlaceholder, required, maxlength := ArticleDescriptionMaxLength)

  private def infoText(text: TypedTag[Element]) = div(ArticleFormClasses.infoText)(
    // Add the info icon, a space, then the info text
    ArticleFormIcons.info, " ", text)

  private def infoText(text: GuiString) = div(ArticleFormClasses.infoText)(
    // Add the info icon, a space, then the info text
    ArticleFormIcons.info, s" $text")
}
