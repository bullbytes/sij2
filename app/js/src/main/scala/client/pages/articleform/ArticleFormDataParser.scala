package client.pages.articleform

import shared.articleformdata.ArticleFormIds.ArticleIds.MinimumOrderQuantityIds
import shared.articleformdata.ArticleFormIds.{ImageUrlIds, _}
import shared.articleformdata.ArticleImages.Descriptions
import shared.articleformdata.brandings.{StandardArticleBrandings, UserDefinedBrandings}
import client.utils.gui.Elements.{getValueOfInputOrWarn, getValueOfTextAreaOrWarn}
import org.scalajs.dom.raw.HTMLFormElement
import shared.articleformdata._
import shared.merchantsandsuppliers.MerchantsAndSuppliers
import shared.pages.HtmlId
import slogging.LazyLogging

import scala.collection.immutable.IndexedSeq

/**
  * Gets the content of the [[ArticleForm]] so it can be saved in a database.
  * <p>
  * Created by Matthias Braun on 10/18/2016.
  */
object ArticleFormDataParser extends LazyLogging {
  private def getArticleData = {
    import ArticleFormIds._
    val number = getValueOfInputOrWarn(ArticleIds.number)
    val name = getValueOfInputOrWarn(ArticleIds.name)
    val dimensionAndSize = getValueOfInputOrWarn(ArticleIds.size)
    val colorAndShape = getValueOfInputOrWarn(ArticleIds.colorAndShape)
    val description = getValueOfTextAreaOrWarn(ArticleIds.description)

    PromotionalArticle(number, name, description, dimensionAndSize, colorAndShape)
  }

  def unbrandedArticleScaleName(scaleNr: Int): String = s"unbranded article scale $scaleNr"

  def getScales(brandingId: HtmlId, getScaleName: (Int => String)): Seq[ArticlePriceScale] =
    for {
      ((quantityId, priceId), index) <- BrandingIds.Scales.quantityAndPriceIds(brandingId).zipWithIndex
      quantity = getValueOfInputOrWarn(quantityId)
      price = getValueOfInputOrWarn(priceId)
      scaleName = getScaleName(index + 1)
    } yield ArticlePriceScale(scaleName, quantity, price)

  def getScalesOfUnbrandedArticle: Seq[ArticlePriceScale] = {
    val quantityOfArticlesForScale = UnbrandedArticleIds.Scales.articleQuantityIds
      .map(idOfFieldForQuantityOfArticlesOfScale =>
        getValueOfInputOrWarn(idOfFieldForQuantityOfArticlesOfScale))

    val pricesForScale = UnbrandedArticleIds.Scales.priceIds
      .map(idOfFieldForPriceOfArticlesOfScale => getValueOfInputOrWarn(idOfFieldForPriceOfArticlesOfScale))

    val quantitiesAndPrices = quantityOfArticlesForScale.zip(pricesForScale)
    quantitiesAndPrices.zipWithIndex.map { case (quantityAndPrice, index) =>
      ArticlePriceScale(unbrandedArticleScaleName(index + 1), quantityAndPrice._1, quantityAndPrice._2)
    }
  }


  private def getUnbrandedArticlePrice = {
    val suggestedPrice = getValueOfInputOrWarn(UnbrandedArticleIds.suggestedPrice)
    UnbrandedArticlePrice(getScalesOfUnbrandedArticle, suggestedPrice)
  }

  def getBrandingPosition = getValueOfInputOrWarn(BrandingIds.positionAndDimension)

  def getInitialCosts(brandingId: HtmlId) = getValueOfInputOrWarn(BrandingIds.initialPrintingCosts(brandingId))

  def getFilmCosts(brandingId: HtmlId) = getValueOfInputOrWarn(BrandingIds.filmCosts(brandingId))

  def getBranding(brandingId: HtmlId, brandingName: String, brandingPosition: String) = {
    val initialCosts = getInitialCosts(brandingId)
    val filmCosts = getFilmCosts(brandingId)
    val priceScales = getScales(brandingId, (scaleNr: Int) => s"scale $scaleNr for $brandingName")
    ArticleBranding(brandingName, brandingPosition, priceScales, initialCosts, filmCosts)
  }

  def getStandardBranding(brandingId: HtmlId, brandingPosition: String) =
    StandardArticleBrandings.nameFor(brandingId)
      .map(brandingName => getBranding(brandingId, brandingName, brandingPosition))

  private def getStandardBrandings(position: String) = {

    import BrandingIds._
    for {
      print1c <- getStandardBranding(print1c, position)
      additionalColor <- getStandardBranding(printAdditionalColor, position)
      print2c <- getStandardBranding(print2c, position)
      print3c <- getStandardBranding(print3c, position)
      print4c <- getStandardBranding(print4c, position)
      digitalPrint4c <- getStandardBranding(digitalPrint4c, position)
      engraving <- getStandardBranding(engraving, position)
      etching <- getStandardBranding(etching, position)
      stitching <- getStandardBranding(stitching, position)
    } yield Seq(print1c, additionalColor, print2c, print3c, print4c, digitalPrint4c, engraving, etching, stitching)

  }

  def userDefinedBrandingName(userDefinedBrandingId: HtmlId, userDefinedBrandingNr: Int) = {
    val inputValue = getValueOfInputOrWarn(userDefinedBrandingId)
    // The user hasn't entered a name for the branding so we provide a default one
    if (inputValue.isEmpty)
      UserDefinedBrandings.defaultName(userDefinedBrandingNr)
    else inputValue

  }

  private def getUserDefinedBrandings(position: String): Seq[ArticleBranding] = {
    val brandingNames = for {
      userDefinedBrandingNr <- 1 to ArticleFormConstants.NrOfUserDefinedBrandings
      userDefinedBrandingNameId = BrandingIds.nameOfUserDefinedBranding(userDefinedBrandingNr)

    } yield userDefinedBrandingName(userDefinedBrandingNameId, userDefinedBrandingNr)

    // Get the data of each user-defined branding
    BrandingIds.userDefinedBrandings.zip(brandingNames).map { case (id, name) => getBranding(id, name, position) }
  }

  private def getBrandings = {
    val position = getBrandingPosition

    val standardBrandings = getStandardBrandings(position).getOrElse {
      logger.warn("Could not get all standard brandings")
      Seq()
    }

    val userDefinedBrandings = getUserDefinedBrandings(position)
    standardBrandings ++ userDefinedBrandings
  }

  private def getMinimumOrderQuantities: IndexedSeq[MinimumOrderQuantity] =
    for {
    // The user can specify multiple minimum order quantities with associated conditions
      (conditionId, quantityId) <- MinimumOrderQuantityIds.conditionAndQuantityIds
      condition = getValueOfInputOrWarn(conditionId)
      quantity = getValueOfInputOrWarn(quantityId)

    } yield MinimumOrderQuantity(condition, quantity)

  private def getMultipleImages(inputIds: Seq[HtmlId], imageDescription: (Int => String)): Seq[ArticleFormImage] =
    inputIds.zipWithIndex.map {
      case (id, index) =>
        val imageNr = index + 1
        ArticleFormImage(getValueOfInputOrWarn(id), imageDescription(imageNr))
    }


  def getSingleImageUrl(inputId: HtmlId, imageDescription: String) = {

    val url = getValueOfInputOrWarn(inputId)
    ArticleFormImage(url, imageDescription)
  }

  private def getImages: Seq[ArticleFormImage] = {
    val mainImage = getSingleImageUrl(ImageUrlIds.mainArticleImage, Descriptions.mainArticleImage)
    val additionalArticleImages = getMultipleImages(ImageUrlIds.additionalArticleImages, Descriptions.additionalArticleImage)
    val colorSamples = getMultipleImages(ImageUrlIds.colorSamples, Descriptions.colorSamples)
    val brandImage = getSingleImageUrl(ImageUrlIds.brandImage, Descriptions.brandImage)
    val logos = getMultipleImages(ImageUrlIds.additionalLogos, Descriptions.additionalLogo)
    val certificates = getMultipleImages(ImageUrlIds.certificates, Descriptions.certificate)

    (mainImage +: additionalArticleImages) ++ (colorSamples :+ brandImage) ++ logos ++ certificates
  }

  private def getMerchantSpecificFiles = {
    val cover = getValueOfInputOrWarn(MerchantIds.coverUrlInput)
    val customPages = getValueOfInputOrWarn(MerchantIds.customPagesUrlInput)
    Seq(
      MerchantSpecificFile(cover, MerchantSpecificFiles.Descriptions.cover),
      MerchantSpecificFile(customPages, MerchantSpecificFiles.Descriptions.customPages)
    )
  }

  def getMerchantOrSupplier = {
    val nameOfMerchantOrSupplier = getValueOfInputOrWarn(ArticleFormIds.MerchantAndSupplierIds.name)
    MerchantsAndSuppliers.nameToMerchantOrSupplier(nameOfMerchantOrSupplier)
      .getOrElse(MerchantsAndSuppliers.unknownMerchant)
  }

  def getCatalogData = {
    val remarksOnCatalogCreation = getValueOfTextAreaOrWarn(ArticleFormIds.remarksOnCatalogCreation)

    CatalogData(remarksOnCatalogCreation, positionInCatalog = -1, CatalogCategory.unassigned,
      CatalogSubCategory.unassigned, pageInCatalog = -1,
      articleNrInCatalog = "")
  }

  def fromForm(form: HTMLFormElement): ArticleFormData = {

    val merchantOrSupplier = getMerchantOrSupplier
    val articleData = getArticleData
    val unbrandedArticlePrice = getUnbrandedArticlePrice
    val brandings = getBrandings
    val minimumOrderQuantities = getMinimumOrderQuantities
    val images = getImages
    val merchantSpecificFiles = getMerchantSpecificFiles
    val catalogData = getCatalogData

    ArticleFormData(
      merchantOrSupplier,

      articleData,
      unbrandedArticlePrice,
      brandings,
      minimumOrderQuantities,
      images,
      merchantSpecificFiles,
      catalogData
    )
  }
}

