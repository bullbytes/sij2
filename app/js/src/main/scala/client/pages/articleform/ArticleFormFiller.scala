package client.pages.articleform

import client.utils.gui.Elements
import org.scalajs.dom.FormData
import org.scalajs.dom.html.{Form, Input, TextArea}
import shared.articleformdata.ArticleFormIds.ArticleIds.MinimumOrderQuantityIds
import shared.articleformdata.ArticleFormIds._
import shared.articleformdata._
import shared.articleformdata.brandings.{StandardArticleBrandings, UserDefinedBrandings}
import shared.pages.HtmlId
import slogging.LazyLogging
import shared.utils.conversions.RichSeq.SeqUsingIndex

/**
  * Fills an article form with [[FormData]]. It also highlights which fields contain different data compared to the
  * previous version of the data.
  * <p>
  * Created by Matthias Braun on 10/25/2016.
  */
object ArticleFormFiller extends LazyLogging {

  private def highlightChange(oldValueMaybe: Option[String], newValue: String, idOfElementToHighlight: HtmlId): Unit =
    oldValueMaybe.foreach(oldValue => highlightChange(oldValue, newValue, idOfElementToHighlight))

  private def highlightChange(oldValue: String, newValue: String, idOfElementToHighlight: HtmlId): Unit =
    if (oldValue != newValue) {
      logger.info(s"Highlighting change in $idOfElementToHighlight since old value ($oldValue) and new value ($newValue) differ")
      Elements.addClass(idOfElementToHighlight, ArticleFormClasses.valueChangedSincePreviousVersion)
    }

  private def setInputValueAndHighlightChange(newValue: String, elementId: HtmlId, form: Form,
                                              previousValue: String): Unit = {
    setInputValueOrWarn(newValue, elementId, form)
    highlightChange(previousValue, newValue, elementId)
  }

  private def setInputValueAndHighlightChange(newValue: String, elementId: HtmlId, form: Form,
                                              previousValueMaybe: Option[String]): Unit = {
    setInputValueOrWarn(newValue, elementId, form)
    highlightChange(previousValueMaybe, newValue, elementId)
  }

  private def setTextAreaValueAndHighlightChange(newValue: String, textAreaId: HtmlId, form: Form,
                                                 previousValueMaybe: Option[String]) = {
    setTextAreaValueOrWarn(newValue, textAreaId, form)
    highlightChange(previousValueMaybe, newValue, textAreaId)
  }

  private def addArticleData(article: PromotionalArticle, prevArticle: Option[PromotionalArticle], form: Form) = {

    import ArticleIds._

    setInputValueAndHighlightChange(article.articleNumberAssignedByMerchantOrSupplier, number, form,
      prevArticle.map(_.articleNumberAssignedByMerchantOrSupplier))

    setInputValueAndHighlightChange(article.name, name, form, prevArticle.map(_.name))

    setTextAreaValueAndHighlightChange(article.description, description, form, prevArticle.map(_.description))

    setInputValueAndHighlightChange(article.size, size, form, prevArticle.map(_.size))

    setInputValueAndHighlightChange(article.colorAndShape, colorAndShape, form, prevArticle.map(_.colorAndShape))
  }

  private def addUnbrandedArticlePrice(latestPrices: UnbrandedArticlePrice,
                                       previousPricesMaybe: Option[UnbrandedArticlePrice],
                                       form: Form) = {
    import UnbrandedArticleIds._

    val previousScalesMaybe = previousPricesMaybe.map(_.unbrandedArticleScales)
    // Sets the price and the quantity to be in a certain price scale for each scale
    setValuesInScales(latestPrices.unbrandedArticleScales, previousScalesMaybe, Scales.articleQuantityIds, Scales.priceIds, form)

    setInputValueAndHighlightChange(latestPrices.suggestedPrice, suggestedPrice, form, previousPricesMaybe.map(_.suggestedPrice))
  }

  private def setValuesInScales(latestScales: Seq[ArticlePriceScale],
                                prevScalesMaybe: Option[Seq[ArticlePriceScale]] = None,
                                quantityIds: Seq[HtmlId],
                                priceIds: Seq[HtmlId], form: Form): Unit = {

    val sortFunc = (scale: ArticlePriceScale) => scale.name
    val sortedLatestScales = latestScales.sortBy(sortFunc)

    prevScalesMaybe.fold {
      sortedLatestScales.zip(quantityIds).zip(priceIds).foreach {
        case ((scale, quantityId), priceId) =>
          setInputValueOrWarn(scale.numberOfArticles, quantityId, form)
          setInputValueOrWarn(scale.price, priceId, form)
      }
    } { prevScales =>

      // Makes sure the old versions of the scales are sorted the same way as the new ones so we don't compare the wrong ones
      val sortedPrevScales = prevScales.sortBy(sortFunc)

      sortedPrevScales.zip(sortedLatestScales).zip(quantityIds).zip(priceIds).foreach {
        case (((oldScale, newScale), quantityId), priceId) =>
          setInputValueAndHighlightChange(newScale.numberOfArticles, quantityId, form, oldScale.numberOfArticles)
          setInputValueAndHighlightChange(newScale.price, priceId, form, oldScale.price)
      }
    }
  }

  private def setTextAreaValueOrWarn(textAreaValue: String, textAreaId: HtmlId, form: Form) =
    Elements.getFrom(form, textAreaId).flatMap(element => Elements.as[TextArea](element)).fold(
      logger.warn(s"Couldn't get text area with ID '$textAreaId'")
    )(
      textArea => textArea.value = textAreaValue
    )


  private def setInputValueOrWarn(inputValue: String, inputId: HtmlId, form: Form) =
    Elements.getFrom(form, inputId).flatMap(element => Elements.as[Input](element)).fold(
      logger.warn(s"Couldn't get input with ID '$inputId'")
    )(
      input => input.value = inputValue
    )

  private def addStandardBranding(latestBranding: ArticleBranding, previousBrandingMaybe: Option[ArticleBranding], form: Form) = {
    import BrandingIds._
    StandardArticleBrandings.idFor(latestBranding.name).fold(
      logger.warn(s"There is no HTML ID for branding with name '${latestBranding.name}"))(
      htmlId => {

        setInputValueAndHighlightChange(latestBranding.initialCosts, initialPrintingCosts(htmlId), form, previousBrandingMaybe.map(_.initialCosts))
        setInputValueAndHighlightChange(latestBranding.filmCosts, filmCosts(htmlId), form, previousBrandingMaybe.map(_.filmCosts))

        setValuesInScales(latestBranding.scales, previousBrandingMaybe.map(_.scales),
          Scales.articleQuantityIdsFor(htmlId),
          Scales.priceIdsFor(htmlId), form)
      })
  }

  private def addStandardBrandings(latestBrandings: Seq[ArticleBranding],
                                   previousBrandingsMaybe: Option[Seq[ArticleBranding]] = None, form: Form): Unit = {
    // All the brandings have the same position and dimension
    latestBrandings.headOption.map(_.position).fold(
      logger.warn("There are no brandings for this article"))(latestPosition => {
      val previousPositionMaybe = previousBrandingsMaybe.flatMap(_.headOption).map(_.position)
      setInputValueAndHighlightChange(latestPosition, BrandingIds.positionAndDimension, form, previousPositionMaybe)
    })

    previousBrandingsMaybe.fold(
      latestBrandings.foreach(branding => addStandardBranding(branding, None, form))
    )(
      // It's important that we sort the brandings so we compare the old and new version of the same branding
      previousBrandings => latestBrandings.sortBy(_.name).zip(previousBrandings.sortBy(_.name)).foreach {
        case (latestBranding, previousBranding) => addStandardBranding(latestBranding, Some(previousBranding), form)
      }
    )
  }

  private def addUserDefinedBrandings(latestUserDefinedBrandings: Seq[ArticleBranding],
                                      previousUserDefinedBrandingsMaybe: Option[Seq[ArticleBranding]] = None, form: Form) = {
    import BrandingIds._

    latestUserDefinedBrandings.sortBy(_.name).forEachWithIndex((latestBranding, index) =>{
        val brandingNr = index + 1
        val previousBrandingMaybe = previousUserDefinedBrandingsMaybe.map(previousBrandings => previousBrandings(index))
        val nameId = nameOfUserDefinedBranding(brandingNr)
        // Don't fill in the default name of a branding, leave it empty
        if (latestBranding.name != UserDefinedBrandings.defaultName(brandingNr))
          setInputValueAndHighlightChange(latestBranding.name, nameId, form, previousBrandingMaybe.map(_.name))

        val brandingId = userDefinedBranding(brandingNr)

        val quantityIds = Scales.articleQuantityIdsFor(brandingId)
        val priceIds = Scales.priceIdsFor(brandingId)
        setValuesInScales(latestBranding.scales, previousBrandingMaybe.map(_.scales), quantityIds, priceIds, form)

        setInputValueAndHighlightChange(latestBranding.initialCosts, initialPrintingCosts(brandingId), form, previousBrandingMaybe.map(_.initialCosts))
        setInputValueAndHighlightChange(latestBranding.filmCosts, filmCosts(brandingId), form, previousBrandingMaybe.map(_.filmCosts))
    })
  }

  private def addMinimumOrderQuantity(latestMoqs: Seq[MinimumOrderQuantity],
                                      prevMoqsMaybe: Option[Seq[MinimumOrderQuantity]], form: Form) =

    latestMoqs.zip(MinimumOrderQuantityIds.conditionAndQuantityIds).zipWithIndex.foreach {
      case ((moq, (conditionId, quantityId)), index) =>
        val prevMoqMaybe = prevMoqsMaybe.map(prevMoqs => prevMoqs(index))
        setInputValueAndHighlightChange(moq.condition, conditionId, form, prevMoqMaybe.map(_.condition))
        setInputValueAndHighlightChange(moq.quantity, quantityId, form, prevMoqMaybe.map(_.quantity))
    }

  /**
    * Gets an image by its description. If the image is found, the function ifFound is applied to the image.
    */
  private def imageByDescriptionOrWarn(images: Seq[ArticleFormImage], description: String)(ifFound: (ArticleFormImage => Unit)) =
    images.find(image => image.description == description).fold(
      logger.warn(s"Couldn't get image with description '$description'"))(ifFound)

  private def setSingleImageUrl(imageUrlInputId: HtmlId, imageDescription: String, latestImages: Seq[ArticleFormImage],
                                prevImagesMaybe: Option[Seq[ArticleFormImage]], form: Form) =

  // If there are no previous images, we just set the URLs of the current images
    prevImagesMaybe.fold(
      imageByDescriptionOrWarn(latestImages, imageDescription)(latestImage =>
        setInputValueOrWarn(latestImage.url, imageUrlInputId, form)
      )
    )(
      // If there are previous versions of this article we set the current version of the URLs and highlight changes
      previousImages =>
        imageByDescriptionOrWarn(previousImages, imageDescription)(previousImage =>
          imageByDescriptionOrWarn(latestImages, imageDescription)(latestImage =>
            setInputValueAndHighlightChange(latestImage.url, imageUrlInputId, form, previousImage.url)
          )
        )
    )


  def setMultipleImageUrls(imageUrlInputIds: Seq[HtmlId], imageDescription: (Int) => String,
                           latestImages: Seq[ArticleFormImage], previousImagesMaybe: Option[Seq[ArticleFormImage]],
                           form: Form): Unit =

      imageUrlInputIds.forEachWithIndex ((id, index) =>{
        val imageNr = index + 1
        setSingleImageUrl(id, imageDescription(imageNr), latestImages, previousImagesMaybe, form)
    })

  private def addImageUrls(latestImages: Seq[ArticleFormImage], prevImagesMaybe: Option[Seq[ArticleFormImage]], form: Form) = {

    val setMultipleImages: (Seq[HtmlId], (Int) => String) => Unit =
      setMultipleImageUrls(_, _, latestImages, prevImagesMaybe, form)

    val setSingleImage: (HtmlId, String) => Unit = setSingleImageUrl(_, _, latestImages, prevImagesMaybe, form)

    import ArticleImages.Descriptions

    setSingleImage(ImageUrlIds.mainArticleImage, Descriptions.mainArticleImage)

    setMultipleImages(ImageUrlIds.additionalArticleImages, Descriptions.additionalArticleImage)

    setMultipleImages(ImageUrlIds.colorSamples, Descriptions.colorSamples)

    setSingleImage(ImageUrlIds.brandImage, Descriptions.brandImage)

    setMultipleImages(ImageUrlIds.additionalLogos, Descriptions.additionalLogo)

    setMultipleImages(ImageUrlIds.certificates, Descriptions.certificate)

  }

  private def addRemarksOnCatalogCreation(latestCatalogData: CatalogData, prevCatalogData: Option[CatalogData], form: Form) =
    setTextAreaValueAndHighlightChange(latestCatalogData.remarksOnCatalogCreation,
      ArticleFormIds.remarksOnCatalogCreation, form, prevCatalogData.map(_.remarksOnCatalogCreation))

  private def addMerchantSpecificFiles(latestFiles: Seq[MerchantSpecificFile],
                                       prevFilesMaybe: Option[Seq[MerchantSpecificFile]],
                                       form: Form) = {
    /**
      * Gets a [[MerchantSpecificFile]] by its description. If the file is found, the function ifFound is applied to the file.
      */
    def fileByDescriptionOrWarn(description: String, files: Seq[MerchantSpecificFile])
                               (ifFound: (MerchantSpecificFile => Unit)) =
      files.find(file => file.description == description).fold(
        logger.warn(s"Couldn't get file with description '$description'"))(ifFound)

    import MerchantSpecificFiles.Descriptions._

    prevFilesMaybe.fold {
      fileByDescriptionOrWarn(cover, latestFiles)(cover => setInputValueOrWarn(cover.url, MerchantIds.coverUrlInput, form))
      fileByDescriptionOrWarn(customPages, latestFiles)(pages => setInputValueOrWarn(pages.url, MerchantIds.customPagesUrlInput, form))
    } {
      prevFiles =>
        fileByDescriptionOrWarn(cover, latestFiles)(latestCover =>
          fileByDescriptionOrWarn(cover, prevFiles)(previousCover =>
            setInputValueAndHighlightChange(latestCover.url, MerchantIds.coverUrlInput, form, previousCover.url)
          ))

        fileByDescriptionOrWarn(customPages, latestFiles)(latestPages =>
          fileByDescriptionOrWarn(customPages, prevFiles)(previousPages =>
            setInputValueAndHighlightChange(latestPages.url, MerchantIds.customPagesUrlInput, form, previousPages.url)
          ))
    }
  }

  def fillOut(form: Form, latestArticle: ArticleWithMetaInfo, prevArticleMaybe: Option[ArticleWithMetaInfo]): Form = {
    logger.info("Filling form with data")
    prevArticleMaybe.foreach(prevArticle => {
      logger.info(s"Curr article: $latestArticle")
      logger.info(s"Prev article: $prevArticle")
    })

    val latestFormData = latestArticle.articleFormData
    val prevFormDataMaybe = prevArticleMaybe.map(_.articleFormData)

    addArticleData(latestFormData.articleData, prevFormDataMaybe.map(_.articleData), form)
    addUnbrandedArticlePrice(latestFormData.unbrandedArticlePrice, prevFormDataMaybe.map(_.unbrandedArticlePrice), form)

    val (latestStandardBrandings, latestUserDefinedBrandings) = latestFormData.brandings
      .partition(branding => StandardArticleBrandings.names.contains(branding.name))

    prevFormDataMaybe.map(_.brandings).fold {
      addStandardBrandings(latestStandardBrandings, None, form)
      addUserDefinedBrandings(latestUserDefinedBrandings, None, form)
    } {
      prevBrandings: Seq[ArticleBranding] =>
        val (previousStandardBrandings, previousUserDefinedBrandings) = prevBrandings.partition(
          branding => StandardArticleBrandings.names.contains(branding.name))

        addStandardBrandings(latestStandardBrandings, Some(previousStandardBrandings), form)
        addUserDefinedBrandings(latestUserDefinedBrandings, Some(previousUserDefinedBrandings), form)
    }

    addMinimumOrderQuantity(latestFormData.minimumOrderQuantities, prevFormDataMaybe.map(_.minimumOrderQuantities), form)
    addImageUrls(latestFormData.images, prevFormDataMaybe.map(_.images), form)
    addRemarksOnCatalogCreation(latestFormData.catalogData, prevFormDataMaybe.map(_.catalogData), form)

    // If the page is for a merchant, fill out their specific region. Otherwise do nothing
    latestFormData.merchantOrSupplier match {
      case merchant: Merchant =>
        addMerchantSpecificFiles(latestFormData.merchantSpecificFiles, prevFormDataMaybe.map(_.merchantSpecificFiles), form)
      case other =>
    }

    logger.info("Done filling form with data")
    form
  }
}
