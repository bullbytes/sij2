package client.pages.articleform

import client.pages.articleform.ArticleFormGuiStrings.ArticleStrings
import org.scalajs.dom.html.{Table, TableCell}
import shared.articleformdata.ArticleFormConstants.NrOfScales
import shared.articleformdata.ArticleFormIds.{BrandingIds, UnbrandedArticleIds}
import shared.pages.HtmlId

import scalatags.JsDom.TypedTag

// Converts GuiStrings to ScalaTags' StringFrags
import client.sharedgui.GuiStringImplicits.guiString2StringFrag
import client.utils.gui.inputs.TextInputs._

import scalacss.ScalatagsCss._
import scalatags.JsDom.all._

/**
  * Provides HTML tables for the [[ArticleForm]].
  * <p>
  * Created by Matthias Braun on 10/15/2016.
  */
object ArticleFormTables {

  def quantitiesForScales(nrOfScales: Int, tableId: HtmlId): Seq[TypedTag[TableCell]] = {

    // First, create inputs with placeholders that tell the user to enter how many articles a buyer has to order
    // so the articles are in a certain price scale. Then, wrap each input field in a <td> tag for the table
    //    val otherResult = BrandingIds.Scales.articleQuantityIdsFor(tableId)
    //      .map(quantityId => textInputWithPlaceholder(quantityId, ArticleStrings.quantityForScale))
    //      .map(inputWithPlaceholder => td(inputWithPlaceholder))

    (1 to nrOfScales).map(scaleNr =>
      emptyWithPlaceholder(BrandingIds.Scales.quantityForScale(scaleNr, tableId), ArticleStrings.quantityForScale))
      .map(input => td(input))
  }

  private def pricesForScales(nrOfScales: Int, tableId: HtmlId) = {
    // First, create inputs with placeholders that tell the user to enter the price of an individual article in
    // a certain price scale. Then, wrap each input field in a <td> tag for the table
    //    val otherResult = BrandingIds.Scales.articleQuantityIdsFor(tableId)
    //      .map(priceId => textInputWithPlaceholder(priceId, ArticleStrings.priceOfArticleOfScale))
    //      .map(inputWithPlaceholder => td(inputWithPlaceholder))
    //    otherResult

    (1 to nrOfScales).map(scaleNr =>
      emptyWithPlaceholder(BrandingIds.Scales.priceOfArticleForScale(scaleNr, tableId), ArticleStrings.priceOfArticleOfScale))
      .map(input => td(input))
  }

  /**
    * Creates a table with scales and prices for an unbranded promotion article or the branding of a promotion article.
    * The table lets the user enter how many promotion articles or brandings someone has to order to be in one of the
    * price scales and how much a single article or branding of a single article costs.
    *
    * @return a table for entering scales and prices for promotion articles or brandings of promotion articles
    */
  private def scalesAndPricesForArticleOrBranding(tableId: HtmlId) = {
    table(ArticleFormClasses.table)(
      // A table head for each scale
      thead(tr((1 to NrOfScales).map(scaleNr => th(ArticleStrings.scale(scaleNr)))))
      ,
      tbody(
        tr(quantitiesForScales(NrOfScales, tableId)),
        tr(pricesForScales(NrOfScales, tableId))
      )
    )
  }

  /**
    * Creates a table with scales and prices for an unbranded promotion article. The table lets the user enter how many
    * promotion articles someone has to order to be in one of the price scales and how much a single unbranded article
    * costs.
    *
    * @return a table for entering scales and prices for unbranded promotion articles
    */
  def scalesAndPricesForUnbrandedArticle: TypedTag[Table] =
    scalesAndPricesForArticleOrBranding(UnbrandedArticleIds.unbrandedArticle)

  /**
    * Creates a table for a specific `typeOfBranding`. The table lets the user enter how many brandings for a promotion
    * article someone has to order to be in one of the price scales and how much the branding of a single article costs.
    *
    * @param typeOfBranding we create a table for this type of branding identified by a [[HtmlId]]
    * @return a table for entering scales and prices for branding promotion articles
    */
  def scalesAndPricesForArticleBranding(typeOfBranding: HtmlId): TypedTag[Table] =
    scalesAndPricesForArticleOrBranding(typeOfBranding)
}
