package client.pages.articleoverview

import client.sharedgui.SharedIcons
import org.scalajs.dom.html.Element

import scalatags.JsDom.TypedTag

/**
  * Provides icons for the [[ArticleOverview]].
  * <p>
  * Created by Matthias Braun on 11/1/2016.
  */
object ArticleOverviewIcons extends SharedIcons {

  val trash: TypedTag[Element] = fontAwesome("fa-trash-o")
  val bigPlus: TypedTag[Element] = fontAwesome("fa-plus fa-2x")

}
