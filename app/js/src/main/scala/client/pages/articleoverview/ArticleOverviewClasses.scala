package client.pages.articleoverview

import client.sharedgui.SharedHtmlClasses

import scalacss.internal.StyleA

/**
  * Contains the CSS classes we use for the [[client.pages.articleoverview.ArticleOverview]].
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
object ArticleOverviewClasses extends SharedHtmlClasses {

  val userHasNoArticlesText: StyleA = fromClassName("user-has-no-articles-text")

  val deleteArticleLink: StyleA = fromClassName("delete-article-link")

  val center: StyleA = fromClassName("center")

  val articleList: StyleA = fromClassName("articles")

  val articleEntry: StyleA = fromClassName("article-entry")

  val addArticleButton = Seq(pureButton, primaryButton, largeButton)
}
