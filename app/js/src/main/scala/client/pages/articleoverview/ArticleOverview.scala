package client.pages.articleoverview

import client.pages.articleoverview.{ArticleOverviewGuiStrings => GuiStrings}
import client.pages.shared.HiddenFieldsReader
import client.sharedgui.{SharedElements, SharedGuiStrings, SharedHtmlClasses, SharedIcons}
import client.utils.gui.Elements
import client.utils.gui.inputs.Buttons
import client.utils.{Ajaxer, Navigation}
import org.scalajs.dom._
import org.scalajs.dom.ext.AjaxException
import org.scalajs.dom.html.{Div, LI}
import shared._
import shared.articleformdata.{ArticleWithMetaInfo, MerchantOrSupplier, MerchantOrSupplierId}
import shared.merchantsandsuppliers.{MerchantOrSupplierRight, MerchantOrSupplierRights, MerchantsAndSuppliers}
import shared.pages.HiddenFields.HiddenFieldKeys
import shared.pages.{HtmlId, PageInfo}
import shared.serverresponses.{OfferArticle, OfferArticles, ServerError}
import slogging.LazyLogging

import scalatags.JsDom.TypedTag

// Converts GuiStrings to ScalaTags' StringFrags
import client.sharedgui.GuiStringImplicits.guiString2StringFrag

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scalacss.ScalatagsCss._
import scalatags.JsDom.all._

/**
  * Shows users their articles.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
object ArticleOverview extends LazyLogging {

  /**
    * Removes the leading quote from the string representation of a [[Symbol]].
    */
  private def symbolWithoutQuote(symbol: Symbol): String = {
    val symString = String.valueOf(symbol)
    if (symString.startsWith("'"))
      symString.substring(1)
    else
      symString
  }

  def articleListItem(articleWithMetaInfo: ArticleWithMetaInfo, rights: Seq[MerchantOrSupplierRight]): TypedTag[LI] = {
    val articleId = articleWithMetaInfo.metaInfo.articleId

    val articleName = articleWithMetaInfo.articleFormData.articleData.name

    // Clicking the article's name lets the user edit the article
    val editArticleLink = {
      // The parameter in the URL that precedes the ID of the article to edit
      val idParam = symbolWithoutQuote(SijRestApi.Parameters.ArticleId)
      val link = s"/${SijRestApi.Paths.EditArticle}?$idParam=$articleId"
      val linkText = if (articleName.trim.isEmpty) GuiStrings.articleWithoutName.value else articleName
      a(href := link)(linkText)
    }

    val listItemId: HtmlId = ArticleOverviewIds.listItem(articleId)

    // Next to each article is a button to delete it
    val deleteArticleLink = {
      def deleteArticleAfterConfirmation() = (e: Event) => {
        val userWantsToDeleteArticle = window.confirm(GuiStrings.deleteArticle(articleName).value)
        if (userWantsToDeleteArticle) {
          // Tell the server to delete the article
          Ajaxer.deleteArticle(articleId)
          // Hide the article item on the client
          Elements.hide(listItemId)
        }
      }

      a(ArticleOverviewClasses.deleteArticleLink)(
        href := "#", onclick := deleteArticleAfterConfirmation())(ArticleOverviewIcons.trash)
    }

    val space = SharedElements.horizontalSpace
    // Only add the link to delete an article if the user has the right to do it
    if (rights.contains(MerchantOrSupplierRights.deleteArticle))
      li(ArticleOverviewClasses.articleEntry)(id := listItemId.value)(deleteArticleLink, space, editArticleLink)
    else
      li(ArticleOverviewClasses.articleEntry)(id := listItemId.value)(editArticleLink)
  }

  def newArticleButton(merchantOrSupplier: MerchantOrSupplier, rights: Seq[MerchantOrSupplierRight]): TypedTag[Div] = {

    val onClick = (e: Event) => Navigation.changeTo(merchantOrSupplier.id, PageInfo.NewArticle)

    def newArticleButton = Buttons.withTextAndIcon(ArticleOverviewGuiStrings.addArticle, ArticleOverviewIcons.bigPlus,
      onClick, ArticleOverviewClasses.addArticleButton)

    val canAddArticle = rights.contains(MerchantOrSupplierRights.addArticle)
    newArticleButton.disabled = !canAddArticle

    div(ArticleOverviewClasses.center)(newArticleButton)
  }

  private def getMerchantOrSupplierFromHiddenFields: MerchantOrSupplier =
    HiddenFieldsReader.parseHiddenFields.get(HiddenFieldKeys.MerchantOrSupplierId)
      .flatMap(idStr => {
        val id = MerchantOrSupplierId(idStr)
        MerchantsAndSuppliers.idToMerchantOrSupplier(id)
      })
      .getOrElse(MerchantsAndSuppliers.unknownMerchant)


  private def infoAboutRights(rights: Seq[MerchantOrSupplierRight]): TypedTag[Div] =
    if (!rights.contains(MerchantOrSupplierRights.addArticle))
      div(SharedHtmlClasses.deadlineOver)(p(SharedGuiStrings.deadlineOver))
    else
      div()

  def create(): Node = {
    val body = document.body

    ArticleOverviewCss.addTo(document)

    val merchantOrSupplier = getMerchantOrSupplierFromHiddenFields

    // We show this icon while the articles are retrieved from the server
    val loadingIcon = SharedIcons.loadingIcon
    val articlesDiv = div(loadingIcon).render
    val merchantOrSupplierRights = MerchantOrSupplierRights.rightsFor(merchantOrSupplier)

    showArticlesWhenLoaded(merchantOrSupplier, merchantOrSupplierRights, articlesDiv, loadingIcon)

    def pageHeader = div(SharedHtmlClasses.header)(
      h1(ArticleOverviewGuiStrings.formTitle),
      h2(ArticleOverviewGuiStrings.yourArticles))

    body.appendChild(
      div(ArticleOverviewClasses.center)(

        pageHeader,

        articlesDiv,

        newArticleButton(merchantOrSupplier, merchantOrSupplierRights),
        // When the deadline has passed, we inform the user that they have no right to add new articles
        infoAboutRights(merchantOrSupplierRights)

      ).render
    )
  }

  private def getErrorMsgForFailedAjaxCall: PartialFunction[Throwable, Div] = {
    case AjaxException(request) =>
      logger.warn(s"Ajax call failed. Status: ${request.status}. Status text: ${request.statusText}.")
      div(GuiStrings.ajaxExceptionWhileContactingServer).render
    case otherError =>
      logger.warn(s"Could not contact server. Exception: $otherError. Message: ${otherError.getMessage}")
      div(GuiStrings.otherExceptionWhileContactingServer).render
  }


  private def showArticlesWhenLoaded(merchantOrSupplier: MerchantOrSupplier,
                                     rights: Seq[MerchantOrSupplierRight],
                                     articlesDiv: Div, loadingIcon: Div) = {

    logger.info(s"Requesting latest articles of ${merchantOrSupplier.name}")

    // Turn the Future[ServerResponse] into an HTML element
    Ajaxer.getNameAndIdForLatestArticlesOf(merchantOrSupplier.id)
      // We got a ServerResponse, but which kind?
      .map {
      case OfferArticles(articlesFromServer) =>
        toArticleList(articlesFromServer, rights)

      case OfferArticle(singleFormData) =>
        logger.info("Server returned single form data")
        toArticleList(Seq(singleFormData), rights)

      case ServerError(msg) =>
        logger.warn(s"Server says something went wrong: $msg")
        div(GuiStrings.errorGettingArticles(msg)).render

      case other =>
        logger.warn(s"Unexpected server response: $other")
        div(GuiStrings.unexpectedServerResponse(other)).render
    }
      // Turn the failed Future into an HTML element
      .recover(getErrorMsgForFailedAjaxCall)
      .map {
        // Show the HTML element instead of the loading icon
        articlesOrErrorMsg =>
          Elements.hide(loadingIcon)
          articlesDiv.appendChild(articlesOrErrorMsg)
      }
  }

  private def toArticleList(articlesWithMetaInfo: Seq[ArticleWithMetaInfo],
                            rights: Seq[MerchantOrSupplierRight]): Div = {
    logger.info(s"Creating article list for ${articlesWithMetaInfo.size} articles")

    def articleList = if (articlesWithMetaInfo.isEmpty)
      div(p(ArticleOverviewClasses.userHasNoArticlesText)(GuiStrings.userHasNoArticles))
    else
      div(
        ul(ArticleOverviewClasses.articleList)(
          articlesWithMetaInfo
            .sortBy(_.articleFormData.articleData.name)
            .map(article => articleListItem(article, rights))
        ))

    articleList.render
  }
}
