package client.sharedgui

import client.sharedgui.GuiStringImplicits.guiString2StringFrag
import org.scalajs.dom.html.{Div, Element}

import scalacss.ScalatagsCss._
import scalatags.JsDom.TypedTag
import scalatags.JsDom.all._

/**
  * These icons are shared among the different pages of this application.
  * <p>
  * Created by Matthias Braun on 11/1/2016.
  */
trait SharedIcons {

  /**
    * Creates a Font Awesome icon.
    *
    * @param iconClassName the class name of the icon
    * @return the Font Awesome icon
    */
  def fontAwesome(iconClassName: String): TypedTag[Element] = i(`class` := s"fa $iconClassName", centerText, ariaHidden)

  /* Screen readers won't read the button name out loud; That's better for accessibility */
  private val ariaHidden = aria.hidden := "true"

  /* When we put this icon together with some text in a button, this style causes the text to be vertically aligned
  with the icon */
  private val centerText = style := "vertical-align:middle;"
}

object SharedIcons extends SharedIcons {

  def loadingIcon: Div = div(
    // fa-fw means fixed width
    fontAwesome("fa-refresh fa-spin fa-3x fa-fw"),
    span(SharedHtmlClasses.screenReaderOnly)(SharedGuiStrings.loading))
    .render
}
