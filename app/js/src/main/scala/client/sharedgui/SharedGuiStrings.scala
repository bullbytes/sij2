package client.sharedgui

import shared.pages.GuiString
import shared.serverresponses.ServerResponse

/**
  * Strings shown to the user that are shared between pages.
  * <p>
  * Created by Matthias Braun on 12/5/2016.
  */
object SharedGuiStrings {

  val deadlineOver = GuiString("Die Frist für das Hinzufügen von Artikeln ist abgelaufen")
  val loading = GuiString("Lade...")
}
