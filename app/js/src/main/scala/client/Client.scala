package client

import client.pages.allarticles.{SortArticles, ImportJsonArticles}
import client.pages.articleoverview.ArticleOverview
import client.pages.choosecompany.ChooseCompany
import client.pages.createarticle.NewArticle
import client.pages.editarticle.EditArticle
import org.scalajs.dom._
import shared.pages.{PageId, PageInfo}
import slogging.{LazyLogging, LoggerConfig, PrintLoggerFactory}

import scala.scalajs.js
import scalatags.JsDom.all._

/**
  * This code is transpiled to JavaScript and runs in the browser.
  * On the client, the user can create new promotional articles, see an overview of their articles and edit articles.
  */
object Client extends js.JSApp with LazyLogging {
  // Log using Scala's println()
  LoggerConfig.factory = PrintLoggerFactory()

  def main(): Unit = showRightPage()

  def createDefaultPage(title: String, pageId: PageId): Node =
    document.body.appendChild(
      p(s"Don't know what to show for page with title '$title' and ID '$pageId'").render
    )

  /**
    * Shows (and populates) the page depending on the ID of the page.
    */
  def showRightPage(): Object = {
    val pageId = PageId(document.head.id)
    pageId match {
      case PageInfo.NewArticle.id => NewArticle.create()
      case PageInfo.ArticleOverview.id => ArticleOverview.create()
      case PageInfo.ChooseCompany.id => ChooseCompany.create()
      case PageInfo.EditArticle.id => EditArticle.create()
      case PageInfo.AllArticles.id => SortArticles.create()
      case PageInfo.ImportJsonArticles.id => ImportJsonArticles.create()
      case unknownPage => createDefaultPage(document.title, pageId)
    }
  }
}
