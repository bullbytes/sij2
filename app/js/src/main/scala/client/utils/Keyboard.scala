package client.utils

import org.scalajs.dom.{Event, KeyboardEvent}

/**
  * Utility related to the client's keyboard. Helps with keyboard events, for example.
  * <p>
  * Created by Matthias Braun on 10/18/2016.
  */
object Keyboard {

  object KeyCodes {
    val Enter = 13
  }

  private def asKeyboardEvent(event: Event): Option[KeyboardEvent] =
    event match {
      case keyboardEvent: KeyboardEvent => Some(keyboardEvent)
      case noKeyboardEvent => None
    }

  // This might be helpful: http://stackoverflow.com/questions/1444477/keycode-and-charcode#1445228
  def isEnter(event: Event): Boolean = asKeyboardEvent(event)
    .exists(keyboardEvent => keyboardEvent.keyCode == KeyCodes.Enter)

}
