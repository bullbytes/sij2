package client.utils.gui

import client.sharedgui.SharedHtmlClasses
import org.scalajs.dom.html.{Div, Form}
import org.scalajs.dom.raw._
import org.scalajs.dom.{Element, document}
import org.scalajs.jquery.{JQuery, jQuery}
import shared.pages.HtmlId
import slogging.LazyLogging

import scala.collection.immutable.IndexedSeq
import scala.reflect.ClassTag
import scala.scalajs.js
import scalacss.internal.StyleA
import scalatags.JsDom.TypedTag

import scalatags.JsDom.all._
/**
  * Helps with creating, accessing, and modifying DOM elements.
  * <p>
  * Created by Matthias Braun on 10/16/2016.
  */
object Elements extends LazyLogging {

  def append(node: Node): Node = document.body.appendChild(node)

  def append(tag: HtmlTag): Node = append(tag.render)

  def setChild(parent: HTMLElement, newChild: HTMLElement): Node = {
    removeAllChildren(parent)
    parent.appendChild(newChild)
  }

  def removeAllChildren(parent: HTMLElement): Unit = forEachElement(parent.children, child => parent.removeChild(child))

  def show(idOfElementToShow: HtmlId): Unit = removeClass(idOfElementToShow, SharedHtmlClasses.hidden)

  def show(elementToShow: Div): Unit = removeClass(elementToShow, SharedHtmlClasses.hidden)

  def hide(elementToHide: HTMLElement): Unit = addClass(elementToHide, SharedHtmlClasses.hidden)

  def hide(idOfElementToHide: HtmlId): Unit = addClass(idOfElementToHide, SharedHtmlClasses.hidden)

  def getFrom(element: Form, id: HtmlId) = Option(element.elements.namedItem(id.value))

  def getValueOfInput(inputId: HtmlId): Option[String] = get[HTMLInputElement](inputId).map(input => input.value)

  def getValueOfInputOrWarn(inputId: HtmlId): String =
    getValueOfInput(inputId).getOrElse {
      logger.warn(s"Could not get value of '$inputId'")
      ""
    }

  def getFilesOfInput(inputId: HtmlId): Seq[File] =
    get[HTMLInputElement](inputId).map(input => toSeq(input.files)).getOrElse(Seq())

  def getValueOfTextArea(textAreaId: HtmlId): Option[String] =
    get[HTMLTextAreaElement](textAreaId).map(textArea => textArea.value)

  def getValueOfTextAreaOrWarn(textAreaId: HtmlId): String = getValueOfTextArea(textAreaId).getOrElse {
    logger.warn(s"Could not get value of text area with ID '$textAreaId'")
    ""
  }

  def firstById(elements: Seq[HTMLInputElement], elemId: HtmlId): Option[HTMLInputElement] =
    elements.find(element => element.id == elemId.value)

  /**
    * Gets all the [[HTMLInputElement]]s on a [[HTMLFormElement]].
    *
    * @param form we get the input elements from this [[HTMLFormElement]]
    * @return all the [[HTMLInputElement]]s on the `form`
    */
  def getInputs(form: HTMLFormElement): Seq[HTMLInputElement] =
    mapOverCollection(form.elements, elem => as[HTMLInputElement](elem, logFailedCast = false)).flatten

  /**
    * Casts an `element` to a type `T`. If the cast fails, return [[None]].
    *
    * @param element       we cast this [[Element]] to `T`
    * @param logFailedCast whether to issue a log message if the cast failed. Defaults to `true`
    * @tparam T the type to which we cast `element`
    * @return `element` as a `T` or [[None]]
    */
  def as[T <: Element : ClassTag](element: Element, logFailedCast: Boolean = true): Option[T] = element match {
    case elemOfDesiredType: T => Some(elemOfDesiredType)
    case other => if (logFailedCast) logger.info(s"Element $element is $other")
      None
  }

  def create[T <: Element : ClassTag](typedTag: TypedTag[T]): T = {
    val untypedElem = document.createElement(typedTag.tag)

    untypedElem match {
      case elem: T => elem
      case other =>
        throw new ClassCastException(s"Element with tag $typedTag is $other")
    }
  }


  def forEachFile(fileList: FileList, action: (File => Unit)): Unit =
    (0 until fileList.length)
      .map(fileList.item)
      .foreach(action)


  def mapOverFiles[Res](fileList: FileList, mapper: (File => Res)): Seq[Res] =
    toSeq(fileList)
      .map(mapper)


  def toSeq(files: FileList): Seq[File] =
    (0 until files.length)
      .map(files.item)

  def mapOverCollection[Res](htmlCollection: HTMLCollection, mapper: (Element => Res)): IndexedSeq[Res] =
    (0 until htmlCollection.length)
      .map(htmlCollection.item)
      .map(mapper)

  def forEachElement(htmlCollection: HTMLCollection, action: (Element => Unit)): Unit =
    (0 until htmlCollection.length)
      .map(htmlCollection.item)
      .foreach(action)

  def get[T: ClassTag](elementId: HtmlId): Option[T] = {
    val queryResult = document.querySelector(s"#$elementId")
    queryResult match {
      case elem: T => Some(elem)
      //      case list: Seq[T] =>
      //        logger.info(s"There are ${list.size} elements with ID $elementId. Getting first.")
      //        list.headOption
      case other =>
        logger.info(s"Element with ID $elementId is $other")
        None
    }
  }

  def addClass(elementId: HtmlId, classToAdd: StyleA): Unit =
    get[Element](elementId).fold {
      logger.warn(s"Can't add class to element with ID $elementId since it wasn't found in the document")
    } { element => addClass(element, classToAdd) }

  def addClass(element: js.Any, classToAdd: StyleA): JQuery = jQuery(element).addClass(classToAdd.htmlClass)

  def removeClass(elementId: HtmlId, classToRemove: StyleA): Unit =
    get[Element](elementId).fold {
      logger.warn(s"Can't remove class from element with ID $elementId since it wasn't found in the document")
    } { element => removeClass(element, classToRemove) }

  def removeClass(element: js.Any, classToRemove: StyleA): JQuery = jQuery(element).removeClass(classToRemove.htmlClass)
}
