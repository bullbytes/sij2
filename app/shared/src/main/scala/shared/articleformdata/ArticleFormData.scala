package shared.articleformdata

/**
  * The data a merchant or supplier can enter about a promotional article in the article form.
  * <p>
  * Created by Matthias Braun on 9/11/2016.
  */
case class ArticleFormData(merchantOrSupplier: MerchantOrSupplier,
                           articleData: PromotionalArticle,
                           unbrandedArticlePrice: UnbrandedArticlePrice,
                           brandings: Seq[ArticleBranding],
                           minimumOrderQuantities: Seq[MinimumOrderQuantity],
                           images: Seq[ArticleFormImage],
                           merchantSpecificFiles: Seq[MerchantSpecificFile],
                           catalogData: CatalogData)

/**
  * Describes a promotional article.
  *
  * @param articleNumberAssignedByMerchantOrSupplier the article number that a merchant or supplier of the article
  *                                                  assigns. This number won't show up in the catalog. Rather,
  *                                                  the VÖW assigns a new number to the article which will be printed
  *                                                  in the catalog
  * @param name                                      the article's name
  * @param description                               describes the article
  * @param size                                      how big the article is
  * @param colorAndShape                             the colors and shapes this article comes in
  */
case class PromotionalArticle(articleNumberAssignedByMerchantOrSupplier: String, name: String, description: String,
                              size: String, colorAndShape: String)

/**
  * The branding (finishing) of a [[PromotionalArticle]].
  *
  * @param name         the branding's name
  * @param position     where the [[PromotionalArticle]] will be branded with this branding
  * @param scales       the costs of branding an article, described using [[ArticlePriceScale]]s
  * @param initialCosts the initial printing costs of the article
  * @param filmCosts    for some brandings, the producer has to create a film to print the branding on the article
  */
case class ArticleBranding(name: String, position: String, scales: Seq[ArticlePriceScale],
                           initialCosts: String, filmCosts: String)

/**
  * Describes how much an article in a certain price scale cost.
  *
  * @param name             the name of the price scale
  * @param numberOfArticles how many articles someone has to order to be in the price scale. For example "1 - 100".
  * @param price            the price of the article in this price scale
  */
case class ArticlePriceScale(name: String, numberOfArticles: String, price: String)

/**
  * Describes how much an article costs without any branding.
  *
  * @param unbrandedArticleScales the price of the articles in different [[ArticlePriceScale]]s
  * @param suggestedPrice         if there are no price scales for the article, the merchant or supplier can enter an
  *                               approximate, suggested price
  */
case class UnbrandedArticlePrice(unbrandedArticleScales: Seq[ArticlePriceScale], suggestedPrice: String)

/**
  * The minimum number of articles someone has to order to get the article.
  *
  * @param condition describes what has to be fulfilled for the minimum order quantity to be in effect. For example,
  *                  the ordered article must be branded
  * @param quantity  the number of articles someone has to order to get the article
  */
case class MinimumOrderQuantity(condition: String, quantity: String)

/**
  * An image that will be printed alongside the article description.
  *
  * @param url         the unified resource locator that leads to the image file
  * @param description describes the image
  */
case class ArticleFormImage(url: String, description: String)

/**
  * Merchants of promotional articles, in contrast to suppliers, can provide custom files (such as PDFs) that will be
  * included in the catalog.
  *
  * @param url         the unified resource locator where the merchant specific file is
  * @param description describes the file
  */
case class MerchantSpecificFile(url: String, description: String)

/**
  * We identify merchants and suppliers using this ID.
  *
  * @param id the string value of the merchant or supplier ID
  */
case class MerchantOrSupplierId(id: String) extends AnyVal {
  override def toString: String = id
}

case class Merchant(override val name: String, override val id: MerchantOrSupplierId) extends MerchantOrSupplier

case class Supplier(override val name: String, override val id: MerchantOrSupplierId) extends MerchantOrSupplier

/**
  * Data about a merchant or supplier
  */
sealed trait MerchantOrSupplier {
  /* The name of the merchant of supplier */
  def name: String

  /* The ID of the merchant of supplier */
  def id: MerchantOrSupplierId

  /* How many articles this merchant or supplier is allowed to create */
  def maxNrOfArticles = 1000
}

/**
  * The [[PromotionalArticle]] is printed in a catalog. This class contains information about the article in context
  * of the catalog.
  *
  * @param remarksOnCatalogCreation
  *                           merchants and suppliers can add remarks on how they want their article to appear in
  *                           the catalog
  * @param positionInCatalog
  *                           where in the catalog the article should appear. The first article has position 1.
  * @param category           the [[CatalogCategory]] to which this article belongs to
  * @param subCategory        the [[CatalogSubCategory]] to which this article belongs to
  * @param pageInCatalog      on which page the article is printed in the catalog
  * @param articleNrInCatalog the number of the article as it appears in the catalog. The VÖW assigns this number and it
  *                           will be different from the article number the merchant or supplier has assigned to their
  *                           article.
  */
case class CatalogData(remarksOnCatalogCreation: String, positionInCatalog: Int,
                       category: CatalogCategory, subCategory: CatalogSubCategory, pageInCatalog: Int,
                       articleNrInCatalog: String)

/**
  * [[PromotionalArticle]]s are grouped into categories like 'textiles', 'food', or 'technology' for the catalog.
  *
  * @param name the name of the category
  */
case class CatalogCategory(name: String) extends AnyVal {
  override def toString: String = name
}

object CatalogCategory {
  val unassigned: CatalogCategory = CatalogCategory("")
}

case class CatalogSubCategory(name: String) extends AnyVal {
  override def toString: String = name
}

object CatalogSubCategory {
  val unassigned: CatalogSubCategory = CatalogSubCategory("")
}

