package shared.articleformdata

/**
  * An [[ArticleFormData]] with its corresponding [[ArticleMetaInfo]].
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
case class ArticleWithMetaInfo(articleFormData: ArticleFormData, metaInfo: ArticleMetaInfo)

case class ArticleId(id: String) extends AnyVal {
  override def toString: String = id
}
