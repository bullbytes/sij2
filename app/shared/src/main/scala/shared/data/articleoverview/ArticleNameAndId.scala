package shared.data.articleoverview

import shared.articleformdata.ArticleId

/**
  * The name and the ID of a promotional article.
  * <p>
  * Created by Matthias Braun on 4/10/2017.
  */
case class ArticleNameAndId(name: String, id: ArticleId)

