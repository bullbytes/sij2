package shared.data.sortarticlespage

import shared.articleformdata.{ArticleId, CatalogData, MerchantOrSupplier}

/**
  * A promotional article as the page for sorting and categorizing all articles sees it.
  * <p>
  * Created by Matthias Braun on 4/10/2017.
  */
case class ArticleForSortArticlesPage(articleId: ArticleId, articleName: String, catalogData: CatalogData,
                                     articleDescription:String, merchantOrSupplier: MerchantOrSupplier,
                                     articleNrAssignedByPartner: String)
