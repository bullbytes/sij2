package shared.utils.conversions

/**
  * Adds new functions to a sequence, implicitly.
  * <p>
  * Created by Matthias Braun on 12/28/2016.
  */
object RichSeq {

  implicit class SeqUsingIndex[T](extendMe: Seq[T]) {

    def mapWithIndex[Res](mapper: (T, Int) => Res): Seq[Res] =
      extendMe.zipWithIndex.map { case (element, index) => mapper(element, index) }

    def forEachWithIndex(func: (T, Int) => Unit): Unit = mapWithIndex(func)
  }

}
