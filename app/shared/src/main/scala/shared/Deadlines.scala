package shared

import java.time.{LocalDate, Month}

import shared.articleformdata.MerchantOrSupplier
import shared.merchantsandsuppliers.MerchantsAndSuppliers

/**
  * Defines until when merchants and suppliers can add, change, or delete articles.
  * <p>
  * Created by Matthias Braun on 12/8/2016.
  */
object Deadlines {
  def deadlineHasPassedFor(merchantOrSupplier: MerchantOrSupplier): Boolean =
    MerchantsAndSuppliers.isMerchant(merchantOrSupplier) && merchantDeadlineHasPassed ||
      MerchantsAndSuppliers.isSupplier(merchantOrSupplier) && supplierDeadlineHasPassed

  // We can't use ZoneId or LocaleDateTime since scalajs-java-time doesn't implement it yet:
  // https://github.com/scala-js/scala-js-java-time/issues/7

  // That's UTC +1: https://www.timeanddate.com/worldclock/austria/vienna
  // private val viennaTimeZone = ZoneId.of("Europe/Vienna")

  private val merchantDeadline = LocalDate.of(2017, Month.DECEMBER, 9)
  private val supplierDeadline = LocalDate.of(2017, Month.DECEMBER, 16)

  // If it's the day after the day of the deadline, the deadline has passed
  def merchantDeadlineHasPassed: Boolean = LocalDate.now().isAfter(merchantDeadline)

  def supplierDeadlineHasPassed: Boolean = LocalDate.now().isAfter(supplierDeadline)
}
