package shared.pages

/**
  * This data about pages on our website is shared between client and server.
  * <p>
  * Created by Matthias Braun on 10/10/2016.
  */
object PageInfo {

  /**
    * A page info in this application must provide this information
    */
  trait SijPageInfo {
    def path: String

    def title: String

    def id: PageId
  }

  /**
    * Users can choose their company on this page.
    */
  object ChooseCompany extends SijPageInfo {

    val path = "choose-company"
    val title = "Unternehmen wählen"
    val id = PageId("chooseCompany")
  }

  /**
    * This page lets the user create a new promotion article by filling out a form.
    */
  object NewArticle extends SijPageInfo {
    val path = "new-article"

    val title = "Produktkatalog 2017"

    val id = PageId("newArticle")
  }

  /**
    * Shows the articles that users have entered in the forms.
    */
  object ArticleOverview extends SijPageInfo {

    val path = "article-overview"

    val title = "Ihre Artikel"

    val id = PageId("articleOverview")
  }

  object EditArticle extends SijPageInfo {
    val path = "edit-article"

    val title = "Artikel bearbeiten"

    val id = PageId("editArticle")
  }

  object AllArticles extends SijPageInfo {
    val path = "all-articles"

    val title = "Alle Artikel"

    val id = PageId("allArticles")
  }

  object ImportJsonArticles extends SijPageInfo {
    val path = "import-json-articles"
    val title = "Artikel aus JSON importieren"
    val id = PageId("importJsonArticles")
  }
}
