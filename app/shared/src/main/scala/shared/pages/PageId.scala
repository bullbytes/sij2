package shared.pages

/**
  * Identifies web pages. The client uses this to know which page it got from the server.
  * <p>
  * Created by Matthias Braun on 10/24/2016.
  */
case class PageId(id: String) extends AnyVal {
  override def toString: String = id
}
