package shared.files

/**
  * A simple abstraction over a file containing text.
  * <p>
  * Created by Matthias Braun on 4/5/2017.
  */
case class SijFile (name:String, content:String)
