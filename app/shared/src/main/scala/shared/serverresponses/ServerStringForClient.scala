package shared.serverresponses

/**
  * A string that the server uses in messages sent to the client.
  * @param value the actual string of the server for client
  */
case class ServerStringForClient(value: String) extends AnyVal {
  override def toString: String = value
}
