package shared.serverresponses

import shared.articleformdata.{ArticleId, ArticleWithMetaInfo}
import shared.data.sortarticlespage.ArticleForSortArticlesPage

/**
  * The client receives one of these responses from the server after sending it data or requesting data.
  * <p>
  * Created by Matthias Braun on 9/11/2016.
  */
sealed abstract class ServerResponse

case class OfferArticle(article: ArticleWithMetaInfo) extends ServerResponse

case class OfferArticles(articles: Seq[ArticleWithMetaInfo]) extends ServerResponse

case class OfferLatestArticleWithPreviousVersion(latestArticle: ArticleWithMetaInfo, prevVersion: Option[ArticleWithMetaInfo]) extends ServerResponse

case class GotArticleAndSavedIt(msg: ServerStringForClient) extends ServerResponse
case class GotArticleButCouldNotSave(msg: ServerStringForClient) extends ServerResponse

case class GotArticlesAndSavedThem(msg: ServerStringForClient) extends ServerResponse
case class GotArticlesButCouldNotSave(msg: ServerStringForClient) extends ServerResponse

case class CouldNotParseStringToArticles(msg: ServerStringForClient) extends ServerResponse

case class DeletedArticle(idOfDeletedArticle: ArticleId) extends ServerResponse

case class DeadlineHasPassed(msg: ServerStringForClient) extends ServerResponse

case class ServerError(msg: ServerStringForClient) extends ServerResponse

