package shared.serverresponses.sortarticlespage

import shared.data.sortarticlespage.ArticleForSortArticlesPage
import shared.serverresponses.ServerStringForClient

/**
  * These are the types of responses the server has for the page that lets the user sort articles.
  * <p>
  * Created by Matthias Braun on 4/10/2017.
  */
trait ServerResponseForSortArticlesPage

case class OfferArticlesToSortArticlesPage(article: Seq[ArticleForSortArticlesPage]) extends ServerResponseForSortArticlesPage

case class ServerError(msg: ServerStringForClient) extends ServerResponseForSortArticlesPage
