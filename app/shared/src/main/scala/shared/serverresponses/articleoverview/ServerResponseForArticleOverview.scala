package shared.serverresponses.articleoverview

import shared.data.articleoverview.ArticleNameAndId
import shared.serverresponses.ServerStringForClient

/**
  * These are the types of responses the server has for the page that shows the user an overview of a partner's articles.
  * <p>
  * Created by Matthias Braun on 4/10/2017.
  */
trait ServerResponseForArticleOverview

case class OfferArticlesToArticleOverview(article: Seq[ArticleNameAndId]) extends ServerResponseForArticleOverview

case class ServerError(msg: ServerStringForClient) extends ServerResponseForArticleOverview
