package shared.articles

import shared.articleformdata.{ArticleWithMetaInfo, CatalogData}
import shared.articles.categories.{ArticleCategories, CategoryIds}
import shared.articles.categories.subcategories.ArticleSubcategories

/**
  * Contains different ways of sorting [[ArticleWithMetaInfo]]s.
  * <p>
  * Created by Matthias Braun on 1/16/2017.
  */
object ArticleOrderings {


  private def catalogData(article: ArticleWithMetaInfo): CatalogData = article.articleFormData.catalogData

  /** The categories appear in the catalog in an order that the VÖW defined. The sort number is used to determine
    * where in the catalog the category appears: The category with sort number 1 comes first, then comes the category
    * with sort number 2 etc. */
  private def categorySortNr(article: ArticleWithMetaInfo) = ArticleCategories.sortNr(catalogData(article).category)


  /** The subcategories appear within the categories in the catalog in an order that the VÖW defined. The sort number
    * is used to determine where in the category the subcategory appears: The subcategory with sort number 1 comes first,
    * then comes the subcategory with sort number 2 etc. */
  private def subcategorySortNr(article: ArticleWithMetaInfo) = ArticleSubcategories.sortNr(catalogData(article).subCategory)

  /**
    * When we sort an article by its category and its subcategory we use the orderings defined by VÖW.
    */
  val byCategoryAndSubcategory: Ordering[ArticleWithMetaInfo] = Ordering.by(article =>
    (categorySortNr(article), subcategorySortNr(article)))

  val byCategoryAndPosition: Ordering[ArticleWithMetaInfo] = Ordering.by(article =>
    (categorySortNr(article), catalogData(article).positionInCatalog))

  val byMerchantOrSupplierName: Ordering[ArticleWithMetaInfo] = Ordering.by(_.articleFormData.merchantOrSupplier.name)

}
