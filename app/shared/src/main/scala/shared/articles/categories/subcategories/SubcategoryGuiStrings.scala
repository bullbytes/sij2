package shared.articles.categories.subcategories

import shared.articles.categories.subcategories.ArticleSubcategories.SubcategoryId
import shared.pages.GuiString
import slogging.LazyLogging

/**
  * Defines how we display the text of subcategories to the user.
  * <p>
  * Created by Matthias Braun on 1/20/2017.
  */
object SubcategoryGuiStrings extends LazyLogging {

  def subcategory(id: SubcategoryId): GuiString = getGuiString(id)

  // The article has a subcategory but its not among the ones we know
  val unknownSubcategory = GuiString("Unbekannte Subkategorie")

  // The article has not been assigned a subcategory yet
  val noSubcategory = GuiString("Keine Unterkategorie")

  private def getGuiString(id: SubcategoryId): GuiString = allSubcategories.getOrElse(id, {
    logger.warn(s"Unknown subcategory: '$id")
    unknownSubcategory
  })

  // The subcategories within the category "office"
  private val office = {
    import ArticleSubcategories.Office
    Map(
      Office.ballpens.id -> GuiString("Kugelschreiber"),
      Office.writingSets.id -> GuiString("Schreibsets"),
      Office.coloredPencils.id -> GuiString("Buntstifte"),
      Office.markerPens.id -> GuiString("Textmarker"),
      Office.stickyNotes.id -> GuiString("Haftis - PostIt"),
      Office.memoBoxes.id -> GuiString("Zettelboxen"),
      Office.notebooks.id -> GuiString("Notizbücher"),
      Office.folders.id -> GuiString("Mappen"),
      Office.mousepads.id -> GuiString("Mousepads"),
      Office.calculators.id -> GuiString("Rechner")
    )
  }
  // The subcategories within the category "USB, powerbanks, and accessories"
  private val usbPowerbanksAndAccessories = {
    import ArticleSubcategories.{UsbAndPowerbanksAndAccessories => Usb}
    Map(
      Usb.usbSticks.id -> GuiString("USB-Sticks"),
      Usb.powerbanks.id -> GuiString("Powerbanks"),
      Usb.adaptersAndConnectionCables.id -> GuiString("Adapter & Verbindungskabel"),
      Usb.solarChargers.id -> GuiString("Solarladegerät")
    )
  }

  // The subcategories within the category "technology"
  private val technology = {
    import ArticleSubcategories.{Technology => Tech}
    Map(
      Tech.loudspeakers.id -> GuiString("Lautsprecher"),
      Tech.radios.id -> GuiString("Radios"),
      Tech.drones.id -> GuiString("Drohnen")
    )
  }

  // The subcategories within the category "bags"
  private val bags = {
    import ArticleSubcategories.Bags
    Map(
      Bags.carryingBags.id -> GuiString("Tragetaschen"),
      Bags.travelBags.id -> GuiString("Reisetaschen"),
      Bags.shoulderBags.id -> GuiString("Umhängetaschen"),
      Bags.rucksacks.id -> GuiString("Rucksäcke")
    )
  }

  // The subcategories within the category "food"
  private val food = {
    import ArticleSubcategories.Food
    Map(
      Food.sweets.id -> GuiString("Süßes"),
      Food.saltyFood.id -> GuiString("Salziges"),
      Food.drinks.id -> GuiString("Getränke"),
      Food.tea.id -> GuiString("Tee"),
      Food.giftSets.id -> GuiString("Geschenksets")
    )
  }

  // The subcategories within the category "utilities"
  private val utilities = {
    import ArticleSubcategories.{Utilities => Utils}
    Map(
      Utils.matchsticks.id -> GuiString("Zündhölzer"),
      Utils.lighters.id -> GuiString("Feuerzeuge"),
      Utils.keychains.id -> GuiString("Schlüsselanhänger"),
      Utils.coinBanks.id -> GuiString("Spardosen"),
      Utils.beauty.id -> GuiString("Beauty"),
      Utils.wallets.id -> GuiString("Geldtaschen"),
      Utils.sundries.id -> GuiString("Diverses"),
      Utils.phoneAccessories.id -> GuiString("Handy-Zubehör"),
      Utils.multitool.id -> GuiString("Multitool"),
      Utils.torches.id -> GuiString("Taschenlampen"),
      Utils.umbrellas.id -> GuiString("Schirme")
    )
  }
  // The subcategories within the category "leisure and games"
  private val leisureAndGames = {
    import ArticleSubcategories.{LeisureAndGames => Leisure}
    Map(
      Leisure.fitnessAccessories.id -> GuiString("Fitnesszubehör"),
      Leisure.drinkingBottles.id -> GuiString("Trinkflaschen"),
      Leisure.thermosbottles.id -> GuiString("Thermosflaschen & -becher"),
      Leisure.leisure.id -> GuiString("Freizeit"),
      Leisure.plushToys.id -> GuiString("Stofftiere & Figuren"),
      Leisure.fanMerchandise.id -> GuiString("Fanartikel")
    )
  }

  // The subcategories within the category "home and travel"
  private val homeAndTravel = {
    import ArticleSubcategories.{HomeAndTravel => HaT}
    Map(
      HaT.porcelainAndCeramics.id -> GuiString("Porzellan & Keramik"),
      HaT.glasswareAndCarafes.id -> GuiString("Glaswaren & Karaffen"),
      HaT.stockCans.id -> GuiString("Vorratsdosen"),
      HaT.kitchenAccessories.id -> GuiString("Küchenzubehör"),
      HaT.teaAccessories.id -> GuiString("Teezubehör"),
      HaT.giftSets.id -> GuiString("Geschenksets"),
      HaT.growingThings.id -> GuiString("Wachsendes"),
      HaT.blankets.id -> GuiString("Decken"),
      HaT.watches.id -> GuiString("Uhren"),
      HaT.shoppingBasketsAndBoxes.id -> GuiString("Einkaufskorb & -boxen"),
      HaT.suitCases.id -> GuiString("Koffer"),
      HaT.toiletryBags.id -> GuiString("Kulturbeutel")
    )
  }
  // The subcategories within the category "textiles"
  private val textiles = {
    import ArticleSubcategories.{Textiles => Tex}
    Map(
      Tex.tShirts.id -> GuiString("T-Shirts"),
      Tex.poloShirts.id -> GuiString("Poloshirts"),
      Tex.shirtsAndBlouses.id -> GuiString("Hemden & Blusen"),
      Tex.pullovers.id -> GuiString("Pullover & Sweater"),
      Tex.fleece.id -> GuiString("Fleece"),
      Tex.softshell.id -> GuiString("Softshell"),
      Tex.jackets.id -> GuiString("Jacken"),
      Tex.hatsAndCaps.id -> GuiString("Kappen & Hauben"),
      Tex.sportClothes.id -> GuiString("Sportbekleidung"),
      Tex.traditionalCostumes.id -> GuiString("Trachten"),
      Tex.workClothes.id -> GuiString("Arbeitsbekleidung"),
      Tex.tiesAndScarfs.id -> GuiString("Krawatten & Tücher & Schals"),
      Tex.terrycloths.id -> GuiString("Frotteewaren")
    )
  }

  private val allSubcategories: Map[SubcategoryId, GuiString] = office ++ usbPowerbanksAndAccessories ++ technology ++
    bags ++ food ++ utilities ++ leisureAndGames ++ homeAndTravel ++ textiles ++
    // The subcategory name for when no subcategory was assigned to the article
    Map(ArticleSubcategories.none.id -> noSubcategory)
}
