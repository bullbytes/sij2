package shared.articles.categories.subcategories

import shared.articleformdata.CatalogSubCategory
import shared.articles.categories.ArticleCategories.ArticleCategory
import shared.articles.categories.CategoryIds
import shared.articles.categories.CategoryIds.CategoryId
import shared.pages.GuiString
import slogging.LazyLogging

/**
  * The subcategories of an [[ArticleCategory]].
  * <p>
  * Created by Matthias Braun on 12/12/2016.
  */
object ArticleSubcategories extends LazyLogging {


  /**
    * The subcategories should appear in a certain order within their category in the catalog.
    * The sort number defines where in the category the subcategory appears:
    * The subcategory with sort number 1 comes first, then comes the subcategory with sort number 2 and so on.
    *
    * If the `subcategory` is unknown, we return `Integer.MAX_VALUE` so the article with that category appears last.
    */
  def sortNr(subcategory: CatalogSubCategory): Int =
  // Get the richer ArticleSubcategory (which has an ID) from the legacy type CatalogSubCategory
    fromName(GuiString(subcategory.name))
      .map(sortNr)
      .getOrElse({
        logger.warn(s"Could not get number to sort an article of subcategory '$subcategory'")
        Integer.MAX_VALUE
      })

  def sortNr(subcategory: ArticleSubcategory): Int =
    SubcategoryOrdering.sortNr(subcategory).getOrElse({
      logger.warn(s"Could not get number to sort an article of subcategory '$subcategory'")
      Integer.MAX_VALUE
    })

  // The subcategory for articles that don't have a subcategory yet
  val none: ArticleSubcategory = fromId("no-subcategory-id")

  val all: Seq[ArticleSubcategory] = Office.all ++ UsbAndPowerbanksAndAccessories.all ++ Technology.all ++ Bags.all ++
    Food.all ++ Utilities.all ++ LeisureAndGames.all ++ HomeAndTravel.all ++ Textiles.all :+ none

  def getById(subcategoryId: SubcategoryId): Option[ArticleSubcategory] = subcategoriesById.get(subcategoryId)

  private val subcategoriesById = all.map(subcategory => subcategory.id -> subcategory).toMap
  private val subcategoriesByName: Map[GuiString, ArticleSubcategory] =
    all.map(subcategory => SubcategoryGuiStrings.subcategory(subcategory.id) -> subcategory).toMap +
      // If an article hasn't been assigned any subcategory on the "all articles" page, its subcategory name is empty
      (GuiString("") -> none)

  /**
    * Gets the subcategories for the given category.
    */
  def getFor(id: CategoryId): Seq[ArticleSubcategory] = categoriesAndSubcategories.getOrElse(id, {
    logger.warn(s"Unknown category: '$id'")
    Seq()
  }
  )

  def fromName(subcategoryName: GuiString): Option[ArticleSubcategory] = {
    val subcategoryMaybe = subcategoriesByName.get(subcategoryName)
    if (subcategoryMaybe.isEmpty) {
      logger.warn(s"Could not get article subcategory for name $subcategoryName")
    }
    subcategoryMaybe
  }

  /*
   * Gets the richer ArticleSubcategory (which has an ID) from the legacy type CatalogSubCategory
   */
  def convert(subCategory: CatalogSubCategory): Option[ArticleSubcategory] = fromName(GuiString(subCategory.name))

  /**
    * Creates an [[ArticleSubcategory]] from an ID.
    */
  private def fromId(id: String) = ArticleSubcategory(SubcategoryId(id))


  object Office {
    val ballpens: ArticleSubcategory = fromId("ballpens-id")
    val writingSets: ArticleSubcategory = fromId("writing-sets-id")
    val coloredPencils: ArticleSubcategory = fromId("colored-pencils-id")
    val markerPens: ArticleSubcategory = fromId("marker-pens-id")
    val stickyNotes: ArticleSubcategory = fromId("sticky-notes-id")
    val memoBoxes: ArticleSubcategory = fromId("memo-boxes-id")
    val notebooks: ArticleSubcategory = fromId("notebooks-id")
    val folders: ArticleSubcategory = fromId("folders-id")
    val mousepads: ArticleSubcategory = fromId("mousepads-id")
    val calculators: ArticleSubcategory = fromId("calculators-id")

    val all = Seq(
      ballpens,
      writingSets,
      coloredPencils,
      markerPens,
      stickyNotes,
      memoBoxes,
      notebooks,
      folders,
      mousepads,
      calculators
    )
  }

  object UsbAndPowerbanksAndAccessories {

    val usbSticks: ArticleSubcategory = fromId("usb-sticks-id")
    val powerbanks: ArticleSubcategory = fromId("powerbanks-id")
    val adaptersAndConnectionCables: ArticleSubcategory = fromId("adapters-and-connection-cables-id")
    val solarChargers: ArticleSubcategory = fromId("solar-chargers-id")

    val all = Seq(
      usbSticks,
      powerbanks,
      adaptersAndConnectionCables,
      solarChargers
    )
  }

  object Technology {

    val loudspeakers: ArticleSubcategory = fromId("loudspeakers-id")
    val radios: ArticleSubcategory = fromId("radios-id")
    val drones: ArticleSubcategory = fromId("drones-id")

    val all = Seq(loudspeakers, radios, drones)
  }

  object Bags {

    val carryingBags: ArticleSubcategory = fromId("carrying-bags-id")
    val travelBags: ArticleSubcategory = fromId("travel-bags-id")
    val shoulderBags: ArticleSubcategory = fromId("shoulder-bags-id")
    val rucksacks: ArticleSubcategory = fromId("rucksacks-id")

    val all = Seq(
      carryingBags,
      travelBags,
      shoulderBags,
      rucksacks
    )
  }

  object Food {

    val sweets: ArticleSubcategory = fromId("sweets-id")
    val saltyFood: ArticleSubcategory = fromId("salty-food-id")
    val drinks: ArticleSubcategory = fromId("drinks-id")
    val tea: ArticleSubcategory = fromId("teas-id")
    // There's another subcategory for gift sets in category "Home And Travel"
    val giftSets: ArticleSubcategory = fromId("food-gift-sets-id")

    val all = Seq(
      sweets,
      saltyFood,
      drinks,
      tea,
      giftSets
    )
  }

  object Utilities {

    val matchsticks: ArticleSubcategory = fromId("matchsticks-id")
    val lighters: ArticleSubcategory = fromId("lighters-id")
    val keychains: ArticleSubcategory = fromId("keychains-id")
    val coinBanks: ArticleSubcategory = fromId("coin-banks-id")
    val beauty: ArticleSubcategory = fromId("beauty-id")
    val wallets: ArticleSubcategory = fromId("wallets-id")
    val sundries: ArticleSubcategory = fromId("sundries-id")
    val phoneAccessories: ArticleSubcategory = fromId("phone-accessories-id")
    val multitool: ArticleSubcategory = fromId("multitool-id")
    val torches: ArticleSubcategory = fromId("torches-id")
    val umbrellas: ArticleSubcategory = fromId("umbrellas-id")

    val all = Seq(
      matchsticks,
      lighters,
      keychains,
      coinBanks,
      beauty,
      wallets,
      sundries,
      phoneAccessories,
      multitool,
      torches,
      umbrellas
    )
  }

  object LeisureAndGames {
    val fitnessAccessories: ArticleSubcategory = fromId("fitness-accessories-id")
    val drinkingBottles: ArticleSubcategory = fromId("drinking-bottles-id")
    val thermosbottles: ArticleSubcategory = fromId("thermosbottles-id")
    val leisure: ArticleSubcategory = fromId("leisure-id")
    val plushToys: ArticleSubcategory = fromId("plush-toys-id")
    val fanMerchandise: ArticleSubcategory = fromId("fan-merchandise-id")

    val all = Seq(
      fitnessAccessories,
      drinkingBottles,
      thermosbottles,
      leisure,
      plushToys,
      fanMerchandise
    )
  }

  object HomeAndTravel {
    val porcelainAndCeramics: ArticleSubcategory = fromId("porcelain-and-ceramics-id")
    val glasswareAndCarafes: ArticleSubcategory = fromId("glassware-and-carafes-id")
    val stockCans: ArticleSubcategory = fromId("stock-cans-id")
    val kitchenAccessories: ArticleSubcategory = fromId("kitchen-accessories-id")
    val teaAccessories: ArticleSubcategory = fromId("tea-accessories-id")
    // There's another subcategory for gift sets in category "Food"
    val giftSets: ArticleSubcategory = fromId("home-and-travel-gift-sets-id")
    val growingThings: ArticleSubcategory = fromId("growing-things-id")
    val blankets: ArticleSubcategory = fromId("blankets-id")
    val watches: ArticleSubcategory = fromId("watches-id")
    val shoppingBasketsAndBoxes: ArticleSubcategory = fromId("shopping-baskets-and-boxes-id")
    val suitCases: ArticleSubcategory = fromId("suit-cases-id")
    val toiletryBags: ArticleSubcategory = fromId("toiletry-bags-id")

    val all = Seq(
      porcelainAndCeramics,
      glasswareAndCarafes,
      stockCans,
      kitchenAccessories,
      teaAccessories,
      giftSets,
      growingThings,
      blankets,
      watches,
      shoppingBasketsAndBoxes,
      suitCases,
      toiletryBags
    )
  }

  object Textiles {

    val tShirts: ArticleSubcategory = fromId("t-shirt-id")
    val poloShirts: ArticleSubcategory = fromId("polo-shirt-id")
    val shirtsAndBlouses: ArticleSubcategory = fromId("shirts-and-blouses-id")
    val pullovers: ArticleSubcategory = fromId("pullovers-id")
    val fleece: ArticleSubcategory = fromId("fleece-id")
    val softshell: ArticleSubcategory = fromId("softshell-id")
    val jackets: ArticleSubcategory = fromId("jackets-id")
    val hatsAndCaps: ArticleSubcategory = fromId("hats-and-caps-id")
    val sportClothes: ArticleSubcategory = fromId("sport-clothes-id")
    val traditionalCostumes: ArticleSubcategory = fromId("traditional-costumes-id")
    val workClothes: ArticleSubcategory = fromId("work-clothes-id")
    val tiesAndScarfs: ArticleSubcategory = fromId("ties-and-scarfs-id")
    val terrycloths: ArticleSubcategory = fromId("terrycloths-id")

    val all = Seq(
      tShirts,
      poloShirts,
      shirtsAndBlouses,
      pullovers,
      fleece,
      softshell,
      jackets,
      hatsAndCaps,
      sportClothes,
      traditionalCostumes,
      workClothes,
      tiesAndScarfs,
      terrycloths
    )
  }

  private val categoriesAndSubcategories: Map[CategoryId, Seq[ArticleSubcategory]] = Map(
    CategoryIds.office -> Office.all,
    CategoryIds.usbAndPowerbanksAndAccessories -> UsbAndPowerbanksAndAccessories.all,
    CategoryIds.technology -> Technology.all,
    CategoryIds.bags -> Bags.all,
    CategoryIds.food -> Food.all,
    CategoryIds.utilities -> Utilities.all,
    CategoryIds.leisureAndGames -> LeisureAndGames.all,
    CategoryIds.homeAndTravel -> HomeAndTravel.all,
    CategoryIds.textiles -> Textiles.all,
    CategoryIds.none -> Seq(none)
  )

  case class ArticleSubcategory(id: SubcategoryId) {
    override def toString: String = id.value
  }

  case class SubcategoryId(value: String) extends AnyVal {
    override def toString: String = value
  }

}
