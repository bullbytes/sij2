package shared.articles.categories

import shared.articleformdata.CatalogCategory
import shared.articles.categories.CategoryIds.CategoryId
import shared.articles.categories.subcategories.ArticleSubcategories
import shared.articles.categories.subcategories.ArticleSubcategories.ArticleSubcategory
import shared.pages.GuiString
import slogging.LazyLogging

/**
  * Describes which categories for a promotional article there are.
  * <p>
  * Created by Matthias Braun on 12/12/2016.
  */
object ArticleCategories extends LazyLogging {


  /**
    * The categories should appear in a certain order in the catalog. The sort number defines where in the catalog the
    * category appears: The category with sort number 1 comes first, then comes the category with sort number 2 and so on.
    *
    * If the `category` is unknown, we return `Integer.MAX_VALUE` so the article with that category appears last.
    */
  def sortNr(category: CatalogCategory): Int =
  // Get the richer ArticleCategory (which has an ID) from the database type CatalogCategory
    fromName(GuiString(category.name)).map(sortNr)
//      .flatMap(articleCategory => categoriesAndSortNumbers.get(articleCategory.id))
      .getOrElse({
        logger.warn(s"Could not get number to sort an article of category '$category'")
        Integer.MAX_VALUE
      })

  def sortNr(category:ArticleCategory):Int = categoriesAndSortNumbers.getOrElse(category.id, {
    logger.warn(s"Could not get number to sort an article of category '$category'")
    Integer.MAX_VALUE
  })

  def abbreviationFor(category: CatalogCategory): Option[CategoryAbbreviation] =
    categoryNameToAbbreviation.get(GuiString(category.name))

  def fromName(categoryName: GuiString): Option[ArticleCategory] = {
    val categoryMaybe = categoriesByName.get(categoryName)
    if (categoryMaybe.isEmpty) {
      logger.warn(s"Could not get article category for name $categoryName")
    }
    categoryMaybe
  }

  /*
   * Gets the richer ArticleCategory (which has an ID) from the legacy type CatalogCategory
   */
  def convert(category: CatalogCategory): Option[ArticleCategory] = fromName(GuiString(category.name))

  private def category(id: CategoryId, abbreviation: String) =
    ArticleCategory(id, ArticleSubcategories.getFor(id), CategoryAbbreviation(abbreviation))

  def get(id: CategoryId): Option[ArticleCategory] = categoriesById.get(id)

  val none: ArticleCategory = category(CategoryIds.none, "???")

  val all: Seq[ArticleCategory] = {

    import CategoryIds._
    Seq(
      category(office, "SB"),
      category(usbAndPowerbanksAndAccessories, "UPZ"),
      category(technology, "TEC"),
      category(bags, "TAS"),
      category(food, "L"),
      category(utilities, "N"),
      category(leisureAndGames, "FS"),
      category(homeAndTravel, "HR"),
      category(textiles, "TEX"),
      ArticleCategories.none
    )
  }

  /* Defines in which order the categories appear in the catalog. Category with sort number 1 comes first,
  then comes category number 2, etc. */
  private val categoriesAndSortNumbers = {
    import CategoryIds._

    Map(
      office -> 1,
      usbAndPowerbanksAndAccessories -> 2,
      technology -> 3,
      bags -> 4,
      food -> 5,
      utilities -> 6,
      leisureAndGames -> 7,
      homeAndTravel -> 8,
      textiles -> 9,
      CategoryIds.none -> Integer.MAX_VALUE
    )
  }


  private val categoriesById: Map[CategoryId, ArticleCategory] = all.map(category => category.id -> category).toMap

  private val categoriesByName: Map[GuiString, ArticleCategory] =
    all.map(category => CategoryGuiStrings.nameFromId(category.id) -> category).toMap +
      // If an article hasn't been assigned any category on the "all articles" page, its category name is empty
      (GuiString("") -> ArticleCategories.none)

  private val categoryNameToAbbreviation = categoriesByName.map { case (categoryName, category) => categoryName -> category.abbreviation }

  case class CategoryAbbreviation(value: String) extends AnyVal {
    override def toString: String = value
  }

  case class ArticleCategory(id: CategoryId, subcategories: Seq[ArticleSubcategory], abbreviation: CategoryAbbreviation)

}
