package shared.articles.categories

import shared.articles.categories.CategoryIds.CategoryId
import shared.pages.GuiString
import slogging.LazyLogging

/**
  * Defines how we display the text of categories to the user.
  * <p>
  * Created by Matthias Braun on 1/20/2017.
  */
object CategoryGuiStrings extends LazyLogging {

  val unknownCategory = GuiString("Unbekannte Kategorie")

  val categoryNames: Map[CategoryId, GuiString] = {
    import CategoryIds._
    Map(
      office -> GuiString("Schreibgeräte & Büro"),
      usbAndPowerbanksAndAccessories -> GuiString("USB & Powerbanks & Zubehör"),
      technology -> GuiString("Technik"),
      bags -> GuiString("Taschen"),
      food -> GuiString("Lebensmittel"),
      utilities -> GuiString("Nützliches"),
      leisureAndGames -> GuiString("Freizeit & Spiel"),
      homeAndTravel -> GuiString("Haushalt & Reise"),
      textiles -> GuiString("Textilien"),
      none -> GuiString("Keine Kategorie")
    )
  }

  /**
    * Gets the name of the category by its ID.
    */
  def nameFromId(id: CategoryId): GuiString =
    categoryNames.getOrElse(id, {
      logger.warn(s"Unknown category ID: '$id'")
      unknownCategory
    })
}
