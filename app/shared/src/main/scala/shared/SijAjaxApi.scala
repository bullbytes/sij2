package shared

import shared.articleformdata.{ArticleFormData, ArticleId, ArticleWithMetaInfo, MerchantOrSupplierId}
import shared.data.sortarticlespage.ArticleForSortArticlesPage
import shared.files.SijFile
import shared.serverresponses.ServerResponse
import shared.serverresponses.articleoverview.ServerResponseForArticleOverview
import shared.serverresponses.sortarticlespage.ServerResponseForSortArticlesPage

import scala.concurrent.Future

/**
  * Defines which AJAX requests the client can make to the server.
  * <p>
  * Created by Matthias Braun on 9/4/2016.
  */
trait SijAjaxApi {

  // Autowire doesn't support method overloading: https://github.com/lihaoyi/autowire#limitations

  def saveNewArticle(data: ArticleFormData): Future[ServerResponse]

  def saveNewVersionOfArticle(dataWithId: ArticleWithMetaInfo): Future[ServerResponse]

  def saveNewVersionOfArticles(articlesToSave: Seq[ArticleForSortArticlesPage]): Future[ServerResponse]

  def saveArticlesFromJsonFile(fileWithArticles: SijFile): Future[ServerResponse]

  def getLatestAndPreviousArticle(id: ArticleId): Future[ServerResponse]

  def getLatestArticlesForSortArticlesPage(): Future[ServerResponseForSortArticlesPage]

  def getLatestArticlesOf(id: MerchantOrSupplierId): Future[ServerResponse]
  def getNameAndIdForLatestArticlesOf(id: MerchantOrSupplierId): Future[ServerResponseForArticleOverview]

  def deleteArticle(articleId: ArticleId): Future[ServerResponse]
}
