package utils

import shared.articleformdata.ArticleWithMetaInfo

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.control.Exception.allCatch

/**
  * Helps reading and writing the JavaScript Object Notation.
  * <p>
  * Created by Matthias Braun on 1/13/2017.
  */
object JsonUtil {
  def toJson(articles: Seq[ArticleWithMetaInfo]): String = upickle.json.write(upickle.default.writeJs(articles))

  def articlesFromJson(json: String): Future[Either[Throwable, Seq[ArticleWithMetaInfo]]] =
  // Use a future since the parsing might take a while and we don't want to block the server
    Future {
      allCatch.either(upickle.default.read[Seq[ArticleWithMetaInfo]](json))
    }
}
