package utils

import slogging.LazyLogging

/**
  * Helps with logging.
  * <p>
  * Created by Matthias Braun on 4/10/2017.
  */
object LogUtil extends LazyLogging {

  def logWithStackTrace(error: Throwable): Unit = {
    val msgMaybe = Option(error.getMessage)
    msgMaybe.foreach(msg => logger.warn(msg))
    val stackTrace = error.getStackTrace
    stackTrace.foreach(stackElement => logger.warn(stackElement.toString))
  }

}
