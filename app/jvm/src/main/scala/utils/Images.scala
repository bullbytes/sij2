package utils

import java.nio.charset.StandardCharsets
import java.util.Base64

import slogging.LazyLogging

import scala.io.Source

/**
  * Helps dealing with images.
  * <p>
  * Created by Matthias Braun on 10/23/2016.
  */
object Images extends LazyLogging {

  def fromBase64(base64Image: String): Either[Throwable, Array[Byte]] = {

   // val base64ImageInUtf8 = Source.fromBytes(base64Image.getBytes(), "UTF-8").mkString
    val partSeparator = ","
    Strings.subAfter(base64Image, partSeparator).fold {
      decodeBase64String(base64Image)
    } { imageData =>
      decodeBase64String(imageData)
    }
  }

  def decodeBase64String(encoded: String): Either[Throwable, Array[Byte]] = {
    try {
      val decoded = Base64.getDecoder.decode(encoded.getBytes(StandardCharsets.UTF_8))
      Right(decoded)
    }
    catch {
      case e: Throwable =>
        logger.warn(s"Could not decode string: $encoded")
        Left(e)
    }
  }
}
