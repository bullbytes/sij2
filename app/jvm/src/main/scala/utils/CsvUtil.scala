package utils

import shared.articleformdata._
import slogging.LazyLogging

/**
  * Converts Scala objects to lists of comma-separated values
  * <p>
  * Created by Matthias Braun on 1/14/2017.
  */
object CsvUtil extends LazyLogging {

  // Separates values from each other
  private val ValueSep = ", "

  private def cleanAndQuote (str:String):String={
    def quote(str: String): String = "\"" + str + "\""
    def removeLineBreaks(str: String) = str.replaceAll("""\r?\n""", "")

    def removeCommas(str: String) = str.replaceAll(",", " ")

    quote(removeCommas(removeLineBreaks(str)))
  }

  /**
    * Surround each string within `strings` with double quotes, remove the string's line breaks so Excel doesn't
    * start a new row, remove commas so Excel doesn't start a new cell, and concatenate the `strings` with `ValueSep`.
    * It's unclear why removing commas and newlines is necessary since we quote the fields.
    */
  private def cleanQuoteAndJoin(strings: Seq[String]): String =
    strings.map(cleanAndQuote).mkString(ValueSep)

  private def toCsv(catalogData: CatalogData): String = cleanQuoteAndJoin(
    Seq(catalogData.category.toString,
      catalogData.subCategory.toString, catalogData.articleNrInCatalog.toString, catalogData.remarksOnCatalogCreation,
      catalogData.positionInCatalog.toString, catalogData.pageInCatalog.toString)
  )

  private def toCsv(article: PromotionalArticle): String =
    cleanQuoteAndJoin(Seq(
      article.name, article.description,
      article.articleNumberAssignedByMerchantOrSupplier,
      article.colorAndShape, article.size
    ))

  private def toCsv(priceScale: ArticlePriceScale): String = cleanQuoteAndJoin(
    Seq(priceScale.name, priceScale.numberOfArticles, priceScale.price)
  )

  private def priceScalesToCsv(scales: Seq[ArticlePriceScale]): String = scales.map(toCsv).mkString(ValueSep)

  private def toCsv(branding: ArticleBranding): String = cleanQuoteAndJoin(
    Seq(branding.name, branding.position, branding.initialCosts, branding.filmCosts)) + ValueSep +
    priceScalesToCsv(branding.scales)

  private def brandingsToCsv(brandings: Seq[ArticleBranding]): String = brandings.map(toCsv).mkString(ValueSep)

  private def toCsv(image: ArticleFormImage): String = cleanQuoteAndJoin(Seq(image.description, image.url))

  private def imageUrlsToCsv(images: Seq[ArticleFormImage]) = images.map(toCsv).mkString(ValueSep)

  private def toCsv(moq: MinimumOrderQuantity): String = cleanQuoteAndJoin(Seq(moq.condition, moq.quantity))

  private def moqsToCsv(minimumOrderQuantities: Seq[MinimumOrderQuantity]): String = minimumOrderQuantities.map(toCsv).mkString(ValueSep)

  private def toCsv(merchantOrSupplier: MerchantOrSupplier): String = cleanAndQuote(merchantOrSupplier.name)

 private def toCsv(unbrandedArticlePrice: UnbrandedArticlePrice): String =
    cleanAndQuote(unbrandedArticlePrice.suggestedPrice) + ValueSep +
      unbrandedArticlePrice.unbrandedArticleScales.map(toCsv).mkString(ValueSep)

  def toCsvRow(article: ArticleFormData): String = Seq(
    toCsv(article.merchantOrSupplier), toCsv(article.articleData),
    toCsv(article.unbrandedArticlePrice), brandingsToCsv(article.brandings),
    imageUrlsToCsv(article.images), moqsToCsv(article.minimumOrderQuantities), toCsv(article.catalogData)
  ).mkString(ValueSep)

  def toCsv(articles: Seq[ArticleFormData]): String = articles.map(toCsvRow).mkString(System.lineSeparator())
}
