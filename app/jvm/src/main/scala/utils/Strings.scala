package utils

/**
  * <p>
  * Created by Matthias Braun on 10/22/2016.
  */
object Strings {

  /**
    * Gets every character of `original` after the first occurrence of string `afterThis`.
    * <p>
    * For example when `original` is 'something.else' and `afterThis` is '.' then the return substring is 'else'.
    *
    * @param original  original string from which we want a substring
    * @param afterThis take everything from the original string after this string
    * @return a substring from the first character after `start` till the
    *         end of `original`. If `original` does not contain `start`, return `None`
    */
  def subAfter(original: String, afterThis: String) =
  if (original.contains(afterThis))
    Some(original.substring(original.indexOf(afterThis) + afterThis.length, original.length))
  else
    None
}
