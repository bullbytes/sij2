package utils

/**
  * Helps with dealing with time.
  * <p>
  * Created by Matthias Braun on 4/6/2017.
  */
object TimeUtil {
  def nanosToSeconds(nanos: Long): Double = nanos.toDouble / 1000000000.0
}
