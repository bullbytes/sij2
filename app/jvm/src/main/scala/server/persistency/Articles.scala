package server.persistency

import java.util.UUID

import server.persistency.postgres.PostgresConnector
import server.versioning.ArticleVersions
import shared.articleformdata._
import shared.data.articleoverview.ArticleNameAndId
import shared.data.sortarticlespage.ArticleForSortArticlesPage
import slogging.LazyLogging

import scala.concurrent.Future
import scala.util.{Success, Try}

/**
  * A repository to save and get promotional articles.
  * Provides a layer of abstraction over the concrete database operations to save and get those articles.
  * <p>
  * Created by Matthias Braun on 12/10/2016.
  */
object Articles extends LazyLogging {


  // Connects to our CouchDB database
  //private val dbConnector = CouchDbConnector(DbConfig.connectionInfo)

  private val dbConnector = PostgresConnector

  private def addMetaInfo(formData: ArticleFormData) = {
    val articleId = ArticleId(UUID.randomUUID().toString)
    val creationTime = System.currentTimeMillis()
    val firstVersion = VersionCount.firstVersion
    ArticleWithMetaInfo(formData, ArticleMetaInfo(articleId, firstVersion, creationTime))
  }

  //def saveFirstVersion(formData: ArticleFormData): Throwable \/ DocOk = dbConnector.add(formData)
  def saveFirstVersion(formData: ArticleFormData)= dbConnector.add(addMetaInfo(formData))

  //  def saveNewVersion(articleWithMetaInfo: ArticleWithMetaInfo): Throwable \/ DocOk = {
  //
  //    dbConnector.add(incrementVersionAndSetCreationDate(articleWithMetaInfo))
  //  }
  def saveNewVersion(articleWithMetaInfo: ArticleWithMetaInfo) =
  dbConnector.add(incrementVersionAndSetCreationDate(articleWithMetaInfo))

  private def incrementVersionAndSetCreationDate(article: ArticleWithMetaInfo): ArticleWithMetaInfo = {
    val articleWithNewVersion = ArticleVersions.incrementVersion(article)
    ArticleVersions.setCreationDate(articleWithNewVersion, System.currentTimeMillis)
  }

  def saveNewVersions(articlesToSave: Seq[ArticleWithMetaInfo]) =
    dbConnector.add(articlesToSave.map(incrementVersionAndSetCreationDate))

  //  def saveNewVersions(articlesToSave: Seq[ArticleWithMetaInfo]): Throwable \/ Seq[DocOk] =
  //    dbConnector.add(articlesToSave.map(incrementVersionAndSetCreationDate))

  //def delete(articleId: ArticleId): Throwable \/ Seq[DocOk] = dbConnector.delete(articleId)
  def delete(articleId: ArticleId) = dbConnector.delete(articleId)

//  private def latestArticles(articles: Seq[ArticleWithMetaInfo]) = {
//    // Group the articles by their ID
//    val articlesById = articles.groupBy(_.metaInfo.articleId)
//    // Get the latest version of each of the articles
//    articlesById.flatMap { case (articleId, articlesWithSameId) => latestVersion(articlesWithSameId) }.toSeq
//  }

  def latestArticlesOf(id: MerchantOrSupplierId) = dbConnector.getLatestArticlesOf(id)


  def latestArticlesOf(ids: Seq[MerchantOrSupplierId]) = dbConnector.getLatestArticlesOf(ids)

  //  def latestArticlesOf(ids: Seq[MerchantOrSupplierId]): Throwable \/ Seq[ArticleWithMetaInfo] = {
  //
  //    // We get the articles for each merchant or supplier. Then we either concatenate all the articles or return
  //    // the first error that occurred in the database
  //
  //    // This is the initial accumulator for the fold below
  //    val emptyListRight = \/-(Nil): Throwable \/ Seq[ArticleWithMetaInfo]
  //    ids.map(id => latestArticlesOf(id)).foldRight(emptyListRight) {
  //      (dbResult, acc) =>
  //        for (accumulatedArticles <- acc; articlesFromDbResult <- dbResult)
  //          yield articlesFromDbResult ++ accumulatedArticles
  //    }
  //  }

  //  def latestArticles: Throwable \/ Seq[ArticleWithMetaInfo] = dbConnector.getAllArticles.map(latestArticles)


  //def latestArticles: Future[Try[Seq[ArticleWithMetaInfo]]] = dbConnector.getLatestArticles
  def latestArticlesForSortArticlesPage: Future[Try[Seq[ArticleForSortArticlesPage]]] = {
    dbConnector.getLatestArticlesWithCatalogDataAndPartners
    logger.info("Not fully implemented")
    Future.successful(Success(Seq()))
  }

  def namesAndIdsOfLatestArticlesOfPartner(id: MerchantOrSupplierId): Future[Try[Seq[ArticleNameAndId]]] = {

    logger.info("Not fully implemented")
    Future.successful(Success(Seq()))
  }

  private def latestVersion(articlesWithSameId: Seq[ArticleWithMetaInfo]): Option[ArticleWithMetaInfo] =
    articlesWithSameId.sortBy(_.metaInfo.versionCount).lastOption

//  private def previousVersion(allVersionsOfArticle: Seq[ArticleWithMetaInfo]): Option[ArticleWithMetaInfo] = {
//    val nrOfVersions = allVersionsOfArticle.size
//    if (nrOfVersions > 1) {
//      val sortedVersionsOfArticle = allVersionsOfArticle.sortBy(_.metaInfo.versionCount)
//      // Get the penultimate version of the article
//      Some(sortedVersionsOfArticle(nrOfVersions - 2))
//    }
//    else None
//  }

  def latestAndPreviousArticle(id: ArticleId) = dbConnector.getLatestAndPreviousArticle(id)

  //  def latestAndPreviousArticle(id: ArticleId): Throwable \/ Option[(ArticleWithMetaInfo, Option[ArticleWithMetaInfo])] =
  //    dbConnector.getArticleVersionsByArticleId(id)
  //      .map(allVersionsOfArticle => latestVersion(allVersionsOfArticle)
  //        // Get the most current version of the article and optionally the version before that
  //        .map(latestArticle => (latestArticle, previousVersion(allVersionsOfArticle)
  //      )))
}
