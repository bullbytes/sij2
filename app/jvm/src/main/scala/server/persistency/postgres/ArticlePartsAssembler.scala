package server.persistency.postgres

import server.persistency.postgres.PostgresTables._
import shared.articleformdata._
import shared.articles.categories.CategoryGuiStrings
import shared.articles.categories.CategoryIds.CategoryId
import shared.articles.categories.subcategories.ArticleSubcategories.SubcategoryId
import shared.articles.categories.subcategories.SubcategoryGuiStrings
import slogging.LazyLogging
import utils.TimeUtil

/**
  * Assembles [[ArticleWithMetaInfo]] from the parts of an article that we get from the database such as
  * [[ArticleVersionInDb]], [[ArticleDetailsInDb]], and [[PartnerInDb]].
  * <p>
  * It can be thought of as the counterpart to [[ToArticleInDb]] that prepares an article for being saved in the DB.
  * <p>
  * Created by Matthias Braun on 4/7/2017.
  */
private[postgres] object ArticlePartsAssembler extends LazyLogging {

//  private type PartsOfArticle = Seq[(ArticleVersionInDb, ArticleDetailsInDb, PartnerInDb, UnbrandedPriceInDb,
//    ArticlePriceScaleInDb, ArticleBrandingInDb, ArticlePriceScaleInDb, MinimumOrderQuantityInDb, ArticleImageInDb,
//    CatalogDataInDb, ArticleCategoryInDb, ArticleSubcategoryInDb)]
  private type PartsOfArticle = Seq[(ArticleVersionInDb, ArticleDetailsInDb, PartnerInDb, UnbrandedPriceInDb,
    ArticlePriceScaleInDb/*, ArticleBrandingInDb, ArticlePriceScaleInDb, MinimumOrderQuantityInDb, ArticleImageInDb,
    CatalogDataInDb, ArticleCategoryInDb, ArticleSubcategoryInDb*/)]

  private[postgres] def assemble(partsOfArticles: PartsOfArticle): Seq[ArticleWithMetaInfo] = {
    val startTime = System.nanoTime()

    val articles = partsOfArticles.groupBy(_._1.versionId).mapValues(toArticleWithMetaInfo).toSeq.flatMap(_._2)

    val durationInSeconds = TimeUtil.nanosToSeconds(System.nanoTime() - startTime)
    logger.info(s"Assembling ${partsOfArticles.size} parts took $durationInSeconds seconds. Returning ${articles.size} articles")

    articles
  }

  private def toBrandings(brandingsInDb: Iterable[ArticleBrandingInDb], scalesOfBrandingsInDb: Iterable[ArticlePriceScaleInDb]):
  Seq[ArticleBranding] =

    brandingsInDb.map(brandingInDb => {
      val scalesForBranding = scalesOfBrandingsInDb.filter(scale => scale.parentId == brandingInDb.id).toSeq

      ArticleBranding(name = brandingInDb.name, position = brandingInDb.position,
        scales = toPriceScales(scalesForBranding),
        initialCosts = brandingInDb.initialCosts, filmCosts = brandingInDb.filmCosts)
    }).toSeq.sortBy(_.name)

  private def toPriceScales(scales: Iterable[ArticlePriceScaleInDb]): Seq[ArticlePriceScale] =
  // It's important to sort by number since the client will put the scales onto the GUI as they appear in the sequence
    scales.toSeq.sortBy(_.number)
      .map(scaleInDb => ArticlePriceScale(name = scaleInDb.name, numberOfArticles = scaleInDb.nrOfArticles,
        price = scaleInDb.price))

  private def toMinimumOrderQuantities(moqsInDb: Iterable[MinimumOrderQuantityInDb]): Seq[MinimumOrderQuantity] =
    moqsInDb.toSeq.sortBy(_.number).map(moq => MinimumOrderQuantity(moq.condition, moq.quantity))

  private def toImages(articleImagesInDb: Iterable[ArticleImageInDb]): Seq[ArticleFormImage] =
    articleImagesInDb.toSeq.sortBy(_.number)
      .map(imageInDb => ArticleFormImage(url = imageInDb.url, description = imageInDb.description))


  private def toCategory(categoryInDb: ArticleCategoryInDb): CatalogCategory = {
    //val categoryMaybe=ArticleCategories.get(CategoryId(categoryInDb.id)
    val categoryName = CategoryGuiStrings.nameFromId(CategoryId(categoryInDb.id))

    CatalogCategory(categoryName.value)
  }

  def toSubcategory(subcategoryInDb: ArticleSubcategoryInDb): CatalogSubCategory = {

    val name = SubcategoryGuiStrings.subcategory(SubcategoryId(subcategoryInDb.id))

    CatalogSubCategory(name.value)
  }

  private def toArticleWithMetaInfo(partsOfArticleVersion: PartsOfArticle): Option[ArticleWithMetaInfo] = {

    val scalesOfUnbrandedPrice = partsOfArticleVersion.map(_._5).toSet

    //val brandingsInDb = partsOfArticleVersion.map(_._6).toSet

    // The price scales of all brandings of this article
    //val scalesOfBrandingsInDb = partsOfArticleVersion.map(_._7).toSet

//    val minimumOrderQuantitiesInDb = partsOfArticleVersion.map(_._8).toSet
//    val articleImagesInDb = partsOfArticleVersion.map(_._9).toSet

    partsOfArticleVersion.headOption.map {
//      case (articleVersionInDb, detailsInDb, partnerInDb, unbrandedPriceInDb, _, _, _, _, _, catalogDataInDb,
//      categoryInDb, subcategoryInDb) =>
      case (articleVersionInDb, detailsInDb, partnerInDb, unbrandedPriceInDb,_) =>

        val priceOfUnbrandedArticle = toPriceOfUnbrandedArticle(unbrandedPriceInDb, scalesOfUnbrandedPrice)
        val merchantOrSupplier = toMerchantOrSupplier(partnerInDb)

        val metaInfo = ArticleMetaInfo(ArticleId(articleVersionInDb.articleId),
          VersionCount(articleVersionInDb.versionCount), articleVersionInDb.creationDate)

        val articleData = PromotionalArticle(
          articleNumberAssignedByMerchantOrSupplier = detailsInDb.articleNrAssignedByPartner,
          name = articleVersionInDb.articleName, description = detailsInDb.description,
          size = detailsInDb.size, colorAndShape = detailsInDb.colorAndShape)

//        val catalogData: CatalogData = CatalogData(
//          remarksOnCatalogCreation = catalogDataInDb.remarksOnCatalogCreation,
//          positionInCatalog = catalogDataInDb.positionInCatalog,
//          category = toCategory(categoryInDb), subCategory = toSubcategory(subcategoryInDb),
//          pageInCatalog = catalogDataInDb.pageInCatalog,
//          articleNrInCatalog = catalogDataInDb.articleNrInCatalog)
val catalogData: CatalogData = CatalogData(
  remarksOnCatalogCreation = "",
  positionInCatalog = -1,
  category =CatalogCategory(""), subCategory = CatalogSubCategory(""),
  pageInCatalog = -1,
  articleNrInCatalog ="")

//        val formData = ArticleFormData(merchantOrSupplier, articleData, priceOfUnbrandedArticle,
//          brandings = toBrandings(brandingsInDb, scalesOfBrandingsInDb),
//          minimumOrderQuantities = toMinimumOrderQuantities(minimumOrderQuantitiesInDb),
//          images = toImages(articleImagesInDb), merchantSpecificFiles = Seq(), catalogData)

        val formData = ArticleFormData(merchantOrSupplier, articleData, priceOfUnbrandedArticle,
          brandings = Seq(),
          minimumOrderQuantities = Seq(),
          images = Seq(), merchantSpecificFiles = Seq(), catalogData)

        ArticleWithMetaInfo(formData, metaInfo)
    }
  }

  private def toPriceOfUnbrandedArticle(unbrandedPriceInDb: UnbrandedPriceInDb, allScalesInDb: Iterable[ArticlePriceScaleInDb]): UnbrandedArticlePrice = {
    // Get the scales that belong to the price of the unbranded article
    val scalesOfUnbrandedArticlePriceInDb = allScalesInDb.filter(scaleInDb => scaleInDb.parentId == unbrandedPriceInDb.id)

    val scalesOfUnbrandedArticlePrice = toPriceScales(scalesOfUnbrandedArticlePriceInDb)

    UnbrandedArticlePrice(scalesOfUnbrandedArticlePrice, unbrandedPriceInDb.suggestedPrice)
  }

  private def toMerchantOrSupplier(partner: PartnerInDb): MerchantOrSupplier = {
    if (partner.isMerchant)
      Merchant(partner.name, MerchantOrSupplierId(partner.id.toString))
    else
      Supplier(partner.name, MerchantOrSupplierId(partner.id.toString))
  }
}
