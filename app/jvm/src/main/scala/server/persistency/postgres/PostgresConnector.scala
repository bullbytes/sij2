package server.persistency.postgres

import java.util.UUID

import server.persistency.postgres.PostgresTables._
import shared.articleformdata._
import shared.articles.categories.ArticleCategories
import shared.articles.categories.subcategories.ArticleSubcategories
import shared.merchantsandsuppliers.MerchantsAndSuppliers
import slick.dbio.Effect
import slick.jdbc.PostgresProfile.api._
import slick.jdbc.meta.MTable
import slogging.LazyLogging
import utils.{LogUtil, TimeUtil}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success, Try}

/**
  * Lets us save data in a Postgres database and retrieve data from it.
  * <p>
  * Created by Matthias Braun on 3/29/2017.
  */
object PostgresConnector extends LazyLogging {


  // This configuration is in src/main/resources/application.conf
  private val db = Database.forConfig("pg-postgres")

  private def setUpDb(): Future[Future[Seq[Any]]] = {
    logger.info("Setting up Postgres database")
    // Get all existing tables and create any table that should exist but doesn't
    val futureCreateSchemaActions = db.run(MTable.getTables).map(existingTables => {

      PostgresTables.allTables.flatMap(table => {
        val tableName = table.baseTableRow.tableName
        // Create the tables if they don't exist yet
        if (!existingTables.exists(existingTable => existingTable.name.name == tableName)) {
          logger.info(s"Creating table '$tableName'")
          Some(table.schema.create)
        } else {
          logger.info(s"Table '$tableName' already exists")
          None
        }
      })
    })

    futureCreateSchemaActions.map(createSchemaActions => {
      val createAndPopulateActions = DBIO.sequence(createSchemaActions :+ populateTablesActions)

      db.run(createAndPopulateActions).andThen {
        case Success(_) => logger.info("Finished setting up database")
        case Failure(error) => logger.warn(s"Error while setting up database: $error")
          LogUtil.logWithStackTrace(error)
      }
    })
  }

  setUpDb()

  private def getArticlePartsQuery(idOfVersion: Rep[UUID]) =
    for {
      articleVersion <- PostgresTables.articleVersions if articleVersion.versionId === idOfVersion
      articleDetails <- articleVersion.articleDetails
      partner <- articleVersion.partner
      unbrandedPrice <- PostgresTables.unbrandedPrices if articleDetails.unbrandedPriceId === unbrandedPrice.id
      unbrandedPriceScales <- PostgresTables.priceScales if unbrandedPriceScales.parentId === unbrandedPrice.id
//      branding <- PostgresTables.brandings if branding.articleDetailsId === articleDetails.id
//      brandingScales <- PostgresTables.priceScales.filter(scale => scale.parentId === branding.id)
//      moq <- PostgresTables.minimumOrderQuantities if moq.articleDetailsId === articleDetails.id
//      image <- PostgresTables.articleImages if image.articleDetailsId === articleDetails.id
//      catalogData <- PostgresTables.catalogData if catalogData.articleDetailsId === articleDetails.id
//      articleCategory <- PostgresTables.articleCategories if articleCategory.id === catalogData.categoryId
//      articleSubcategory <- PostgresTables.articleSubcategories if articleSubcategory.id === catalogData.subcategoryId
    } yield (articleVersion, articleDetails, partner, unbrandedPrice, unbrandedPriceScales/*, branding,
      brandingScales, moq, image, catalogData, articleCategory, articleSubcategory*/)

  /* Creates a query that gets all the latest article versions from a query that yields article versions. */
  private def latestVersionsQuery(articlesQuery: Query[ArticleVersionTable, ArticleVersionInDb, Seq]):
  Query[ArticleVersionTable, ArticleVersionInDb, Seq] = {

    def articleIdAndLatestVersion =
      for {
        (articleId, versions) <- articlesQuery.groupBy(_.articleId)
      } yield articleId -> versions.map(_.versionCount).max.getOrElse(-1)

    for {
      article <- articlesQuery
      articleIdAndLatestVersion <- articleIdAndLatestVersion
      if article.articleId === articleIdAndLatestVersion._1 && article.versionCount === articleIdAndLatestVersion._2
    } yield article
  }

  def getLatestArticlesOf(id: MerchantOrSupplierId): Future[Try[Seq[ArticleWithMetaInfo]]] = {

    val articlesOfPartnerQuery = for {
      articlesOfPartner <- PostgresTables.articleVersions if articlesOfPartner.partnerId === id.id
    } yield articlesOfPartner

    val latestArticlesOfPartnerQuery = latestVersionsQuery(articlesOfPartnerQuery)

    val partsQuery = latestArticlesOfPartnerQuery.map(_.versionId).flatMap(id => getArticlePartsQuery(id))
    val startTime = System.nanoTime()
    db.run(partsQuery.result.asTry)
      .map(partsMaybe => partsMaybe
        .map(parts => {

          val durationInSeconds = TimeUtil.nanosToSeconds(System.nanoTime() - startTime)
          logger.info(s"Getting latest article parts of $id from database took $durationInSeconds seconds")
          ArticlePartsAssembler.assemble(parts)
        }))
  }

  private def latestArticleVersionQuery(articleId: ArticleId) = {
    val allVersionsOfArticleQuery = PostgresTables.articleVersions.filter(articleVersion =>
      articleVersion.articleId === articleId.id)

    latestVersionsQuery(allVersionsOfArticleQuery)
  }


  private def previousArticleVersionQuery(articleId: ArticleId) = for {
    latest <- latestArticleVersionQuery(articleId)
    previous <- PostgresTables.articleVersions if previous.articleId === articleId.id && previous.versionCount === latest.versionCount - 1
  } yield previous

  def getLatestAndPreviousArticle(id: ArticleId): Future[Try[(Option[ArticleWithMetaInfo], Option[ArticleWithMetaInfo])]] = {

    val getLatestVersionOfArticle = latestArticleVersionQuery(id)

    val getPreviousVersionOfArticle = previousArticleVersionQuery(id)

    val getLatestPartsAction = getLatestVersionOfArticle
      .flatMap(latestVersion => getArticlePartsQuery(latestVersion.versionId))
      .result

    val getPreviousPartsAction = getPreviousVersionOfArticle
      .flatMap(previousVersion => getArticlePartsQuery(previousVersion.versionId))
      .result

    val getPartsOfBothVersions = getLatestPartsAction.zip(getPreviousPartsAction)

    db.run(getPartsOfBothVersions.asTry)
      .map(pairOfPartsMaybe => pairOfPartsMaybe
        .map { case (partsOfLatest, partsOfPrevious) =>
          ArticlePartsAssembler.assemble(partsOfLatest).headOption -> ArticlePartsAssembler.assemble(partsOfPrevious).headOption
        })
  }


  def getLatestArticlesOf(ids: Seq[MerchantOrSupplierId]): Future[Try[Seq[ArticleWithMetaInfo]]] = {

    val getAllArticleVersionsOfPartners = {
      val idsAsStrings = ids.map(id => id.id)
      PostgresTables.articleVersions.filter(articleVersion => articleVersion.partnerId.inSet(idsAsStrings))
    }

    val getLatestArticlesOfPartners = latestVersionsQuery(getAllArticleVersionsOfPartners)
    val getPartsQuery = getLatestArticlesOfPartners.flatMap(article => getArticlePartsQuery(article.versionId))

    val startTime = System.nanoTime()
    db.run(getPartsQuery.result.asTry).map(partsMaybe =>
      partsMaybe.map(parts => {
        val durationInSeconds = TimeUtil.nanosToSeconds(System.nanoTime() - startTime)
        logger.info(s"Getting latest article parts from database took $durationInSeconds seconds")
        ArticlePartsAssembler.assemble(parts)
      })
    )
  }

  def getLatestArticles: Future[Try[Seq[ArticleWithMetaInfo]]] = {
    val getPartsOfLatestArticles = latestVersionsQuery(PostgresTables.articleVersions)
      .flatMap(article => getArticlePartsQuery(article.versionId))

    db.run(getPartsOfLatestArticles.result.asTry)
      .map(partsMaybe => partsMaybe.map(parts => ArticlePartsAssembler.assemble(parts)))
  }

  def getLatestArticlesWithCatalogDataAndPartners= {
    val getLatestVersions= latestVersionsQuery(PostgresTables.articleVersions)
    db.run(getLatestVersions.result).map(latestVersions=> logger.info(s"There are ${latestVersions.size} articles in the DB"))

  }

  def add(articleToAdd: ArticleWithMetaInfo): Future[Try[Seq[Option[Int]]]] = add(Seq(articleToAdd))

  def add(articlesToAdd: Seq[ArticleWithMetaInfo]): Future[Try[Seq[Option[Int]]]] = {

    val partsOfArticles = articlesToAdd.map(ToArticleInDb.toArticlePartsInDb)

    val allArticleVersions = partsOfArticles.map(_._1)
    val allArticleDetails = partsOfArticles.map(_._2)
    val allPricesOfUnbrandedArticles = partsOfArticles.map(_._3)
    val allPriceScalesOfUnbrandedArticles = partsOfArticles.flatMap(_._4)
    val allArticleBrandings = partsOfArticles.flatMap(_._5)
    val allPriceScalesOfBrandings = partsOfArticles.flatMap(_._6)
    val allMinimumOrderQuantities = partsOfArticles.flatMap(_._7)
    val allArticleImages = partsOfArticles.flatMap(_._8)
    val allCatalogData = partsOfArticles.map(_._9)

    val saveAllAction = DBIO.sequence(Seq(
      // Order of the actions is important since article versions reference the details,
      // so the details must exist before we can create an article. Also the brandings reference the details, so
      // the details must exist before we insert the brandings
      PostgresTables.articleDetails ++= allArticleDetails,
      PostgresTables.priceScales ++= allPriceScalesOfUnbrandedArticles ++ allPriceScalesOfBrandings,
      PostgresTables.unbrandedPrices ++= allPricesOfUnbrandedArticles,
      PostgresTables.brandings ++= allArticleBrandings,
      PostgresTables.articleVersions ++= allArticleVersions,
      PostgresTables.minimumOrderQuantities ++= allMinimumOrderQuantities,
      PostgresTables.articleImages ++= allArticleImages,
      PostgresTables.catalogData ++= allCatalogData
    ))
    db.run(saveAllAction.asTry)
  }

  def delete(articleId: ArticleId): Future[Try[Int]] = {
    // Deletes all the versions of the article
    val deleteAction = PostgresTables.articleVersions.filter(_.articleId === articleId.id).delete
    db.run(deleteAction.asTry)
  }

  private def populateTablesActions: DBIOAction[Seq[Int], NoStream, Effect.Write] = {
    // Populate the partners table consisting of merchants and suppliers
    val merchantsToInsert = MerchantsAndSuppliers.merchants.map(merchant => {
      val idAsString = merchant.id.id
      val name = merchant.name
      PartnerInDb(idAsString, name, isMerchant = true)
    })
    val suppliersToInsert = MerchantsAndSuppliers.suppliers.map(supplier => {
      val idAsString = supplier.id.id
      val name = supplier.name
      PartnerInDb(idAsString, name, isMerchant = false)
    })

    // Add the categories and subcategories of the articles
    val categoriesToInsert = ArticleCategories.all
      .map(category => ArticleCategoryInDb(category.id.value, ArticleCategories.sortNr(category)))

    val subcategoriesToInsert = ArticleSubcategories.all
      .map(subcategory => ArticleSubcategoryInDb(subcategory.id.value, ArticleSubcategories.sortNr(subcategory)))

    val insertAllPartnersAction = DBIO.sequence(
      merchantsToInsert.map(merchant => PostgresTables.partners.insertOrUpdate(merchant)) ++
        suppliersToInsert.map(supplier => PostgresTables.partners.insertOrUpdate(supplier))
    )

    val insertAllCategoriesAndSubcategoriesAction = DBIO.sequence(
      categoriesToInsert.map(category => PostgresTables.articleCategories.insertOrUpdate(category)) ++
        subcategoriesToInsert.map(subcategory => PostgresTables.articleSubcategories.insertOrUpdate(subcategory))
    )
    insertAllPartnersAction.andThen(insertAllCategoriesAndSubcategoriesAction)
  }
}
