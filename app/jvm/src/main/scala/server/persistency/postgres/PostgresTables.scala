package server.persistency.postgres

import java.util.UUID

import slick.jdbc.PostgresProfile.api._
import slick.lifted.{TableQuery, Tag}

/**
  * Represents tables in our Postgres database.
  * <p>
  * Created by Matthias Braun on 4/1/2017.
  */
private[postgres] object PostgresTables {

  private[postgres] val articleVersions = TableQuery[ArticleVersionTable]

  private[postgres] val partners = TableQuery[PartnerTable]

  private[postgres] val articleDetails = TableQuery[ArticleDetailsTable]

  private[postgres] val unbrandedPrices = TableQuery[UnbrandedPriceTable]

  private[postgres] val priceScales = TableQuery[PriceScaleTable]

  private[postgres] val brandings = TableQuery[BrandingTable]

  private[postgres] val minimumOrderQuantities = TableQuery[MinimumOrderQuantityTable]

  private[postgres] val articleImages = TableQuery[ArticleImageTable]

  private[postgres] val catalogData = TableQuery[CatalogDataTable]

  private[postgres] val articleCategories = TableQuery[ArticleCategoriesTable]

  private[postgres] val articleSubcategories = TableQuery[ArticleSubcategoriesTable]


  // Order is important here: If table B has a foreign key pointing to table A, it must be `Seq(A, B)` since
  // this is the order they will be created
  private[postgres] val allTables = Seq(priceScales, unbrandedPrices, brandings, partners, articleDetails,
    minimumOrderQuantities, articleImages, articleCategories, articleSubcategories, catalogData, articleVersions)

  private[postgres] class ArticleVersionTable(tag: Tag) extends Table[ArticleVersionInDb](tag, "article_versions") {
    // The ID of this version. Don't confuse it with the ID of the article (one article can have may versions)
    def versionId = column[UUID]("id", O.PrimaryKey)

    // The first version of an article has count 1, the second count 2, etc.
    def versionCount = column[Int]("version_count")

    def articleId = column[String]("article_id")

    def articleName = column[String]("article_name")

    def creationDate = column[Long]("creation_date")

    def partnerId = column[String]("partner_id")

    def partner = foreignKey("partner", partnerId, partners)(_.id)

    def articleDetailsId = column[UUID]("article_details_id")

    def articleDetails = foreignKey("article_details", articleDetailsId, PostgresTables.articleDetails)(_.id)

    def * = (versionId, articleId, articleName, creationDate, versionCount,
      partnerId, articleDetailsId) <>
      (ArticleVersionInDb.tupled, ArticleVersionInDb.unapply)
  }

  private[postgres] class PartnerTable(tag: Tag) extends Table[PartnerInDb](tag, "partners") {
    def id = column[String]("id", O.PrimaryKey)

    def name = column[String]("name")

    def isMerchant = column[Boolean]("is_merchant")

    def * = (id, name, isMerchant) <> (PartnerInDb.tupled, PartnerInDb.unapply)
  }

  private[postgres] class ArticleDetailsTable(tag: Tag) extends Table[ArticleDetailsInDb](tag, "article_details") {
    def id = column[UUID]("id", O.PrimaryKey)

    def articleNrAssignedByPartner = column[String]("article_nr_assigned_by_partner")

    def description = column[String]("description")

    def size = column[String]("size")

    def colorAndShape = column[String]("color_and_shape")

    def unbrandedPriceId = column[UUID]("unbranded_price_id")

    def * = (id, articleNrAssignedByPartner, description, size, colorAndShape, unbrandedPriceId) <> (ArticleDetailsInDb.tupled, ArticleDetailsInDb.unapply)
  }

  private[postgres] class UnbrandedPriceTable(tag: Tag) extends Table[UnbrandedPriceInDb](tag, "unbranded_prices") {
    def id = column[UUID]("id", O.PrimaryKey)

    def suggestedPrice = column[String]("suggested_price")

    // The price scales of the unbranded price are in their own table. The price scale rows link back to a row of this table

    def * = (id, suggestedPrice) <> (UnbrandedPriceInDb.tupled, UnbrandedPriceInDb.unapply)
  }

  private[postgres] class PriceScaleTable(tag: Tag) extends Table[ArticlePriceScaleInDb](tag, "price_scales") {
    def id = column[UUID]("id", O.PrimaryKey)

    def name = column[String]("name")

    def nrOfArticles = column[String]("nr_of_articles")

    def price = column[String]("price")

    def number = column[Int]("number")

    // A price scale can belong to a branding or it may be part of the article's unbranded price. This ID points to
    // that branding or unbranded price in the DB
    def parentId = column[UUID]("parent_id")

    def * = (id, name, nrOfArticles, price, number, parentId) <> (ArticlePriceScaleInDb.tupled, ArticlePriceScaleInDb.unapply)
  }

  private[postgres] class BrandingTable(tag: Tag) extends Table[ArticleBrandingInDb](tag, "brandings") {
    def id = column[UUID]("id", O.PrimaryKey)

    def name = column[String]("name")

    def position = column[String]("position")

    def initialCosts = column[String]("initial_costs")

    def filmCosts = column[String]("film_costs")

    // This ID lets us determine to which article the brandings belong
    def articleDetailsId = column[UUID]("article_details_id")

    // The price scales of the branding are in their own table. The price scale rows link back to a row of this table

    def * = (id, name, position, initialCosts, filmCosts, articleDetailsId) <> (ArticleBrandingInDb.tupled, ArticleBrandingInDb.unapply)
  }

  private[postgres] class MinimumOrderQuantityTable(tag: Tag) extends Table[MinimumOrderQuantityInDb](tag, "minimum_order_quantities") {
    def id = column[UUID]("id", O.PrimaryKey)

    def condition = column[String]("condition")

    def quantity = column[String]("quantity")

    def number = column[Int]("number")

    // This ID lets us determine to which article the minimum order quantity belongs
    def articleDetailsId = column[UUID]("article_details_id")

    def * = (id, condition, quantity, number, articleDetailsId) <> (MinimumOrderQuantityInDb.tupled, MinimumOrderQuantityInDb.unapply)
  }

  private[postgres] class ArticleImageTable(tag: Tag) extends Table[ArticleImageInDb](tag, "article_images") {
    def id = column[UUID]("id", O.PrimaryKey)

    def url = column[String]("url")

    def description = column[String]("description")

    def number = column[Int]("number")

    // This ID lets us determine to which article the image belongs
    def articleDetailsId = column[UUID]("article_details_id")

    def * = (id, url, description, number, articleDetailsId) <> (ArticleImageInDb.tupled, ArticleImageInDb.unapply)
  }

  private[postgres] class CatalogDataTable(tag: Tag) extends Table[CatalogDataInDb](tag, "catalog_data") {
    def id = column[UUID]("id", O.PrimaryKey)

    def remarksOnCatalogCreation = column[String]("remarks_on_catalog_creation")

    def positionInCatalog = column[Int]("position_in_catalog")

    def categoryId = column[String]("category_id")

    //def category = foreignKey("category", categoryId, articleCategories)(_.id)

    def subcategoryId = column[String]("subcategory_id")

    //def subcategory = foreignKey("subcategory", subcategoryId, articleSubcategories)(_.id)

    def pageInCatalog = column[Int]("page_in_catalog")

    // The article number assigned by VÖW as it appears in the catalog. Not the article number assigned by the partner
    def articleNrInCatalog = column[String]("article_nr_in_catalog")

    // This ID lets us determine to which article the catalog data belongs
    def articleDetailsId = column[UUID]("article_details_id")

    def * = (id, remarksOnCatalogCreation, positionInCatalog, categoryId, subcategoryId, articleNrInCatalog,
      pageInCatalog, articleDetailsId) <> (CatalogDataInDb.tupled, CatalogDataInDb.unapply)
  }

  private[postgres] class ArticleCategoriesTable(tag: Tag) extends Table[ArticleCategoryInDb](tag, "article_categories") {
    def id = column[String]("category_id", O.PrimaryKey)

    def sortNr = column[Int]("sort_nr")

    def * = (id, sortNr) <> (ArticleCategoryInDb.tupled, ArticleCategoryInDb.unapply)
  }

  private[postgres] class ArticleSubcategoriesTable(tag: Tag) extends Table[ArticleSubcategoryInDb](tag, "article_subcategories") {
    def id = column[String]("subcategory_id", O.PrimaryKey)

    def sortNr = column[Int]("sort_nr")

    def * = (id, sortNr) <> (ArticleSubcategoryInDb.tupled, ArticleSubcategoryInDb.unapply)
  }

  case class PartnerInDb(id: String, name: String, isMerchant: Boolean)

  case class UnbrandedPriceInDb(id: UUID, suggestedPrice: String)

  /* The number of the price scale for the DB tells us whether the scale is the first, the second, etc. within the branding or within the unbranded price */
  case class ArticlePriceScaleInDb(id: UUID, name: String, nrOfArticles: String, price: String, number: Int, parentId: UUID)

  case class ArticleVersionInDb(versionId: UUID, articleId: String, articleName: String, creationDate: Long,
                                versionCount: Int, partnerId: String, articleDetailsId: UUID)

  case class ArticleDetailsInDb(id: UUID, articleNrAssignedByPartner: String, description: String, size: String,
                                colorAndShape: String, unbrandedPriceId: UUID)

  case class ArticleBrandingInDb(id: UUID, name: String, position: String, initialCosts: String, filmCosts: String, articleDetailsId: UUID)

  case class MinimumOrderQuantityInDb(id: UUID, condition: String, quantity: String, number: Int, articleDetailsId: UUID)

  case class ArticleImageInDb(id: UUID, url: String, description: String, number: Int, articleDetailsId: UUID)

  case class CatalogDataInDb(id: UUID, remarksOnCatalogCreation: String, positionInCatalog: Int, categoryId: String,
                             subcategoryId: String, articleNrInCatalog: String, pageInCatalog: Int,
                             articleDetailsId: UUID)

  case class ArticleCategoryInDb(id: String, sortNr: Int)

  case class ArticleSubcategoryInDb(id: String, sortNr: Int)

}
