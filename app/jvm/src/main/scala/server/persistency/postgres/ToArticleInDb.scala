package server.persistency.postgres

import java.util.UUID

import server.persistency.postgres.PostgresTables._
import shared.articleformdata._
import shared.articles.categories.ArticleCategories
import shared.articles.categories.subcategories.ArticleSubcategories
import shared.utils.conversions.RichSeq.SeqUsingIndex
import slogging.LazyLogging

/**
  * Converts our articles into representations that we can save into our Postgres DB.
  * <p>
  * Created by Matthias Braun on 4/5/2017.
  */
private[postgres] object ToArticleInDb extends LazyLogging {

  private def toImages(images: Seq[ArticleFormImage], articleDetails: ArticleDetailsInDb) =
    images.mapWithIndex((image, index) =>
      ArticleImageInDb(UUID.randomUUID(), url = image.url, description = image.description, number = index + 1,
        articleDetailsId = articleDetails.id)
    )

  private def toCatalogData(catalogData: CatalogData, articleDetails: ArticleDetailsInDb) = {
    val categoryId = ArticleCategories.convert(catalogData.category).map(_.id)
      .getOrElse(ArticleCategories.none.id).value

    val subcategoryId = ArticleSubcategories.convert(catalogData.subCategory).map(_.id)
      .getOrElse(ArticleSubcategories.none.id).value

    CatalogDataInDb(UUID.randomUUID(), remarksOnCatalogCreation = catalogData.remarksOnCatalogCreation,
      positionInCatalog = catalogData.positionInCatalog, categoryId = categoryId, subcategoryId = subcategoryId,
      articleNrInCatalog = catalogData.articleNrInCatalog, pageInCatalog = catalogData.pageInCatalog,
      articleDetailsId = articleDetails.id)
  }

  private[postgres] def toArticlePartsInDb(article: ArticleWithMetaInfo):
  (ArticleVersionInDb, ArticleDetailsInDb, UnbrandedPriceInDb, Seq[ArticlePriceScaleInDb], Seq[ArticleBrandingInDb],
    Seq[ArticlePriceScaleInDb], Seq[MinimumOrderQuantityInDb], Seq[ArticleImageInDb], CatalogDataInDb) = {

    val formData = article.articleFormData

    val (priceOfUnbrandedArticleInDb, priceScalesOfUnbrandedArticleInDb) = {
      val unbrandedArticlePrice = formData.unbrandedArticlePrice

      val price = toUnbrandedPriceInDb(unbrandedArticlePrice)
      val scales = toScalesOfUnbrandedPricesInDb(unbrandedArticlePrice.unbrandedArticleScales, price.id)
      (price, scales)
    }

    val articleDetails = toArticleDetails(formData, priceOfUnbrandedArticleInDb.id)

    val brandingsInDb = toBrandingsInDb(articleDetails, formData.brandings)

    val priceScalesForBrandingsInDb = brandingsInDb.zip(formData.brandings).flatMap(toPriceScalesOfBrandingInDb)

    val minimumOrderQuantitiesInDb = toMinimumOrderQuantitiesInDb(formData.minimumOrderQuantities, articleDetails)

    val images = toImages(formData.images, articleDetails)

    val catalogData = toCatalogData(formData.catalogData, articleDetails)

    val articleInDb = toArticleInDb(article, articleDetails.id)

    (articleInDb, articleDetails, priceOfUnbrandedArticleInDb, priceScalesOfUnbrandedArticleInDb,
      brandingsInDb, priceScalesForBrandingsInDb, minimumOrderQuantitiesInDb, images, catalogData)
  }

  private def toArticleInDb(article: ArticleWithMetaInfo, articleDetailsId: UUID): ArticleVersionInDb = {
    val formData = article.articleFormData
    val metaInfo = article.metaInfo
    val creationDate = metaInfo.creationDate
    val versionCount = metaInfo.versionCount.count
    val partnerId = formData.merchantOrSupplier.id.id

    // Each version of an article has an ID. An article version can be thought of as the snapshot of an article in time
    val articleVersionId = UUID.randomUUID()
    // The ID of an article. It may have multiple versions
    val articleId = metaInfo.articleId.id

    ArticleVersionInDb(articleVersionId, articleId, formData.articleData.name,
      creationDate, versionCount, partnerId, articleDetailsId)
  }

  private def toBrandingsInDb(articleDetails: ArticleDetailsInDb, brandings: Seq[ArticleBranding]): Seq[ArticleBrandingInDb] =
    brandings.map(branding => ArticleBrandingInDb(UUID.randomUUID(),
      name = branding.name, position = branding.position,
      initialCosts = branding.initialCosts, filmCosts = branding.filmCosts,
      articleDetailsId = articleDetails.id))

  private def toPriceScalesOfBrandingInDb(brandingInDbAndInApp: (ArticleBrandingInDb, ArticleBranding)): Seq[ArticlePriceScaleInDb] = {
    val (brandingInDb, brandingInApp) = brandingInDbAndInApp
    val scalesInDb = brandingInApp.scales.mapWithIndex((scale, index) =>
      ArticlePriceScaleInDb(UUID.randomUUID(), name = scale.name,
        nrOfArticles = scale.numberOfArticles, price = scale.price, number = index + 1, parentId = brandingInDb.id)
    )
    scalesInDb
  }

  //  private[postgres] def toBrandingsWithPriceScalesForDb(detailsInDb: Seq[ArticleDetailsInDb], formData: Seq[ArticleFormData]):
  //  (Seq[ArticleBrandingInDb], Seq[ArticlePriceScaleInDb]) = {
  //
  //    val brandingsOfArticlesToAdd = formData.map(_.brandings)
  //
  //    val listOfPairs = detailsInDb.zip(brandingsOfArticlesToAdd).map {
  //      case (detailsForArticle, brandingsOfArticle) =>
  //        val brandingsInDb = toBrandingsInDb(detailsForArticle, brandingsOfArticle)
  //
  //        val priceScalesInDb = brandingsInDb.zip(brandingsOfArticle).flatMap(toPriceScalesOfBrandingInDb)
  //
  //        (brandingsInDb, priceScalesInDb)
  //    }
  //    val nestedPairOfSequences = listOfPairs.unzip
  //    (nestedPairOfSequences._1.flatten, nestedPairOfSequences._2.flatten)
  //  }

  private def toMinimumOrderQuantitiesInDb(quantities: Seq[MinimumOrderQuantity],
                                           articleDetails: ArticleDetailsInDb) =
    quantities.mapWithIndex((moq, index) =>
      MinimumOrderQuantityInDb(UUID.randomUUID(), condition = moq.condition,
        // We also provide a number for the MOQ so the client knows which MOQ to put first, second, etc.
        quantity = moq.quantity, number = index + 1, articleDetailsId = articleDetails.id))

  //
  //  private[postgres] def toArticleDetails(formData: Seq[ArticleFormData],
  //                                         pricesOfUnbrandedArticles: Seq[UnbrandedPriceInDb]): Seq[ArticleDetailsInDb] =
  //    formData.zip(pricesOfUnbrandedArticles).map { case (article, price) => toArticleDetails(article, price.id) }

  private def toArticleDetails(article: ArticleFormData, unbrandedPriceId: UUID): ArticleDetailsInDb = {
    val articleData = article.articleData
    ArticleDetailsInDb(UUID.randomUUID,
      articleNrAssignedByPartner = articleData.articleNumberAssignedByMerchantOrSupplier,
      description = articleData.description, colorAndShape = articleData.colorAndShape, size = articleData.size,
      unbrandedPriceId = unbrandedPriceId
    )
  }

  //
  //  private[postgres] def toScalesOfUnbrandedPricesInDb(formData: Seq[ArticleFormData],
  //                                                      pricesOfUnbrandedArticles: Seq[UnbrandedPriceInDb]): Seq[ArticlePriceScaleInDb] = {
  //
  //    val unbrandedArticlePriceScalesOfArticlesToAdd = formData.map(_.unbrandedArticlePrice.unbrandedArticleScales)
  //    unbrandedArticlePriceScalesOfArticlesToAdd.zip(pricesOfUnbrandedArticles)
  //      .flatMap { case (unbrandedArticlePriceScales, price) =>
  //        toScalesOfUnbrandedPricesInDb(unbrandedArticlePriceScales, price.id)
  //      }
  //  }

  private def toScalesOfUnbrandedPricesInDb(scales: Seq[ArticlePriceScale], unbrandedPriceId: UUID): Seq[ArticlePriceScaleInDb] =
    scales.mapWithIndex((scale, index) =>
      ArticlePriceScaleInDb(UUID.randomUUID(), name = scale.name,
        nrOfArticles = scale.numberOfArticles, price = scale.price, number = index + 1, parentId = unbrandedPriceId))


  private[postgres] def toUnbrandedPriceInDb(price: UnbrandedArticlePrice): UnbrandedPriceInDb = {
    UnbrandedPriceInDb(UUID.randomUUID(), suggestedPrice = price.suggestedPrice)
  }
}
