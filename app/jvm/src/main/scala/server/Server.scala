package server

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import server.persistency.Articles
import server.routes.SijRouter
import server.strings.ServerLogStrings.ServerStringImplicits.serverString2String
import server.strings.{ServerLogStrings, ServerStringsForClient}
import shared.articleformdata.{ArticleFormData, ArticleId, ArticleWithMetaInfo, MerchantOrSupplierId}
import shared.files.SijFile
import shared.serverresponses._
import shared.serverresponses.articleoverview.{OfferArticlesToArticleOverview, ServerResponseForArticleOverview}
import shared.serverresponses.sortarticlespage.{OfferArticlesToSortArticlesPage, ServerResponseForSortArticlesPage}
import shared.{Deadlines, SijAjaxApi}
import slogging.LazyLogging
import utils.{JsonUtil, TimeUtil}

import scala.concurrent.Future
import scala.util.{Failure, Properties, Success}

/**
  * The server lets clients add, change, and retrieve promotional articles.
  */
object Server extends SijAjaxApi with LazyLogging {

  private implicit val system = ActorSystem()
  private implicit val executionContext = system.dispatcher

  /**
    * Starts the server.
    */
  def up(): Unit = {
    implicit val materializer = ActorMaterializer()

    val port = Properties.envOrElse("PORT", "8080").toInt
    val host = "0.0.0.0"

    Http().bindAndHandle(SijRouter.route, host, port)

    logger.info(s"Server started. Host: $host:$port")
  }
  override def getLatestAndPreviousArticle(id: ArticleId): Future[ServerResponse] = {
    logger.info(ServerLogStrings.requestForLatestAndPreviousArticle(id))

     Articles.latestAndPreviousArticle(id).map {

      case Success((Some(latestArticle), prevArticleMaybe)) =>
        OfferLatestArticleWithPreviousVersion(latestArticle, prevArticleMaybe)
      case Success((None, _)) => logger.info(ServerLogStrings.couldNotFindArticleWithId(id))
        ServerError(ServerStringsForClient.noArticleWithId(id))
      case Failure(error) =>
        logger.warn(ServerLogStrings.couldNotFindArticleWithId(id, error))
        ServerError(ServerStringsForClient.noArticleWithId(id))
    }
  }

  override def getLatestArticlesForSortArticlesPage(): Future[ServerResponseForSortArticlesPage] = {
    logger.info(ServerLogStrings.requestForAllArticles)
    val startTime = System.nanoTime()

    Articles.latestArticlesForSortArticlesPage.map {
      case Success(latestArticles) =>
        logger.info(s"Offering all ${latestArticles.size} articles")
        val durationInSeconds = TimeUtil.nanosToSeconds(System.nanoTime() - startTime)
        logger.info(s"Getting all articles took $durationInSeconds seconds")
        OfferArticlesToSortArticlesPage(latestArticles)
      case Failure(e) => sortarticlespage.ServerError(ServerStringsForClient.errorInDb(e))
    }
  }

  override def getNameAndIdForLatestArticlesOf(id: MerchantOrSupplierId): Future[ServerResponseForArticleOverview] = {
    logger.info(ServerLogStrings.requestForLatestArticlesOfPartner(id))
    val retrievalResult = Articles.namesAndIdsOfLatestArticlesOfPartner(id)

    retrievalResult.map {
      case Success(latestArticles) =>
        logger.info(ServerLogStrings.gotArticlesForPartner(latestArticles.size, id))
        OfferArticlesToArticleOverview(latestArticles)
      case Failure(e) =>
        logger.warn(ServerLogStrings.errorInDb(e))
        articleoverview.ServerError(ServerStringsForClient.errorInDb(e))
    }
  }
  override def getLatestArticlesOf(id: MerchantOrSupplierId): Future[ServerResponse] = {
    logger.info(ServerLogStrings.requestForLatestArticlesOfPartner(id))
    val retrievalResult = Articles.latestArticlesOf(id)

    retrievalResult.map {
      case Success(latestArticles) =>
        logger.info(ServerLogStrings.gotArticlesForPartner(latestArticles.size, id))
        OfferArticles(latestArticles)
      case Failure(e) =>
        logger.warn(ServerLogStrings.errorInDb(e))
        ServerError(ServerStringsForClient.errorInDb(e))
    }
  }

  override def deleteArticle(articleId: ArticleId): Future[ServerResponse] = {
    logger.info(ServerLogStrings.requestToDeleteArticle(articleId))
    val deletionResult = Articles.delete(articleId)

    deletionResult.map {
      case Failure(e) =>
        logger.warn(ServerLogStrings.couldNotDeleteArticle(articleId, e))
        ServerError(ServerStringsForClient.exceptionWhileDeletingArticle(articleId, e))
      case Success(_) =>
        logger.info(ServerLogStrings.deletedArticle(articleId))
        DeletedArticle(articleId)
    }
  }

  override def saveNewArticle(formData: ArticleFormData): Future[ServerResponse] = {
    val articleName = formData.articleData.name
    if (Deadlines.deadlineHasPassedFor(formData.merchantOrSupplier)) {
      logger.info(ServerLogStrings.deadlineHasPassed(articleName))
      Future.successful(DeadlineHasPassed(ServerStringsForClient.deadlineHasPassed))
    } else {

      logger.info(ServerLogStrings.savingArticle(articleName))

      val saveResult = Articles.saveFirstVersion(formData)

      saveResult.map {
        case Failure(e) => logger.warn(ServerLogStrings.couldNotSaveArticle(articleName, e))
          GotArticleButCouldNotSave(ServerStringsForClient.couldNotSaveArticle(articleName, e))

        case Success(_) => logger.info(ServerLogStrings.savedNewArticle(articleName))
          GotArticleAndSavedIt(ServerStringsForClient.receivedArticle(articleName))
      }
    }
  }

  override def saveNewVersionOfArticle(articleWithMetaInfo: ArticleWithMetaInfo): Future[ServerResponse] = {

    val articleName = articleWithMetaInfo.articleFormData.articleData.name

    if (Deadlines.deadlineHasPassedFor(articleWithMetaInfo.articleFormData.merchantOrSupplier)) {
      logger.info(ServerLogStrings.deadlineHasPassed(articleName))
      Future.successful(DeadlineHasPassed(ServerStringsForClient.deadlineHasPassed))
    } else {
      logger.info(ServerLogStrings.gotNewVersionOfArticle(articleName))
      val saveResult = Articles.saveNewVersion(articleWithMetaInfo)

      saveResult.map {
        case Failure(e) => logger.warn(ServerLogStrings.couldNotSaveNewArticleVersion(articleName, e))
          GotArticleButCouldNotSave(ServerStringsForClient.couldNotSaveArticle(articleName, e))

        case Success(result) => logger.info(ServerLogStrings.savedNewArticleVersion(articleName))
          GotArticleAndSavedIt(ServerStringsForClient.gotNewVersionOfArticle(articleName))
      }
    }
  }

  override def saveNewVersionOfArticles(articlesToSave: Seq[ArticleWithMetaInfo]): Future[ServerResponse] = {

    logger.info(ServerLogStrings.gotNewVersionOfArticles(articlesToSave))
    val saveResult = Articles.saveNewVersions(articlesToSave)

    saveResult.map {
      case Failure(e) => logger.warn(ServerLogStrings.couldNotSaveNewArticleVersions(articlesToSave, e))
        GotArticlesButCouldNotSave(ServerStringsForClient.couldNotSaveArticles(articlesToSave, e))

      case Success(results) => logger.info(ServerLogStrings.savedNewArticleVersions(articlesToSave))
        GotArticlesAndSavedThem(ServerStringsForClient.gotNewVersionOfArticles(articlesToSave))
    }
  }

  override def saveArticlesFromJsonFile(fileWithArticles: SijFile): Future[ServerResponse] = {
    logger.info("Got file with articles from client")
    JsonUtil.articlesFromJson(fileWithArticles.content)
      .flatMap {
        case Right(articles) =>
          Articles.saveNewVersions(articles).map {
            case Success(_) => logger.info(s"Saved ${articles.size} articles in database")
              GotArticlesAndSavedThem(ServerStringForClient(s"Saved ${articles.size} articles in database"))
            case Failure(error) => logger.warn(s"Could not save articles in database. Error: $error")
              GotArticlesButCouldNotSave(ServerStringsForClient.couldNotSaveArticles(articles, error))
          }
        case Left(error) => Future.successful(
          CouldNotParseStringToArticles(ServerStringsForClient.couldNotParseStringToArticles(error)))
      }
  }
}
