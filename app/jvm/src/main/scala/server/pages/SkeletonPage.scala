package server.pages

import java.nio.charset.StandardCharsets

import shared.pages.{HiddenFields, PageId}

import scalatags.Text.TypedTag
import scalatags.Text.all._
import scalatags.Text.tags2.title

/**
  *
  * A bare-bone HTML page that includes our Scala code compiled to JavaScript. We populate the DOM using JavaScript at
  * the client to show content.
  * <p>
  * Created by Matthias Braun on 10/10/2016.
  *
  * @param pageTitle
  *                        the title of the page
  * @param pageId          the [[PageId]] of the page. The client populates this page with content depending on this ID.
  * @param valuesForClient the server sends the client information on what to display or how to behave using this
  *                        [[Map]] of values
  */
class SkeletonPage(pageTitle: String, pageId: PageId, valuesForClient: Map[String, Any] = Map()) {

  // The application name as defined in build.sbt in lowercase
  private val appNameLower = "sij"
  // Calls the main method of our app. https://www.scala-js.org/tutorial/basic/#automatically-creating-a-launcher
  private val launcher = "/" + appNameLower + "-launcher.js"
  // The client code we've written in Scala, translated to JavaScript
  private val app = "/" + appNameLower + ".js"

  // Client-side dependencies such as, for example, jQuery
  private val jsDependencies = s"/$appNameLower-jsdeps.min.js"

  // Font Awesome icons. Hash at end stems from our account at https://fontawesomecdn.com/
  private val fontAwesome = "https://use.fontawesome.com/ad5b15ef33.js"

  // Used for styling our web page
  private val pureCss = "https://cdn.jsdelivr.net/pure/0.6.0/pure-min.css"

  // We put a map of values and lists as hidden fields on the page for the client to read
  def valuesAsHiddenFields: Seq[TypedTag[String]] = valuesForClient.map { case (key, infoValue) =>
    input(`type` := "hidden", name := key, value := String.valueOf(infoValue))
  }.toSeq

  val content: TypedTag[String] =
    html(
      head(id := pageId.id)(
        meta(charset := StandardCharsets.UTF_8.displayName()),
        title(pageTitle),
        script(src := app),
        script(src := jsDependencies),
        script(src := fontAwesome),

        link(
          rel := "stylesheet",
          href := pureCss
        )
      ),
      body(
        div(id := HiddenFields.divWithFields.value)(valuesAsHiddenFields),
        script(src := launcher)
      )
    )
}
