package server.strings

import shared.articleformdata.{ArticleId, ArticleWithMetaInfo, MerchantOrSupplierId}

/**
  * The server uses these strings in log messages.
  * <p>
  * Created by Matthias Braun on 12/8/2016.
  */
object ServerLogStrings {

  /**
    * A string that the server uses in log messages.
    *
    * @param value the actual string of the log message
    */
  case class ServerLogString(value: String) extends AnyVal {
    override def toString: String = value
  }

  /**
    * Whenever a function needs a [[String]] but is given a [[ServerLogString]], the implicit function of this object
    * converts the argument from [[String]] to [[ServerLogString]].
    * To make this implicit conversion available, import this object.
    */
  object ServerStringImplicits {
    implicit def serverString2String(logString: ServerLogString): String = logString.value
  }

  private def articleOrArticles(nrOfArticles: Int) = if (nrOfArticles == 1) "article" else "articles"

  def gotNewVersionOfArticles(articlesToSave: Seq[ArticleWithMetaInfo]): ServerLogString = {
    val nrOfArticles = articlesToSave.size
    ServerLogString(s"Got $nrOfArticles ${articleOrArticles(nrOfArticles)} to save")
  }

  val requestForArticlesAsCsv = ServerLogString("Received request for all articles as CSV")

  val requestForAllSupplierArticles = ServerLogString("Received request for articles of all suppliers")

  def requestForArticlesOf(idsOfPartners: Seq[MerchantOrSupplierId]) =
    ServerLogString(s"Received request for articles of partners with IDs ${idsOfPartners.mkString(", ")}")

  def couldNotGetSupplierArticles(e: Throwable) = ServerLogString(s"Could not get articles of all suppliers. Error: $e")

  def couldNotGetAllArticles(e: Throwable) = ServerLogString(s"Could not get articles of all partners. Error: $e")

  def couldNotGetArticlesOf(ids: Seq[MerchantOrSupplierId], e: Throwable) =
    ServerLogString(s"Could not get articles of partners with IDs ${ids.mkString}. Error: $e")

  def deletedArticle(articleId: ArticleId) = ServerLogString(s"Deleted article with ID '$articleId'")

  def couldNotDeleteArticle(articleId: ArticleId, e: Throwable) =
    ServerLogString(s"Could not delete article with ID '$articleId'. Exception is: $e")

  def gotArticlesForPartner(nrOfArticles: Int, id: MerchantOrSupplierId) =
    ServerLogString(s"Got $nrOfArticles articles for partner with ID $id")

  def gotNewVersionOfArticle(articleName: String) = ServerLogString(s"Received new version of article '$articleName'")

  def savingArticle(articleName: String) = ServerLogString(s"Saving article '$articleName'")

  def couldNotSaveArticle(articleName: String, error: Throwable) =
    ServerLogString(s"Could not save article with name '$articleName'. Exception is $error")

  def savedNewArticle(articleName: String, id: String) = ServerLogString(s"Saved new article '$articleName' with ID $id")

  def savedNewArticle(articleName: String) = ServerLogString(s"Saved new article '$articleName'")

  def savedNewArticleVersion(articleName: String, articleId: String) =
    ServerLogString(s"Added new version of article '$articleName' with ID $articleId")

  def savedNewArticleVersion(articleName: String) =
    ServerLogString(s"Added new version of article '$articleName'")

  //  def savedNewArticleVersions(articlesToSave: Seq[ArticleWithMetaInfo], results: Seq[DocOk]): ServerLogString = {
  //    val nrOfArticles = articlesToSave.size
  //    ServerLogString(s"Saved $nrOfArticles ${articleOrArticles(nrOfArticles)}")
  //  }

  def savedNewArticleVersions(articlesToSave: Seq[ArticleWithMetaInfo]): ServerLogString = {
    val nrOfArticles = articlesToSave.size
    ServerLogString(s"Saved $nrOfArticles ${articleOrArticles(nrOfArticles)}")
  }

  def couldNotSaveNewArticleVersions(articlesToSave: Seq[ArticleWithMetaInfo], e: Throwable): ServerLogString = {
    val nrOfArticles = articlesToSave.size
    ServerLogString(s"Could not add new version of $nrOfArticles ${articleOrArticles(nrOfArticles)}. Exception is $e")
  }

  def couldNotSaveNewArticleVersion(articleName: String, e: Throwable) =
    ServerLogString(s"Could not add new version of article '$articleName'. Exception is $e")

  def deadlineHasPassed() = ServerLogString(s"Won't save data of article '${}' since deadline has passed")

  def requestToDeleteArticle(articleId: ArticleId) =
    ServerLogString(s"Received request to delete article with ID $articleId")

  def deadlineHasPassed(articleName: String) =
    ServerLogString(s" Won't save data of article '$articleName' since deadline has passed")

  def requestForAllArticles = ServerLogString("Received request for all articles")

  def requestForLatestAndPreviousArticle(id: ArticleId) =
    ServerLogString(s"Received request for latest and previous version of article with ID '$id'")

  def requestForLatestArticlesOfPartner(id: MerchantOrSupplierId) = ServerLogString(
    s"Received request for latest articles of partner with ID $id")

  def errorInDb(error: Throwable) = ServerLogString(s"Error occurred in database: $error")

  def exceptionWhileDeletingArticle(articleId: ArticleId, error: Throwable) =
    ServerLogString(s"Couldn't delete article with ID '$articleId'. Error: $error")

  def couldNotFindArticleWithId(id: ArticleId, error: Throwable) = ServerLogString(s"Didn't find article with ID $id in the database. Error: $error")
  def couldNotFindArticleWithId(id: ArticleId) =  ServerLogString(s"Didn't find article with ID $id in the database.")
}
