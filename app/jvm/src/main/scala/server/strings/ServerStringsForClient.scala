package server.strings

import shared.articleformdata.{ArticleId, ArticleWithMetaInfo}
import shared.serverresponses.ServerStringForClient

/**
  * The server uses these strings in messages sent to the client.
  * <p>
  * Created by Matthias Braun on 12/9/2016.
  */
object ServerStringsForClient {


  def gotNewVersionOfArticles(articlesToSave: Seq[ArticleWithMetaInfo]) =
    ServerStringForClient(s"Server hat ${articlesToSave.size} Artikel gespeichert")

  //  def couldNotGetArticlesAsCsv(exception: Exception) =
  //    ServerStringForClient(s"Konnte Artikel nicht zu CSV konvertieren. Fehler: $exception")

  def couldNotGetArticlesAsCsv(error: Throwable) =
    ServerStringForClient(s"Konnte Artikel nicht zu CSV konvertieren. Fehler: $error")

  def couldNotSaveArticle(articleName: String, error: Throwable) =
    ServerStringForClient(s"Server hat Artikel '$articleName' erhalten, konnte ihn aber nicht speichern. Fehler: $error")

  def couldNotSaveArticles(articlesToSave: Seq[ArticleWithMetaInfo], error: Throwable) =
    ServerStringForClient(s"Server hat ${articlesToSave.size} Artikel erhalten, konnte sie aber nicht speichern. Fehler: $error")

  def gotNewVersionOfArticle(articleName: String) = ServerStringForClient(s"Server hat neue Daten von Artikel '$articleName' gespeichert")

  def receivedArticle(articleName: String) = ServerStringForClient(s"Server hat Daten von Artikel '$articleName' erhalten")

  def noArticleWithId(id: ArticleId) = ServerStringForClient(s"Artikel mit ID '$id' konnte nicht gefunden werden")

  def deadlineHasPassed = ServerStringForClient("Die Frist für das Hinzufügen und Ändern von Artikeln ist abgelaufen")

  def exceptionWhileDeletingArticle(articleId: ArticleId, error: Throwable) =
    ServerStringForClient(s"Beim Löschen des Artikels mit der ID '$articleId' ist ein Fehler aufgetreten: $error")

  def errorInDb(e: Throwable) = ServerStringForClient(s"Ein Fehler in der Datenbank ist aufgetreten: $e")

  def couldNotParseStringToArticles(error: Throwable) =
    ServerStringForClient(s"Konnte String nicht in Artikel umwandeln. Ist der String gültiges JSON?. Fehler: $error")
}
