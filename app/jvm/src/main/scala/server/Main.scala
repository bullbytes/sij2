package server

import slogging._

/**
  * Starts this application.
  */
object Main extends LazyLogging {

  def printRuntimeInfo() = {
    val runtime = Runtime.getRuntime
    val mb = 1024 * 1024
    val maxMemoryInMb = runtime.maxMemory() / mb
    logger.info(s"JVM max memory: $maxMemoryInMb MB")
  }

  def main(args: Array[String]): Unit = {

    // Without this, we wouldn't have log messages
    LoggerConfig.factory = SLF4JLoggerFactory()

    logger.info("Let's start the server :-)")
    printRuntimeInfo()

    Server.up()
  }
}
