package utils

import org.scalatest.matchers.{MatchResult, Matcher}
/**
  * Additional matchers for our specifications.
  * <p>
  * Created by Matthias Braun on 1/14/2017.
  */
object CustomMatchers {

  /**
    * Creates a `Matcher` that checks if a string contains a the `expectedSubstrings`
    */
  def includeAllOf(expectedSubstrings: String*): Matcher[String] =
    new Matcher[String] {
      def apply(left: String): MatchResult =
        MatchResult(expectedSubstrings forall left.contains,
          s"""String "$left" did not include all of those substrings: ${expectedSubstrings.map(s => s""""$s"""").mkString(", ")}""",
          s"""String "$left" contained all of those substrings: ${expectedSubstrings.map(s => s""""$s"""").mkString(", ")}""")
    }

}
