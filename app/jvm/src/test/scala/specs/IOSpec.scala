package specs

import java.io.File
import java.nio.file.{Path, Paths}
import java.util.UUID

import org.scalatest.EitherValues._
import org.scalatest.Matchers._
import org.scalatest.{EitherValues, FlatSpec}
import slogging.LazyLogging
import utils.IO

/**
  * Defines the behavior of [[utils.IO]] which helps dealing with files.
  * <p>
  * Created by Matthias Braun on 1/16/2017.
  */
class IOSpec extends FlatSpec with LazyLogging {

  def withTempFile(testCode: (Path) => Any) {
    val tempDirPath = System.getProperty("java.io.tmpdir")
    val tempDir = new File(tempDirPath)
    tempDir.mkdirs()
    val tempFileName = UUID.randomUUID().toString
    val tempFilePath = Paths.get(tempDir.getAbsolutePath, tempFileName)
    testCode(tempFilePath)
  }

  "IO" should " be able to write a file " in withTempFile { tempFile =>

    val writeResult = IO.write(tempFile, "A test")

    writeResult.right.value should be(tempFile)
  }

  "IO" should " be able to write a file and read its content" in withTempFile { tempFile =>

    val fileContent = "A test"
    IO.write(tempFile, fileContent) match {
      case Right(pathToWrittenFile) => IO.read(pathToWrittenFile) should be(fileContent)
      case Left(exception) => fail(s"Could not write file at $tempFile. Exception: $exception")
    }
  }

}
