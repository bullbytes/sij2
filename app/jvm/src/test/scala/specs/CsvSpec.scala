package specs


import data.TestData
import org.scalatest.FlatSpec
import org.scalatest.Matchers._
import shared.articleformdata.ArticleFormData
import shared.merchantsandsuppliers.MerchantsAndSuppliers
import slogging.{LazyLogging, LoggerConfig, SLF4JLoggerFactory}
import utils.CsvUtil
import utils.CustomMatchers.includeAllOf

/**
  * Specifies how our [[CsvUtil]] should behave.
  * <p>
  * Created by Matthias Braun on 1/14/2017.
  */
class CsvSpec extends FlatSpec with LazyLogging {

  // Configure the logger. Without this, we wouldn't have log messages
  LoggerConfig.factory = SLF4JLoggerFactory()

  private val testArticle: ArticleFormData = TestData.completeArticleFormData(MerchantsAndSuppliers.unknownMerchant)

  "The CsvUtil" should "convert an article to a non-empty CSV row" in {
    val csv = CsvUtil.toCsvRow(testArticle)
    logger.info(s"CSV: $csv")
    csv should not be empty
  }

  "The CSV string of article data" should "contain all the article's fields" in {
    val csv = CsvUtil.toCsvRow(testArticle)
    val articleData = testArticle.articleData
    csv should includeAllOf(articleData.size, articleData.description, articleData.colorAndShape,
      articleData.articleNumberAssignedByMerchantOrSupplier, articleData.name)
  }

  "The CSV string of an article branding" should "contain all the article branding's fields" in {
    val csv = CsvUtil.toCsvRow(testArticle)
    val brandings = testArticle.brandings
    brandings.foreach(branding => {
      csv should includeAllOf(branding.name, branding.position, branding.initialCosts, branding.filmCosts)
      // Test the price scales
      branding.scales.foreach(scale =>
        csv should includeAllOf(scale.name, scale.numberOfArticles, scale.price)
      )
    })
  }

  "The CSV string of an article's unbranded price" should "contain all the fields of the article's unbranded price" in {

    val csv = CsvUtil.toCsvRow(testArticle)

    val price = testArticle.unbrandedArticlePrice
    csv should include(price.suggestedPrice)

    price.unbrandedArticleScales.foreach(scale =>
      csv should includeAllOf(scale.name, scale.numberOfArticles, scale.price)
    )
  }
  "The CSV string of a merchant or supplier" should "contain the name of the merchant or supplier" in {
    val merchantOrSupplier = testArticle.merchantOrSupplier

    val csv = CsvUtil.toCsvRow(testArticle)
    logger.info(s"CSV: $csv")
    csv should include(merchantOrSupplier.name)
  }
  "The CSV string of a an article's minimum order quantity" should "contain the fields of the minimum order quantity" in {
    val csv = CsvUtil.toCsvRow(testArticle)
    logger.info(s"CSV: $csv")
    testArticle.minimumOrderQuantities.foreach(minimumOrderQuantity =>
      csv should includeAllOf(minimumOrderQuantity.condition, minimumOrderQuantity.quantity)
    )
  }
  "The CSV string of a an article's catalog data" should "contain the fields of the catalog data" in {
    val catalogData = testArticle.catalogData
    val csv = CsvUtil.toCsvRow(testArticle)
    logger.info(s"CSV: $csv")
    csv should includeAllOf(catalogData.pageInCatalog.toString, catalogData.positionInCatalog.toString,
      catalogData.articleNrInCatalog.toString, catalogData.category.toString, catalogData.subCategory.toString,
      catalogData.remarksOnCatalogCreation)
  }

  "The CSV string of a an article's image" should "contain the URL and the description of the image" in {
    val csv = CsvUtil.toCsvRow(testArticle)
    logger.info(s"CSV: $csv")
    testArticle.images.foreach(image => csv should includeAllOf(image.url, image.description))
  }

}
