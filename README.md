# Scala.js [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) application with [Akka HTTP](http://doc.akka.io/docs/akka-http/current/scala/http/index.html) and [Postgres](https://www.postgresql.org/)
Using a form, the user can create promotional articles and send them to the server. Users can edit and delete articles.
Changes from one version of an article to the next are highlighted in the form.

## Get started

### Run the server locally in a Docker container
1. Install [SBT](http://www.scala-sbt.org/) and [Docker](https://docs.docker.com/engine/getstarted/step_one/)
2. To create an image from source, start SBT and issue `appJVM/docker`
3. If you're on Windows, make sure you share your Windows drive to make volumes work. Go to Docker's settings → Shared Drives
4. To run the created image execute `docker-compose up` in your terminal
5. Point your browser to `localhost:8080`
6. Stop the running container using `docker-compose down`

## Debugging
### Debugging the server
1. Create a [remote debugging configuration](https://www.jetbrains.com/help/idea/2016.2/run-debug-configuration-remote.html) that attaches to the JVM on port 5005
2. Start SBT like this: `sbt -Ddev.mode=true`
3. Start debugging using the remote configuration

## Miscellaneous hints
* Don't forget to `reload` in the SBT console after making changes to `build.sbt`
* Use `~appJVM/docker` to create a new Docker image automatically whenever you edit the source code
* To create JavaScript faster, start SBT like this: `sbt -Ddev.mode=true`. The generated JavaScript file will be larger, though

## TODOs:
* Find better way for the server to send data alongside the HTML
* There could be a REST API for setting the right for merchants and suppliers to change articles

